package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	nums := make([]bool, 10)
	for i := 0; i < n; i++ {
		var a, b, c, d int
		var r string
		_, _ = fmt.Scan(&a, &b, &c, &d, &r)

		if r == "NO" {
			nums[a] = true
			nums[b] = true
			nums[c] = true
			nums[d] = true
		} else {
			for i := range nums {
				if i != a && i != b && i != c && i != d {
					nums[i] = true
				}
			}
		}
	}

	for i, r := range nums {
		if !r {
			fmt.Println(i)
			break
		}
	}
}
