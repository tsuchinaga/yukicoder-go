package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	marks := make([]string, 5)
	rewards := make(map[string]int)
	for i := range marks {
		sc.Scan()
		s := sc.Text()
		sc.Scan()
		c, _ := strconv.Atoi(sc.Text())
		rewards[s] = c
		marks[i] = s
	}

	sc.Scan()
	n1, _ := strconv.Atoi(sc.Text())
	lille1 := make(map[string]int)
	for i := 0; i < n1; i++ {
		sc.Scan()
		lille1[sc.Text()]++
	}

	sc.Scan()
	n2, _ := strconv.Atoi(sc.Text())
	lille2 := make(map[string]int)
	for i := 0; i < n2; i++ {
		sc.Scan()
		lille2[sc.Text()]++
	}

	sc.Scan()
	n3, _ := strconv.Atoi(sc.Text())
	lille3 := make(map[string]int)
	for i := 0; i < n3; i++ {
		sc.Scan()
		lille3[sc.Text()]++
	}

	// fmt.Println(marks)
	// fmt.Println(rewards)
	// fmt.Println(n1, lille1)
	// fmt.Println(n2, lille2)
	// fmt.Println(n3, lille3)

	total := 0
	counts := make([]int, 5)
	for i, mark := range marks {
		cnt := lille1[mark] * lille2[mark] * lille3[mark] * 5
		total += cnt * rewards[mark]
		counts[i] = cnt
	}

	fmt.Printf("%.2f\n", float64(total)/float64(n1*n2*n3))
	for _, cnt := range counts {
		fmt.Println(cnt)
	}
}
