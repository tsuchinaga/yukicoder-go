package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	rdr := bufio.NewReaderSize(os.Stdin, 100000000)
	buf := make([]byte, 0, 100000000)
	for {
		l, p, _ := rdr.ReadLine()
		buf = append(buf, l...)
		if !p {
			break
		}
	}
	n := string(buf)

	var m int
	for _, c := range n {
		m = (m*4 + int(c-'0')) % 15
	}

	if m == 0 {
		fmt.Println("FizzBuzz")
	} else if m%3 == 0 {
		fmt.Println("Fizz")
	} else if m%5 == 0 {
		fmt.Println("Buzz")
	} else {
		fmt.Println(n)
	}
}
