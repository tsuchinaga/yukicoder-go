package main

import (
	"fmt"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	ans := ""
	if n%2 == 1 {
		ans += "7"
		n -= 3
	}

	fmt.Println(ans + str83_4(n/2, "1"))
}

func str83_4(n int, s string) string {
	if n <= 0 {
		return ""
	} else if n == 1 {
		return s
	} else if n%2 == 1 {
		return s + str83_4(n-1, s)
	} else {
		ss := str83_4(n/2, s)
		return ss + ss
	}
}
