package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	l := float64(len(s))
	safe := float64(strings.Count(s, "o"))
	for _, c := range []rune(s) {
		fmt.Printf("%.13f\n", safe/l*100)
		if string(c) == "o" {
			safe--
		}
		l--
	}
}
