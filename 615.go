package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	// 数字の一覧を取得し、昇順に並べておく
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	nums := make([]int, n)
	for i := range nums {
		sc.Scan()
		nums[i], _ = strconv.Atoi(sc.Text())
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	// fmt.Println(nums)

	// 各項の差を取って、これは降順にしておく
	diffs := make([]int, n-1)
	for i := 0; i < n-1; i++ {
		diffs[i] = nums[i+1] - nums[i]
	}

	sort.Slice(diffs, func(i, j int) bool {
		return diffs[i] > diffs[j]
	})

	// fmt.Println(diffs)

	// k個にわけるので、k-1個は集合の分け目で捨てられる
	ans := 0
	for i := k - 1; i < n-1; i++ {
		ans += diffs[i]
	}

	fmt.Println(ans)
}
