package main

import (
	"fmt"
	"sort"
)

func main() {
	var l, n, w int
	_, _ = fmt.Scan(&l, &n)

	blocks := make([]int, n)
	for i := range blocks {
		_, _ = fmt.Scan(&w)
		blocks[i] = w
	}

	sort.Slice(blocks, func(i, j int) bool {
		return blocks[i] < blocks[j]
	})

	sum := 0
	cnt := -1
	for i, w := range blocks {
		if sum+w > l {
			cnt = i
			break
		}

		sum += w
	}

	if cnt == -1 {
		cnt = n
	}

	fmt.Println(cnt)
}
