package main

import (
	"fmt"
)

func main() {
	var h, w int
	_, _ = fmt.Scan(&h, &w)

	line := ""
	paint := make([]int, h*w)
	black := make([]int, 0)
	for i := 0; i < h; i++ {
		_, _ = fmt.Scan(&line)
		for j, c := range line {
			if string(c) == "#" {
				paint[i*w+j] = 1
				black = append(black, i*w+j)
			}
		}
	}
	// fmt.Println(black)

	if len(black) == 0 || len(black)%2 != 0 {
		fmt.Println("NO")
		return
	}

	// 先頭から次のマスを塗る場合、次の次のマスを塗る場合、とシフトする数を増やしていく
	// 最小は1で、最大は全部のマス - 塗るマス
	for i := 1; i <= len(paint)-len(black)+1; i++ {
		filled := make(map[int]int, 0)
		r := true
		yShift := (black[0]+i)/w - (black[0])/w // y軸にシフトする数
		for j := 0; j < len(black); j++ {
			p := black[j]

			if filled[p] == 1 {
				continue
			}

			if p+i >= len(paint) { // 移動先が存在していない
				r = false
				break
			}

			// fmt.Println(i, j, p, paint[p+i] == 0, filled[p+i] == 1, len(black), (p%w+i)/w != yShift, (p%w+i)/w, yShift)

			// 移動先が塗られていないマス、移動先が使用済み、y軸のシフト数が違う場合 で失敗
			if paint[p+i] == 0 || filled[p+i] == 1 || (p%w+i)/w != yShift {
				r = false
				break
			}

			filled[p] = 1
			filled[p+i] = 1
		}

		if r {
			fmt.Println("YES")
			return
		}
	}
	fmt.Println("NO")
}
