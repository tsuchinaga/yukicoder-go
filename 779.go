package main

import (
	"fmt"
	"time"
)

func main() {
	var y, m, d int
	_, _ = fmt.Scan(&y, &m, &d)

	date := time.Date(y, time.Month(m), d, 0, 0, 0, 0, time.Local)

	l := time.Date(1989, time.Month(1), 8, 0, 0, 0, 0, time.Local)
	h := time.Date(2019, time.Month(4), 30, 0, 0, 0, 0, time.Local)

	if !date.Before(l) && !date.After(h) {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
