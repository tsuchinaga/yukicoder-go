package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	type card struct {
		s    string // 元の字
		m, n int    // マーク(1: D, 2: C, 3: H, 4: S), 数字(1: A, 2~9: 2~9, 10: T, 11: J, 12: Q, 13: K)
	}
	cards := make([]card, n)

	s := ""
	for i := range cards {
		_, _ = fmt.Scan(&s)
		cards[i].s = s

		switch s[0] {
		case 'D':
			cards[i].m = 1
		case 'C':
			cards[i].m = 2
		case 'H':
			cards[i].m = 3
		case 'S':
			cards[i].m = 4
		}

		switch s[1] {
		case 'A':
			cards[i].n = 1
		case 'T':
			cards[i].n = 10
		case 'J':
			cards[i].n = 11
		case 'Q':
			cards[i].n = 12
		case 'K':
			cards[i].n = 13
		default:
			cards[i].n = int(s[1] - '0')
		}
	}

	sort.Slice(cards, func(i, j int) bool {
		return cards[i].m < cards[j].m || (cards[i].m == cards[j].m && cards[i].n < cards[j].n)
	})
	for i, c := range cards {
		if i != 0 {
			fmt.Print(" ")
		}
		fmt.Print(c.s)
	}
	fmt.Println()
}
