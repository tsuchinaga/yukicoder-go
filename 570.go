package main

import (
	"fmt"
	"sort"
)

func main() {
	var a, b, c int
	_, _ = fmt.Scan(&a, &b, &c)

	p := map[int]string{a: "A", b: "B", c: "C"}
	h := []int{a, b, c}

	sort.Slice(h, func(i, j int) bool {
		return h[i] > h[j]
	})

	for _, i := range h {
		fmt.Println(p[i])
	}
}
