package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	t := string([]rune(s)[:len(s)/2])
	u := string([]rune(s)[len(s)/2:])

	if t == u {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
