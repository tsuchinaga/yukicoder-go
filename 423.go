package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	fmt.Println(ToHamu423(ToNum423(s) * 2))
}

func ToNum423(s string) int {
	s = strings.Replace(s, "hamu", "1", -1)
	s = strings.Replace(s, "ham", "0", -1)

	res := 0
	for i := 0; i < len(s); i++ {
		if string(s[len(s)-1-i]) == "1" {
			res += int(math.Pow(2, float64(i)))
		}
	}
	return res
}

func ToHamu423(n int) string {
	res := ""
	for _, c := range []rune(fmt.Sprintf("%b", n)) {
		if string(c) == "1" {
			res += "hamu"
		} else {
			res += "ham"
		}
	}
	return res
}
