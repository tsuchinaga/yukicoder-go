package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	// 諸々の初期化
	limit := 1
	nums := make([]int, n)
	scores := make([][]int, n)
	for i := 0; i < n; i++ {
		nums[i] = i
		limit *= i + 1
		scores[i] = make([]int, n)
	}
	// fmt.Println(nums)
	// fmt.Println(limit)

	// 得点情報の取得
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	for i := 0; i < m; i++ {
		sc.Scan()
		i1, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		i2, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		scores[i1][i2], _ = strconv.Atoi(sc.Text())
	}
	// fmt.Println(scores)

	// 各並び順を試す
	ans := 0
	for i := 0; i < limit; i++ {
		set := make([]int, n)
		copy(set, nums)

		k := i
		list := make([]int, 0)
		score := 0
		for len(set) > 0 {
			l := k % len(set)
			k /= len(set)

			// 順序によるスコア処理
			for _, a := range list {
				score += scores[a][set[l]]
			}

			list = append(list, set[l])
			if l == 0 {
				set = set[1:]
			} else {
				set = append(set[:l], set[l+1:]...)
			}
			// fmt.Println(list, set)
		}
		// fmt.Println(list)

		if score > ans {
			ans = score
		}
	}
	fmt.Println(ans)
}
