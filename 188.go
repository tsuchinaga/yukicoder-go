package main

import "fmt"

func main() {
	count := 0

	days := []int{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	for i, d := range days {
		month := i + 1
		for day := 1; day <= d; day++ {
			if month == day/10+day%10 {
				count++
			}
		}
	}

	fmt.Println(count)
}
