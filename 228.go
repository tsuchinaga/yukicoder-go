package main

import "fmt"

func main() {
	var a int
	nums := map[int]int{}

	for i := 1; i <= 4*4; i++ {
		_, _ = fmt.Scan(&a)
		nums[a] = i % 16
	}

	// fmt.Println(nums)

	for nums[0] != 0 {
		c := nums[0]
		t := nums[nums[0]]
		if (c-1)%16 != t && (c+1)%16 != t && (c-4)%16 != t && (c+4)%16 != t {
			fmt.Println("No")
			return
		}

		nums[0], nums[nums[0]] = nums[nums[0]], nums[0]

		// fmt.Println(nums)
	}

	cnt := 0
	for k, v := range nums {
		if k == v {
			cnt++
		}
	}

	if cnt == 16 {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
