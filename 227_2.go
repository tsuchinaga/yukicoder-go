package main

import "fmt"

func main() {
	var c int
	cards := make(map[int]int)
	hands := make(map[int]int)
	for i := 0; i < 5; i++ {
		_, _ = fmt.Scan(&c)
		cards[c]++
		hands[cards[c]-1]--
		hands[cards[c]]++
	}

	c3, c2 := hands[3], hands[2]
	ans := "NO HAND"
	if c3 == 1 && c2 == 1 {
		ans = "FULL HOUSE"
	} else if c3 == 1 {
		ans = "THREE CARD"
	} else if c2 == 2 {
		ans = "TWO PAIR"
	} else if c2 == 1 {
		ans = "ONE PAIR"
	}
	fmt.Println(ans)
}
