package main

import (
	"fmt"
	"strings"
)

func main() {
	s := ""
	_, _ = fmt.Scan(&s)
	fmt.Println(strings.Replace(s, "treeone", "forest", -1))
}
