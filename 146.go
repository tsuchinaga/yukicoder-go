package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n, c, d int
	_, _ = fmt.Scan(&n)
	p := int(math.Pow10(9)) + 7

	cnt := 0
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	for i := 0; i < n; i++ {
		sc.Scan()
		c, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		d, _ = strconv.Atoi(sc.Text())
		if c%2 == 0 {
			cnt += (c / 2) % p * (d % p)
		} else {
			cnt += (c/2 + 1) % p * (d % p)
		}
		cnt %= p
	}
	fmt.Println(cnt)
}
