package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, m int
	var s string

	r := false
	_, _ = fmt.Scan(&n, &m)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&s)
		r = r || strings.Contains(s, "LOVE")
	}

	if r {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
