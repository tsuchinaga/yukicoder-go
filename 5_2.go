package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	var l, n int
	_, _ = fmt.Scan(&l, &n)

	blocks := make([]int, n)
	for i := range blocks {
		sc.Scan()
		blocks[i], _ = strconv.Atoi(sc.Text())
	}
	sort.Slice(blocks, func(i, j int) bool {
		return blocks[i] < blocks[j]
	})

	ans := 0
	for _, w := range blocks {
		if l < w {
			break
		}
		ans++
		l -= w
	}
	fmt.Println(ans)
}
