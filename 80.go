package main

import "fmt"

func main() {
	var d int
	_, _ = fmt.Scan(&d)
	d1 := d / 2
	d2 := d1 / 2
	fmt.Println(d2 * (d1 - d2))
}
