package main

import "fmt"

func main() {
	var n, a, c int
	_, _ = fmt.Scan(&n)

	nums := make([]int, n)
	for range nums {
		_, _ = fmt.Scan(&a)
		nums[a-1]++
	}

	for i := 0; i < n-1; i++ {
		v := nums[i]
		if v > 1 {
			nums[i], nums[i+1], c = 1, nums[i+1]+(v-1), c+(v-1)
		} else if v < 1 {
			nums[i], nums[i+1], c = 1, nums[i+1]+(v-1), c-(v-1)
		}
	}
	fmt.Println(c)
}
