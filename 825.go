package main

import "fmt"

func main() {
	var a, b, c, ans int
	_, _ = fmt.Scan(&a, &b, &c)

	// bがなければa以上の枚数を作ることが出来ない
	// 所持枚数より9枚以上多い枚数を求められても作ることが出来ない
	if (b == 0 && a < c) || a+b+9 < c {
		fmt.Println("Impossible")
		return
	}

	// bがあって、枚数が足りないなら10Gをばらして枚数を増やしておく
	// ついでに少なくとも1G以上のものを買う必要があるので、なるべく小さな硬貨で1度支払うことを考える
	ans += 1
	if b > 0 && a+b < c || a == 0 {
		a += 9
		b -= 1
	} else if a > 0 {
		a -= 1
	}

	// ここからは減らすだけ
	for a+b != c {
		if (a+b)-c >= 9 && a >= 10 { // 9枚以上多くて、1Gが10枚あれば10G1枚に変えてしまう。
			a -= 10
			b += 1
		} else if a > 0 {
			a -= 1
			ans += 1
		} else if b > 0 {
			b -= 1
			ans += 10
		} else {
			ans = 0
			break
		}
		// fmt.Println(ans, a, b, c)
	}

	if ans == 0 {
		fmt.Println("Impossible")
	} else {
		fmt.Println(ans)
	}
}
