package main

import "fmt"

func main() {
	var n, a float64
	_, _ = fmt.Scan(&n)

	total := 0.0
	ballots := make([]float64, int(n))
	for i := range ballots {
		_, _ = fmt.Scan(&a)
		total += a
		ballots[i] = a
	}

	border := total / 10
	cnt := 0
	for _, n := range ballots {
		if border >= n {
			cnt++
		}
	}

	fmt.Println(cnt * 30)
}
