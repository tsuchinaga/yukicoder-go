package main

import "fmt"

func main() {
	var n, h, m, t int
	_, _ = fmt.Scan(&n, &h, &m, &t)

	m += t * (n - 1)
	h, m = (h+m/60)%24, m%60
	fmt.Printf("%d\n%d", h, m)
}
