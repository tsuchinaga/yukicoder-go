package main

import (
	"fmt"
	"sort"
)

func main() {
	var n string
	_, _ = fmt.Scan(&n)
	r := []rune(n)
	sort.Slice(r, func(i, j int) bool {
		return r[i] > r[j]
	})
	fmt.Println(string(r))
}
