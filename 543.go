package main

import "fmt"

func main() {
	var a, b string
	_, _ = fmt.Scan(&a, &b)
	fmt.Printf("%s %s", b, a)
}
