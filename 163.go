package main

import (
	"fmt"
	"strings"
)

func main() {
	var s, o string
	_, _ = fmt.Scan(&s)

	u := strings.ToUpper(s)

	for i, c := range s {
		if string(c) != string(u[i]) {
			o += string(u[i])
		} else {
			o += strings.ToLower(string(u[i]))
		}
	}

	fmt.Println(o)
}
