package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	max := int(math.Pow(2, 15))

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	res := make([]bool, max)
	res[0] = true
	for i := 0; i < n; i++ {
		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())

		for j := 0; j < max; j++ {
			if res[j] {
				res[j^a] = true
			}
		}
	}

	ans := 0
	for j := 0; j < max; j++ {
		if res[j] {
			ans++
		}
	}
	fmt.Println(ans)
}
