package main

import "fmt"

func main() {
	var a, b string
	_, _ = fmt.Scan(&a, &b)

	if len(a) > len(b) {
		fmt.Println(a)
	} else if len(a) < len(b) {
		fmt.Println(b)
	} else { // 長さが等しい
		for i := 0; i < len(a); i++ {
			if a[i] == b[i] {
				// 同じ桁の値が同じなら次の桁に移る
				continue
			}

			if (a[i] != '4' && a[i] != '7') || (b[i] != '4' && b[i] != '7') { // 少なくとも片方が4でも7でもない
				if a[i] > b[i] {
					fmt.Println(a)
				} else {
					fmt.Println(b)
				}
			} else { // 両方4か7で一致しない
				if a[i] == '4' {
					fmt.Println(a)
				} else {
					fmt.Println(b)
				}
			}
			break
		}
	}
}
