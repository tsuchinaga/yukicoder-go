package main

import "fmt"

func main() {
	s := ""
	_, _ = fmt.Scan(&s)

	if len(s) == 1 {
		fmt.Println(-1)
		return
	}

	for i, c := range s {
		if (i == 0 && string(c) != "1") || (i > 0 && string(c) != "3") {
			fmt.Println(-1)
			return
		}
	}

	fmt.Println(len(s) - 1)
}
