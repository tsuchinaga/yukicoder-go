package main

import (
	"fmt"
	"math"
)

func main() {
	var n, g, v float64
	_, _ = fmt.Scan(&n, &g, &v)
	fmt.Printf("%.12f", g*math.Floor(n/5)/v)
}
