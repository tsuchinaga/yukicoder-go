package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	if strings.Contains(s, "575") {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
