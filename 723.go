package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, x, a int
	_, _ = fmt.Scan(&n, &x)

	m := make(map[int]int, 0)
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	for i := 0; i < n; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())
		m[a]++
	}

	ans := 0
	for k, y := range m {
		if z, ok := m[x-k]; ok {
			ans += y * z
		}
	}
	fmt.Println(ans)
}
