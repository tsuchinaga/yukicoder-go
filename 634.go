package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	// n以下のコインを洗い出す
	coins := make([]int, 0)
	for i := 1; i <= n; i++ {
		c := i * (i + 1) / 2
		if c == n {
			fmt.Println(1)
			return
		} else if c < n {
			coins = append(coins, c)
		} else {
			break
		}
	}

	// fmt.Println(coins)

	// 答えの最大は3らしい
	for _, c1 := range coins {
		for _, c2 := range coins {
			if c1+c2 == n {
				fmt.Println(2)
				return
			}
		}
	}

	fmt.Println(3)
}
