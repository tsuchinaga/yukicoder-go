package main

import "fmt"

func main() {
	var n, m, x, y int
	_, _ = fmt.Scan(&n, &m, &x, &y)

	x--
	y--
	if x%(2*m) == y%(2*m) || x%(2*m)+y%(2*m) == (2*m-1) {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
