package main

import (
	"fmt"
	"sort"
)

func main() {
	var h, w int
	ps := make([]P571, 3)
	for i := range ps {
		_, _ = fmt.Scan(&h, &w)
		ps[i] = P571{i, h, w}
	}
	sort.Slice(ps, func(i, j int) bool {
		if ps[i].H > ps[j].H {
			return true
		} else if ps[i].H == ps[j].H && ps[i].W < ps[j].W {
			return true
		}
		return false
	})

	for _, p := range ps {
		switch p.N {
		case 0:
			fmt.Println("A")
		case 1:
			fmt.Println("B")
		case 2:
			fmt.Println("C")
		}
	}
}

type P571 struct {
	N, H, W int
}
