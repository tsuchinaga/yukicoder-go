package main

import (
	"fmt"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	nums := make([]int, n)
	for i := range nums {
		nums[i] = i + 1
	}
	nums = remove411(nums, k-1)
	fmt.Println(ascAsc411(nums, k))
}

func remove411(s []int, i int) []int {
	n := make([]int, len(s))
	copy(n, s)
	return append(n[:i], n[i+1:]...)
}

func ascAsc411(nums []int, before int) int {
	cnt := 0
	for i, n := range nums {
		if n < before { // 前の数字より小さな数字でかつ、数列の一番前 = 続く数字が自分より大きい数字しかない場合に昇順昇順になる
			if i == 0 {
				cnt++
			}
		} else {
			cnt += ascAsc411(remove411(nums, i), n)
		}
	}

	return cnt
}
