package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	nums := make([]int, 1)
	marks := make([]string, 1)
	for _, c := range s {
		if c == '*' || c == '+' {
			marks = append(marks, string(c))
			nums = append(nums, 0)
		} else {
			nums[len(nums)-1] = nums[len(nums)-1]*10 + int(c) - '0'
		}
	}

	ans := nums[0]
	for i := 1; i < len(nums); i++ {
		if marks[i] == "*" {
			ans += nums[i]
		} else {
			ans *= nums[i]
		}
	}
	fmt.Println(ans)
}
