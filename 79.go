package main

import "fmt"

func main() {
	var n, l int
	_, _ = fmt.Scan(&n)

	list := make(map[int]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&l)
		if _, ok := list[l]; ok {
			list[l]++
		} else {
			list[l] = 1
		}
	}

	max := 0
	maxLevel := 0
	for k, v := range list {
		if (max == v && maxLevel < k) || max < v {
			max = v
			maxLevel = k
		}
	}

	fmt.Println(maxLevel)
}
