package main

import "fmt"

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	d := n - k
	if d == -1 || d == 2 {
		fmt.Println("Won")
	} else if d == 1 || d == -2 {
		fmt.Println("Lost")
	} else {
		fmt.Println("Drew")
	}
}
