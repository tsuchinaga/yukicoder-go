package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	pn := make([]int, 0)
	pnn := make([]int, n+1)
	for i := 2; i <= n; i++ {
		if pnn[i] == 0 {
			pn = append(pn, i)

			for j := i + i; j <= n; j += i {
				pnn[j] = 1
			}
		}
	}
	// fmt.Println(pn)

	// trueが勝ち、それ以外が負け
	// 負ける数字を渡せる数字は勝てる数字
	win := make([]bool, n+1)
	for i := 2; i <= n; i++ {
		if win[i] == true {
			continue
		}

		for _, p := range pn {
			if i+p <= n {
				win[i+p] = true
			}
		}

		if win[n] == true {
			break
		}
	}

	// fmt.Println(win)

	if win[n] == true {
		fmt.Println("Win")
	} else {
		fmt.Println("Lose")
	}
}
