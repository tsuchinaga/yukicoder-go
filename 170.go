package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	src := make([]string, 0)
	for _, c := range s {
		src = append(src, string(c))
	}

	p := make(map[string]int, 0)
	for _, str := range permutation170(src, "") {
		p[str]++
	}

	fmt.Println(len(p) - 1)
}

func permutation170(src []string, dst string) []string {
	r := make([]string, 0)
	for i, c := range src {
		if len(src)-1 == 0 {
			r = append(r, dst+string(c))
		} else {
			nSrc := make([]string, len(src[:i]))
			copy(nSrc, src[:i])
			nSrc = append(nSrc, src[i+1:]...)

			// fmt.Println(dst, i, c, src, nSrc)
			r = append(r, permutation170(nSrc, dst+string(c))...)
		}
	}
	return r
}
