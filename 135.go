package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	nums := make([]int, 0)
	for i := 0; i < n; i++ {
		var x int
		_, _ = fmt.Scan(&x)
		if !Contains(nums, x) {
			nums = append(nums, x)
		}
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	min := 0
	for i := 1; i < len(nums); i++ {
		if min == 0 || min > nums[i]-nums[i-1] {
			min = nums[i] - nums[i-1]
		}
	}

	fmt.Println(min)
}

func Contains(nums []int, x int) bool {
	for _, n := range nums {
		if n == x {
			return true
		}
	}
	return false
}
