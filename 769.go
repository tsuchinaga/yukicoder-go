package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	sc := bufio.NewScanner(os.Stdin)
	uc, dc := make([]int, n), make([]int, n) // 使ったカード, 引いたカード
	d, p := 1, 0                             // 向き, 誰のターンか(0～n-1)
	d2, d4 := false, false                   // 前の番でdraw2が使われたか, 前の番でdraw4が使われたか
	d2s, d4s := 0, 0                         // draw2が連続で使われた枚数, draw4が連続で使われた枚数
	for i := 0; i < m; i++ {
		if i != 0 {
			p = (p + n + d) % n
			// fmt.Println(p, d)
		}

		sc.Scan()
		l := sc.Text()

		// fmt.Println(p, l)

		if d2 == true && l != "drawtwo" {
			dc[p] += d2s * 2
			d2 = false
			d2s = 0
			p = (p + n + d) % n // 次の人の番に移る
		}

		if d4 == true && l != "drawfour" {
			dc[p] += d4s * 4
			d4 = false
			d4s = 0
			p = (p + n + d) % n // 次の人の番に移る
		}

		uc[p]++
		switch l {
		case "drawtwo":
			d2 = true
			d2s++
		case "drawfour":
			d4 = true
			d4s++
		case "skip":
			p += d
		case "reverse":
			d *= -1
		}

		// fmt.Println(uc, dc)
	}
	fmt.Printf("%d %d\n", p+1, uc[p]-dc[p])
}
