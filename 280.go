package main

import "fmt"

func main() {
	var n, z1, z2 int
	_, _ = fmt.Scan(&n, &z2)

	p := make([]int, 0) // 分母列
	c := make([]int, 0) // 分子列
	for i := 1; i < n; i++ {
		z1 = z2
		_, _ = fmt.Scan(&z2)

		// z1とz2の最大公約数を求める
		d := divisor280(z1, z2)
		if z1/d > 1 {
			p = append(p, z1/d)
		}
		if z2/d > 1 {
			c = append(c, z2/d)
		}
	}

	// fmt.Println(c, p)

	// 総当たりで最大公約数で割っておく
	for i := range p {
		for j := range c {
			d := divisor280(p[i], c[j])

			p[i] /= d
			c[j] /= d
		}
	}

	pn, cn := 1, 1
	for _, v := range p {
		pn *= v
	}
	for _, v := range c {
		cn *= v
	}

	// fmt.Println(c, p, cn, pn)
	fmt.Printf("%d/%d\n", cn, pn)
}

func divisor280(a, b int) int {
	if a%b == 0 {
		return b
	}
	return divisor280(b, a%b)
}
