package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	s, l, d, x, y := 1, n, 1, -1, 0
	nums := make([]int, n*n)
	for i := 0; i < 2*n-1; i++ {
		for j := 0; j < l; j++ {
			switch d {
			case 0:
				y--
			case 1:
				x++
			case 2:
				y++
			case 3:
				x--
			}

			nums[x+y*n] = s
			s++
		}

		d = (d + 1) % 4
		if i%2 == 0 {
			l--
		}
	}

	for y := 0; y < n; y++ {
		line := ""
		for x := 0; x < n; x++ {
			line += fmt.Sprintf("%03d ", nums[x+y*n])
		}
		fmt.Printf("%s\n", strings.Trim(line, " "))
	}
}
