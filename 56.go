package main

import "fmt"

func main() {
	var d, p int
	_, _ = fmt.Scan(&d, &p)

	fmt.Println(d + (d * p / 100))
}
