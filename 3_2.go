package main

import (
	"fmt"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	if n == 1 {
		fmt.Println("1")
		return
	}

	// nums[i]にi+1のマスにいるときに動けるマスの数が入ってる
	nums := []int{1}
	before := []int{1}
	for len(nums) < n {
		l := len(before)
		tmp := make([]int, l*2)
		copy(tmp, before)
		for i := 0; i < l; i++ {
			tmp[l+i] = before[i] + 1
		}
		nums = append(nums, tmp...)
		before = tmp
	}

	ans := -1
	step := map[int]int{1: 1}
	queue := []int{1}
	for len(queue) > 0 {
		// キューの先頭から1つ取り出す
		c := queue[0]
		queue = queue[1:]

		// 移動した先がnなら、現在までのstep+1が答え
		move := nums[c-1]
		if c-move == n || c+move == n {
			ans = step[c] + 1
			break
		}

		// 現在地からマイナス方向の移動先が1以上でまだ踏んでない場所ならqueueにいれる
		if c-move >= 1 && step[c-move] == 0 {
			step[c-move] = step[c] + 1
			queue = append(queue, c-move)
		}

		// 現在地からプラス方向の移動先がn以下でまだ踏んでない場所ならqueueにいれる
		if c+move <= n && step[c+move] == 0 {
			step[c+move] = step[c] + 1
			queue = append(queue, c+move)
		}
	}
	fmt.Println(ans)
}
