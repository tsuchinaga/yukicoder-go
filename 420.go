package main

import (
	"fmt"
)

func main() {
	var x int
	_, _ = fmt.Scan(&x)

	if x == 0 {
		fmt.Println("1 0")
	} else if x == 31 {
		fmt.Println("1 2147483647")
	} else if x > 31 {
		fmt.Println("0 0")
	} else {
		a := comb420(31-1, x-1)
		b := comb420(31-1, x)
		fmt.Printf("%d %d\n", a+b, 2147483647*a)
	}
}

func comb420(l, r int) int {
	if r == 0 || l == r {
		return 1
	} else if r == 1 {
		return l
	} else {
		return comb420(l, r-1) * (l - r + 1) / r
	}
}
