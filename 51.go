package main

import (
	"fmt"
	"math"
)

func main() {
	var w, d int
	_, _ = fmt.Scan(&w, &d)

	total := 0
	for i := 0; i < d-1; i++ {
		total += (w - total) / int(math.Pow(float64(d-i), 2))
	}

	fmt.Println(w - total)
}
