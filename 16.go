package main

import (
	"fmt"
	"math"
)

func main() {
	var x, y, n, a, ans int
	_, _ = fmt.Scan(&x, &n)
	mod := int(math.Pow10(6)) + 3
	// fmt.Println(mod)

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		y = pow(x, a, mod)
		// fmt.Println(x, a, y)
		ans = (ans + y) % mod
	}
	fmt.Println(ans)
}

func pow(x, a, mod int) int {
	if a == 0 {
		return 1
	}

	if a%2 == 0 {
		b := pow(x, a/2, mod)
		return (b * b) % mod
	} else {
		return (x * pow(x, a-1, mod)) % mod
	}
}
