package main

import "fmt"

func main() {
	s := "285714"
	var n int
	_, _ = fmt.Scan(&n)

	fmt.Println(string([]rune(s)[(n-1)%len(s)]))
}
