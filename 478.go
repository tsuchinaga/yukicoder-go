package main

import "fmt"

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	t := []int{3, 1, 4, 2}
	m := n - 2 - k // 門松列の数
	a, b := t[0], t[1]
	fmt.Printf("%d %d", a, b)
	for i := 2; i < n; i++ {
		if m <= 0 {
			fmt.Printf(" %d", b)
		} else {
			fmt.Printf(" %d", t[i%4])
			a, b = b, t[i%4]
			m--
		}
	}
	fmt.Println()
}
