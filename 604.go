package main

import "fmt"

func main() {
	var a, b, c int
	_, _ = fmt.Scan(&a, &b, &c)

	ans := c / (b + (a - 1)) * a
	c %= b + (a - 1)

	// a回必要かどうか
	if c <= a-1 {
		ans += c
	} else {
		ans += a
	}

	fmt.Println(ans)
}
