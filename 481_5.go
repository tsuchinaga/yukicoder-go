package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	n := 55
	for i := 0; i < 9; i++ {
		sc.Scan()
		b, _ := strconv.Atoi(sc.Text())
		n -= b
	}
	fmt.Println(n)
}
