package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n, &a)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	max := a
	e := make([]int, 0)
	for i := 1; i < n; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())

		if max < a {
			e = append(e, max)
			max = a
		} else {
			e = append(e, a)
		}
	}

	for _, exp := range e {
		max += exp / 2
	}
	fmt.Println(max)
}
