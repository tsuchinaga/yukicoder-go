package main

import "fmt"

func main() {
	var b int
	m := map[int]int{1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0}
	for i := 0; i < 9; i++ {
		_, _ = fmt.Scan(&b)
		delete(m, b)
	}
	for ans := range m {
		fmt.Println(ans)
	}
}
