package main

import (
	"fmt"
	"strings"
)

func main() {
	var sb, sa string
	var n int
	_, _ = fmt.Scan(&sb, &n, &sa)

	if strings.Count(sb, "o") != strings.Count(sa, "o") { // 数が変わってるなら成功
		fmt.Println("SUCCESS")
		return
	}

	if n == 0 {
		if sa != sb { // 動かさないで一致しなければ成功
			fmt.Println("SUCCESS")
		} else {
			fmt.Println("FAILURE")
		}
		return
	}

	if n >= 2 { // 2回動かせたらどんな形にでもできる
		fmt.Println("FAILURE")
		return
	}

	// ここまできたら、コインの数は変わってなくて、あと動かせるのは1回
	// beforeの右2つか左2つを入れ替えてafterと一致すれば失敗、それ以外は成功
	if sa == string([]uint8{sb[1], sb[0], sb[2]}) || sa == string([]uint8{sb[0], sb[2], sb[1]}) {
		fmt.Println("FAILURE")
	} else {
		fmt.Println("SUCCESS")
	}
}
