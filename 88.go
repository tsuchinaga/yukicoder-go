package main

import (
	"fmt"
	"strings"
)

func main() {
	var s, b string
	_, _ = fmt.Scan(&s)
	users := map[int]string{0: "yukiko", 3: "oda"}

	cnt := 0
	for i := 0; i < 8; i++ {
		_, _ = fmt.Scan(&b)
		cnt += len(strings.Replace(b, ".", "", -1))
	}

	if cnt%2 == 0 { // 次は先攻
		fmt.Println(users[len(s)%6])
	} else { // 次は後攻
		fmt.Println(users[(len(s)+3)%6])
	}
}
