package main

import (
	"fmt"
	"math/big"
)

func main() {
	var a, b int64
	_, _ = fmt.Scan(&a, &b)
	ba := big.NewInt(a)
	fmt.Println(ba.Add(ba, big.NewInt(b)))
}
