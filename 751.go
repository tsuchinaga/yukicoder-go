package main

import "fmt"

func main() {
	var n, a, m int

	var c, p []int // 分子, 分母

	for i := 0; i < 2; i++ {
		_, _ = fmt.Scan(&n)
		for j := 0; j < n; j++ {
			_, _ = fmt.Scan(&a)
			if a < 0 {
				a *= -1
				m++
			}

			if (i == 0 && j == 0) || (i == 1 && j%2 != 0) {
				for k := range p {
					d := divisor751(a, p[k])
					a /= d
					p[k] /= d
				}

				c = append(c, a)
			} else {
				for k := range c {
					d := divisor751(a, c[k])
					a /= d
					c[k] /= d
				}

				p = append(p, a)
			}
		}
	}

	cn, pn := 1, 1
	for i := range c {
		cn *= c[i]
	}
	for i := range p {
		pn *= p[i]
	}
	if m%2 == 1 {
		cn *= -1
	}

	fmt.Printf("%d %d\n", cn, pn)
}

func divisor751(a, b int) int {
	if a%b == 0 {
		return b
	}
	return divisor751(b, a%b)
}
