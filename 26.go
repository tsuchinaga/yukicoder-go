package main

import "fmt"

func main() {
	var n, m, p, q int
	_, _ = fmt.Scan(&n, &m)

	cups := make([]int, 3)
	cups[n-1] = 1

	for i := 0; i < m; i++ {
		_, _ = fmt.Scan(&p, &q)
		cups[p-1], cups[q-1] = cups[q-1], cups[p-1]
	}

	for i, v := range cups {
		if v == 1 {
			fmt.Println(i + 1)
			break
		}
	}
}
