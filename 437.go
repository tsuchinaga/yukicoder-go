package main

import "fmt"

func main() {
	var n string
	var ans int
	_, _ = fmt.Scan(&n)
	r := []rune(n)

	type cww struct {
		score   int
		indexes []int
	}
	cwws := make([]cww, 0)
	for i := 0; i < len(r)-2; i++ {
		if r[i] == '0' {
			continue
		}

		for j := i + 1; j < len(r)-1; j++ {
			if r[i] == r[j] {
				continue
			}

			for k := j + 1; k < len(r); k++ {
				if r[j] == r[k] {
					c := cww{int(r[i]-'0')*100 + int(r[j]-'0')*10 + int(r[k]-'0'), []int{i, j, k}}

					for _, p := range cwws {
						add := true
						for _, in := range p.indexes {
							if in == i || in == j || in == k {
								add = false
								break
							}
						}
						if add {
							cwws = append(cwws, cww{p.score + c.score, append([]int{i, j, k}, p.indexes...)})
							if ans < p.score+c.score {
								ans = p.score + c.score
							}
						}
					}

					cwws = append(cwws, c)
					if ans < c.score {
						ans = c.score
					}
				}
			}
		}
	}

	// fmt.Println(cwws)
	fmt.Println(ans)
}
