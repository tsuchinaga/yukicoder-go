package main

import (
	"fmt"
	"math"
)

func main() {
	var h, w int
	_, _ = fmt.Scan(&h, &w)

	type p struct {
		x, y int
	}

	// 立ち位置
	pos := make([]p, 0)
	for i := 1; i <= h; i++ {
		pos = append(pos, p{0, i})
		pos = append(pos, p{w + 1, i})
	}
	for i := 1; i <= w; i++ {
		pos = append(pos, p{i, 0})
		pos = append(pos, p{i, h + 1})
	}

	// 黒い位置
	black := make([]p, 0)
	line := ""
	for i := 0; i < h; i++ {
		_, _ = fmt.Scan(&line)
		for j, c := range line {
			if c == '1' {
				black = append(black, p{j + 1, i + 1})
			}
		}
	}

	// fmt.Println(pos)
	// fmt.Println(black)

	// 総当たりで一番短い距離を探す
	min := math.MaxFloat64
	for _, po := range pos {
		time := 0.0
		for _, b := range black {
			time += math.Sqrt(float64(po.x-b.x)*float64(po.x-b.x) + float64(po.y-b.y)*float64(po.y-b.y))
		}
		if min > time {
			min = time
		}
	}
	fmt.Printf("%.8f\n", min)
}
