package main

import (
	"fmt"
)

func main() {
	var x, y, x1, y1 int
	_, _ = fmt.Scan(&x, &y, &x1, &y1)

	ans := 0
	if x == y && x1 == y1 && x > x1 {
		ans = x + 1
	} else if x > y {
		ans = x
	} else {
		ans = y
	}
	fmt.Println(ans)
}
