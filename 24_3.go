package main

import "fmt"

func main() {
	var n, a, b, c, d int
	var r string
	_, _ = fmt.Scan(&n)

	nums := make([]int, 10)

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a, &b, &c, &d, &r)

		if r == "YES" {
			nums[a]++
			nums[b]++
			nums[c]++
			nums[d]++
		} else {
			nums[a] -= n
			nums[b] -= n
			nums[c] -= n
			nums[d] -= n
		}
	}

	max := -1
	maxI := -1
	for k, v := range nums {
		if max < v {
			max = v
			maxI = k
		}
	}
	fmt.Println(maxI)
}
