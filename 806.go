package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n, a, b int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	nodes := make([]int, n)
	for i := 0; i < n-1; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		b, _ = strconv.Atoi(sc.Text())

		nodes[a-1]++
		nodes[b-1]++
	}

	// fmt.Println(nodes)

	// 各ノードが持つ枝の数が分かったので、枝の数で集計しなおす
	branches := make(map[int]int)
	for _, n := range nodes {
		branches[n]++
	}

	// fmt.Println(branches)

	// 1: 2, 2: n-2 になるまで一番大きいのと一番小さいのから減らして、その隣に加算する作業をする
	ans := 0
	for {
		if branches[1] == 2 && branches[2] == n-2 {
			break
		}

		max := 0
		min := math.MaxInt64
		for b := range branches {
			if max < b {
				max = b
			}
			if min > b {
				min = b
			}
		}
		d := int(math.Min(float64(branches[max]), float64(branches[min])))
		// fmt.Println(branches[max], branches[min], d)

		branches[max] -= d
		branches[max-1] += d
		branches[min+1] += d
		branches[min] -= d

		if branches[max] == 0 {
			delete(branches, max)
		}
		if branches[min] == 0 {
			delete(branches, min)
		}

		ans += d
		// fmt.Println(ans, branches)
	}
	fmt.Println(ans)
}
