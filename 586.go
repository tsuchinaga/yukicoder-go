package main

import "fmt"

func main() {
	var p1, p2, n, r int
	_, _ = fmt.Scan(&p1, &p2, &n)
	room := make(map[int]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&r)
		if _, ok := room[r]; ok {
			room[r]++
		} else {
			room[r] = 1
		}
	}

	c := 0
	for _, r := range room {
		if r >= 2 {
			c += (p1 + p2) * (r - 1)
		}
	}
	fmt.Println(c)
}
