package main

import (
	"fmt"
	"math"
)

func main() {
	var n, m string
	_, _ = fmt.Scan(&n, &m)
	if m == "0" {
		fmt.Println(1)
	} else {
		nn := n[len(n)-1] - '0'
		mn := m[len(m)-1] - '0'
		if len(m) >= 2 {
			mn += (m[len(m)-2] - '0') * 10
		}
		fmt.Println(int(math.Pow(float64(nn), float64((mn+3)%4+1))) % 10)
	}
}
