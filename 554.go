package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	m := int(math.Pow10(9)) + 7

	o, e := 0, 1
	if n == 1 {
		fmt.Println(e)
	} else {
		for i := 2; i <= n; i++ {
			if i == n {
				if i%2 == 0 {
					fmt.Println((e * i) % m)
				} else {
					fmt.Println((o * i) % m)
				}
			}
			if i%2 == 0 {
				o += e * i
				o %= m
			} else {
				e += o * i
				e %= m
			}
		}
	}
}
