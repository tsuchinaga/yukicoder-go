package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	for i := 0; i <= 200; i++ {
		for j := 0; j <= 200; j++ {
			aa := int(math.Round(float64(i*100) / float64(i+j)))
			bb := int(math.Round(float64(j*100) / float64(i+j)))
			if a == aa && b == bb {
				fmt.Println(i + j)
				return
			}
		}
	}
}
