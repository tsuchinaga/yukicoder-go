package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	r := []rune(s)
	for i := 0; i < len(r); i++ {
		if string(r[len(r)-1-i]) == "<" {
			fmt.Print(">")
		} else {
			fmt.Print("<")
		}
	}
	fmt.Printf("\n")
}
