package main

import "fmt"

func main() {
	var a, b, c int
	_, _ = fmt.Scan(&a, &b, &c)

	if ((a%2 == 0 || b%2 == 0) && a >= 2 && b >= 2) || a*b == 2 {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
