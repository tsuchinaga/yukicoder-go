package main

import (
	"fmt"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	for n > 0 {
		a = 1
		if n%2 == 1 {
			a = 7
			n--
		}
		n -= 2
		fmt.Print(a)
	}
	fmt.Println()
}
