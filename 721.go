package main

import (
	"fmt"
	"time"
)

func main() {
	var y, m, d int
	_, _ = fmt.Scanf("%d/%d/%d", &y, &m, &d)

	t := time.Date(y, time.Month(m), d, 0, 0, 0, 0, time.Local)
	t = t.Add(2 * 24 * time.Hour)
	fmt.Printf("%02d/%02d/%02d\n", t.Year(), t.Month(), t.Day())
}
