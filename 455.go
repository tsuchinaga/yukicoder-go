package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var h, w int
	_, _ = fmt.Scan(&h, &w)

	sc := bufio.NewScanner(os.Stdin)
	var x1, y1, x2, y2, x3, y3 int
	for i := 0; i < h; i++ {
		sc.Scan()
		for j, c := range sc.Text() {
			if c == '*' {
				if x1 == 0 {
					x1, y1 = j+1, i+1
				} else {
					x2, y2 = j+1, i+1
				}
			}
		}
	}

	if x1 == x2 { // 縦並び
		if x1 == 1 { // x1が左端ならx1の右隣
			x3, y3 = x1+1, y1
		} else { // x1の左隣
			x3, y3 = x1-1, y1
		}
	} else if y1 == y2 { // 横並び
		if y1 == 1 { // y1が上端ならy1の下隣
			x3, y3 = x1, y1+1
		} else { // y1の上隣
			x3, y3 = x1, y1-1
		}
	} else { // 斜め
		x3, y3 = x1, y2
	}

	for i := 1; i <= h; i++ {
		for j := 1; j <= w; j++ {
			if (j == x1 && i == y1) || (j == x2 && i == y2) || (j == x3 && i == y3) {
				fmt.Print("*")
			} else {
				fmt.Print("-")
			}
		}
		fmt.Println()
	}
}
