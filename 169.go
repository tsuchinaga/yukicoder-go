package main

import "fmt"

func main() {
	var k, s float64
	_, _ = fmt.Scan(&k, &s)

	fmt.Println(int(s / (100 - k) * 100))
}
