package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	p1 := map[int]int{1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1}
	p1 = pattern58([]int{1, 2, 3, 4, 5, 6}, p1, n-1)

	p2 := map[int]int{}
	if n == m {
		p2 = map[int]int{4: 2, 5: 2, 6: 2}
		p2 = pattern58([]int{4, 5, 6, 4, 5, 6}, p2, m-1)
	} else {
		p2 = map[int]int{1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1}
		p2 = pattern58([]int{1, 2, 3, 4, 5, 6}, p2, n-1-m)
		p2 = pattern58([]int{4, 5, 6, 4, 5, 6}, p2, m)
	}

	// fmt.Println(p1, p2)

	var w, o int // 勝ち, それ以外
	for d1, c1 := range p1 {
		for d2, c2 := range p2 {
			if d1 < d2 {
				w += c1 * c2
			} else {
				o += c1 * c2
			}
		}
	}

	fmt.Printf("%f\n", float64(w)/float64(w+o))
}

func pattern58(p []int, q map[int]int, n int) map[int]int {
	if n == 0 {
		return q
	} else {
		r := map[int]int{}
		for k, v := range q {
			for _, n := range p {
				r[k+n] += v
			}
		}
		return pattern58(p, r, n-1)
	}
}
