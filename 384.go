package main

import "fmt"

func main() {
	var h, w, n, k int
	_, _ = fmt.Scan(&h, &w, &n, &k)

	t := (h + w - 1) % n
	if t == 0 {
		t = n
	}
	if t == k {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
