package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c float64
	_, _ = fmt.Scan(&a, &b, &c)

	bn, cn := math.Ceil(a/b), math.Ceil(a/c)
	if bn*2 >= cn*3 {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
