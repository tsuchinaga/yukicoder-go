package main

import (
	"fmt"
	"math"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	nums := make([]int, n)
	for i := range nums {
		_, _ = fmt.Scan(&a)
		nums[i] = a
	}

	for i := 1; i < 2*n-3; i++ {
		for p := int(math.Max(0, float64(i-n-1))); p <= n-2; p++ {
			q := i - p
			if !(0 <= q && p < q && q <= n-1) {
				continue
			}
			if nums[p] > nums[q] {
				nums[p], nums[q] = nums[q], nums[p]
			}
		}
	}

	for i, a := range nums {
		fmt.Print(a)
		if i == len(nums)-1 {
			fmt.Printf("\n")
		} else {
			fmt.Printf(" ")
		}
	}
}
