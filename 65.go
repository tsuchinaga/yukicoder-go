package main

import "fmt"

func main() {
	var k int
	_, _ = fmt.Scan(&k)

	es := map[int]float64{}
	for i := k - 1; i >= 0; i-- {
		var e1, e2, e3, e4, e5, e6 float64
		if i+1 < k {
			e1 = es[i+1]
		}
		if i+2 < k {
			e2 = es[i+2]
		}
		if i+3 < k {
			e3 = es[i+3]
		}
		if i+4 < k {
			e4 = es[i+4]
		}
		if i+5 < k {
			e5 = es[i+5]
		}
		if i+6 < k {
			e6 = es[i+6]
		}

		es[i] = (e1+e2+e3+e4+e5+e6)/6 + 1
	}

	// fmt.Println(es)
	fmt.Printf("%.3f\n", es[0])
}
