package main

import (
	"fmt"
	"sort"
)

func main() {
	var n, m, c int
	_, _ = fmt.Scan(&n, &m)

	boxes := make([]int, n)
	for i := range boxes {
		_, _ = fmt.Scan(&c)
		boxes[i] = c
	}

	sort.Slice(boxes, func(i, j int) bool {
		return boxes[i] < boxes[j]
	})

	total := 0
	for _, c := range boxes {
		m -= c
		if m < 0 {
			break
		}
		total++
	}

	fmt.Println(total)
}
