package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	step := b / a
	if b%a > 0 {
		step++
	}

	fmt.Println(step)
}
