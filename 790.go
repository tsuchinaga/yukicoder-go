package main

import "fmt"

func main() {
	var n, ans int
	_, _ = fmt.Scan(&n)

	m := []map[string]int{{"": 1}}
	for i := 1; i <= n; i++ {
		tmp := map[string]int{}

		// 1個前のとのくっつけと囲い
		for s := range m[i-1] {
			// 右からのくっつけ
			tmp[s+"()"]++

			// 左からのくっつけ
			tmp["()"+s]++

			// 囲い
			tmp["("+s+")"]++
		}

		// 2個目～ののくっつきでの表現
		for j := 2; j <= i/2; j++ {
			for s1 := range m[j] {
				for s2 := range m[len(m)-j] {
					// fmt.Println(s1, s2)
					tmp[s1+s2]++
					tmp[s2+s1]++
				}
			}
		}

		ans = len(tmp)
		m = append(m, tmp)
		// fmt.Println(m)
	}

	fmt.Println(ans)
}
