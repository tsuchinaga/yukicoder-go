package main

import (
	"fmt"
	"math"
)

func main() {
	var p, c int
	_, _ = fmt.Scan(&p, &c)

	pD := []int{2, 3, 5, 7, 11, 13}
	cD := []int{4, 6, 8, 9, 10, 12}

	d := map[int]int{1: 1}
	for i := 0; i < p; i++ {
		tmp := make(map[int]int, 0)
		for k, v := range d {
			for _, a := range pD {
				tmp[a*k] += v
			}
		}
		d = tmp
	}
	for i := 0; i < c; i++ {
		tmp := make(map[int]int, 0)
		for k, v := range d {
			for _, a := range cD {
				tmp[a*k] += v
			}
		}
		d = tmp
	}

	sum := 0
	for k, v := range d {
		sum += k * v
	}

	fmt.Printf("%.10f\n", float64(sum)/math.Pow(6, float64(p+c)))
}
