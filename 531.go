package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)
	if m >= n {
		fmt.Println(1)
	} else if n%2 == 0 && 2*m >= n {
		fmt.Println(2)
	} else {
		fmt.Println(-1)
	}
}
