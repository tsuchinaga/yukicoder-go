package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	for i := 1; i <= n; i++ {
		str := ""
		if i%3 == 0 {
			str += "Fizz"
		}
		if i%5 == 0 {
			str += "Buzz"
		}

		if str == "" {
			fmt.Println(i)
		} else {
			fmt.Println(str)
		}
	}
}
