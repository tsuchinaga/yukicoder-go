package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	ans := 0
	for n > 1 {
		if n%2 == 1 {
			n++
		}
		n /= 2
		ans++
	}
	fmt.Println(ans)
}
