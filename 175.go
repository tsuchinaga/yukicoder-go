package main

import (
	"fmt"
	"math"
)

func main() {
	var l, n float64
	_, _ = fmt.Scan(&l, &n)
	fmt.Println(int(math.Pow(2, l-3) * n))
}
