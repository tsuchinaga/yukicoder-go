package main

import "fmt"

func main() {
	var r, k, h, w, x, y int
	_, _ = fmt.Scan(&r, &k, &h, &w)

	index := func(i, j int) int { return 0 }
	switch r {
	case 0:
		x, y = h, w
		index = func(i, j int) int { return y*i + j }
	case 90:
		x, y = w, h
		index = func(i, j int) int { return y - 1 - i + y*j }
	case 180:
		x, y = h, w
		index = func(i, j int) int { return x*y - y*i - j - 1 }
	case 270:
		x, y = w, h
		index = func(i, j int) int { return y*(x-1) + i - y*j }
	}

	// 回転しながら配列に入れる
	pic := make([]int, h*w)
	var c string
	for i := 0; i < h; i++ {
		_, _ = fmt.Scan(&c)
		for j, c := range []rune(c) {
			// fmt.Println(h, w, h*w, len(pic), i, j, index(i, j))
			if c == '#' {
				k := index(i, j)
				pic[k] = 1
			}
		}
	}
	// fmt.Println(pic)

	// 拡大しながら出力
	for i := 0; i < x; i++ {
		for a := 0; a < k; a++ { // 拡大分の縦の繰り返し
			for j := 0; j < y; j++ {
				for a := 0; a < k; a++ { // 拡大分の横の繰り返し
					if pic[y*i+j] == 0 {
						fmt.Print(".")
					} else {
						fmt.Print("#")
					}
				}
			}
			fmt.Println()
		}
	}
}
