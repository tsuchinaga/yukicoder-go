package main

import "fmt"

func main() {
	var a int
	_, _ = fmt.Scan(&a)

	h, m := 10, a/10*6
	h, m = (10+m/60)%24, m%60

	fmt.Printf("%02d:%02d", h, m)
}
