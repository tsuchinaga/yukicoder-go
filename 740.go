package main

import "fmt"

func main() {
	var n, m, p, q, cnt int
	_, _ = fmt.Scan(&n, &m, &p, &q)

	d := m*(12-q) + 2*m*q // 年間落葉枚数
	n, cnt = n%d, n/d*12  // 落ちきる年の1月時点
	for i := 1; i <= 12; i++ {
		if n <= 0 {
			fmt.Println(cnt)
			break
		}

		if p <= i && i < p+q {
			n -= 2 * m
		} else {
			n -= m
		}
		cnt++
	}
}
