package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	n, _ := strconv.Atoi(sc.Text())

	type client map[string]int
	clients := make(map[int]client, 0)

	for i := 0; i < n; i++ {
		sc.Scan()
		d, _ := strconv.Atoi(sc.Text())
		switch d {
		case 0: // 入店
			sc.Scan()
			n, _ := strconv.Atoi(sc.Text())
			sc.Scan()
			m, _ := strconv.Atoi(sc.Text())
			c := client{}
			for j := 0; j < m; j++ {
				sc.Scan()
				c[sc.Text()]++
			}
			clients[n] = c
		case 1: // サラキタル
			sc.Scan()
			s := sc.Text()
			catch := false
			for j := 1; j <= 20; j++ {
				if c, ok := clients[j]; ok {
					if c[s] > 0 {
						c[s]--
						catch = true
						fmt.Println(j)
						break
					}
				}
			}
			if !catch {
				fmt.Println(-1)
			}
		case 2: // 退店
			sc.Scan()
			n, _ := strconv.Atoi(sc.Text())
			delete(clients, n)
		}
	}
}
