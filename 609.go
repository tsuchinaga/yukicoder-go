package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	bi := bufio.NewScanner(os.Stdin)
	bi.Split(bufio.ScanWords)

	sum := 0
	nums := make([]int, n)
	for i := range nums {
		bi.Scan()
		a, _ := strconv.Atoi(bi.Text())
		sum += a
		nums[i] = a
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	// fmt.Println(nums)

	l := nums[int(math.Floor(float64(n)/2))]
	h := nums[int(math.Ceil(float64(n)/2))]

	// fmt.Println(l, h)

	var ld, hd int
	for _, a := range nums {
		ld += int(math.Abs(float64(l - a)))
		hd += int(math.Abs(float64(h - a)))
	}

	fmt.Println(int(math.Min(float64(ld), float64(hd))))
}
