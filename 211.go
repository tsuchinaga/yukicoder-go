package main

import "fmt"

func main() {
	p := []int{2, 3, 5, 7, 11, 13}
	c := []int{4, 6, 8, 9, 10, 12}

	m := make(map[int]int, 0)
	for _, i := range p {
		for _, j := range c {
			if _, ok := m[i*j]; ok {
				m[i*j]++
			} else {
				m[i*j] = 1
			}
		}
	}

	var k int
	_, _ = fmt.Scan(&k)
	if v, ok := m[k]; ok {
		fmt.Println(float64(v) / 36)
	} else {
		fmt.Println(0)
	}
}
