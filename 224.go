package main

import "fmt"

func main() {
	var n int
	var s, t string
	_, _ = fmt.Scan(&n, &s, &t)

	cnt := 0
	for i, c := range s {
		if string(c) != string(t[i]) {
			cnt++
		}
	}

	fmt.Println(cnt)
}
