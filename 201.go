package main

import "fmt"

func main() {
	var sa, pa, xa, sb, pb, xb string
	_, _ = fmt.Scan(&sa, &pa, &xa, &sb, &pb, &xb)

	if len(pa) > len(pb) || (len(pa) == len(pb) && pa > pb) {
		fmt.Println(sa)
	} else if len(pa) < len(pb) || (len(pa) == len(pb) && pa < pb) {
		fmt.Println(sb)
	} else {
		fmt.Println(-1)
	}
}
