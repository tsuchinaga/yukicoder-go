package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	if b-a >= 1 {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}

	fmt.Println(math.Abs(float64(a + 1 - b)))
}
