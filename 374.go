package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	if a < b {
		fmt.Println("K")
	} else {
		fmt.Println("S")
	}
}
