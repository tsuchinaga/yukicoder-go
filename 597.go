package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	for i := 0; i < n; i++ {
		var s string
		_, _ = fmt.Scan(&s)
		fmt.Print(s)
	}

	fmt.Printf("\n")
}
