package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	dest := make(map[int][]int, 0) // あるマスから行ける移動先
	for i := 1; i <= n; i++ {
		b := strings.Count(fmt.Sprintf("%b", i), "1")
		d := make([]int, 0)
		if i+b <= n {
			d = append(d, i+b)
		}
		if i-b >= 1 {
			d = append(d, i-b)
		}
		dest[i] = d
	}
	// fmt.Println(dest)

	type Node struct {
		n, depth int
	}
	stack := []Node{{1, 1}}
	// 幅優先で最初に見つかった答えが最短
	ans := -1
	for {
		if len(stack) == 0 { // 探索し終えた
			break
		}

		p := stack[0] // 現在地
		if p.n == n {
			ans = p.depth
			break
		}

		stack = stack[1:] // pop
		for _, d := range dest[p.n] {
			if _, ok := dest[d]; ok {
				stack = append(stack, Node{d, p.depth + 1})
			}
		}

		// 一度通ったところはdestから消す
		delete(dest, p.n)
		// fmt.Println(p, stack, dest)
	}

	fmt.Println(ans)
}
