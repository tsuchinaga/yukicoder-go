package main

import (
	"fmt"
	"sort"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	y := make([]int, 0)
	for i := 0; i <= a; i++ {
		for j := 0; j <= b; j++ {
			if !Contain542(y, i+5*j) {
				y = append(y, i+5*j)
			}
		}
	}
	sort.Slice(y, func(i, j int) bool {
		return y[i] < y[j]
	})

	for i := 1; i < len(y); i++ {
		fmt.Println(y[i])
	}
}

func Contain542(y []int, n int) bool {
	for _, v := range y {
		if v == n {
			return true
		}
	}
	return false
}
