package main

import "fmt"

func main() {
	var w, h, n, s, k int
	_, _ = fmt.Scan(&w, &h, &n)

	marks := map[int]int{}
	nums := map[int]int{}

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&s, &k)
		marks[s]++
		nums[k]++
	}

	ans := 0
	for i := 1; i <= w; i++ {
		if marks[i] > 0 {
			ans += h - marks[i]
		} else {
			ans += len(nums)
		}
	}

	fmt.Println(ans)
}
