package main

import (
	"fmt"
	"strconv"
)

func main() {
	var d int
	_, _ = fmt.Scan(&d)

	a := strconv.Itoa(d * 108)
	fmt.Printf("%s.%s\n", string(a[:len(a)-2]), string(a[len(a)-2:]))
}
