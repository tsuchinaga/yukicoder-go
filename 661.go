package main

import "fmt"

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)

		if a%8 == 0 && a%10 == 0 {
			fmt.Println("ikisugi")
		} else if a%8 == 0 {
			fmt.Println("iki")
		} else if a%10 == 0 {
			fmt.Println("sugi")
		} else {
			fmt.Println(a / 3)
		}
	}
}
