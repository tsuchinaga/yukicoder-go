package main

import (
	"fmt"
)

func main() {
	var h, n, m int
	_, _ = fmt.Scan(&m, &n)

	cnt := 1
	for i := 0; i < n-1; i++ {
		_, _ = fmt.Scan(&h)
		if h > m {
			cnt++
		}
	}

	d := "th"
	switch cnt % 10 {
	case 1:
		d = "st"
	case 2:
		d = "nd"
	case 3:
		d = "rd"
	}
	fmt.Printf("%d%s", cnt, d)
}
