package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)
	d := divisor415(n, m)
	fmt.Println(n/d - 1)
}

func divisor415(a, b int) int {
	if a%b == 0 {
		return b
	}
	return divisor415(b, a%b)
}
