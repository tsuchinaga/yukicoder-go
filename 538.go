package main

import (
	"fmt"
	"math"
)

func main() {
	var b1, b2, b3 float64
	_, _ = fmt.Scan(&b1, &b2, &b3)

	r := (b3 - b2) / (b2 - b1)
	d := b2 - r*b1
	fmt.Println(int(math.Round(r*b3 + d)))
}
