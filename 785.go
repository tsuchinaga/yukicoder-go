package main

import (
	"fmt"
	"strings"
)

func main() {
	var rl, gl, bl string
	_, _ = fmt.Scan(&rl, &gl, &bl)

	r, g, b := 256, 256, 256
	if rl != "NONE" {
		for i := 0; i < strings.Count(rl, ",")+1; i++ {
			r -= 31 - 2*i
		}
	}
	if gl != "NONE" {
		for i := 0; i < strings.Count(gl, ",")+1; i++ {
			g -= 31 - 2*i
		}
	}
	if bl != "NONE" {
		for i := 0; i < strings.Count(bl, ",")+1; i++ {
			b -= 31 - 2*i
		}
	}

	fmt.Println(r * g * b)
}
