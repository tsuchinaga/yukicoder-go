package main

import (
	"fmt"
	"math"
)

func main() {
	var x, y, l, a float64
	_, _ = fmt.Scan(&x, &y, &l)

	a = math.Ceil(math.Abs(x)/l) + math.Ceil(math.Abs(y)/l)

	if y < 0 {
		a += 2
	} else if x != 0 {
		a += 1
	}

	fmt.Println(int(a))
}
