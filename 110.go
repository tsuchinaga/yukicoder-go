package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	type Block struct {
		c, w int
	}
	blocks := make([]Block, 0)

	var n, w int
	for i := 0; i < 2; i++ {
		_, _ = fmt.Scan(&n)
		for j := 0; j < n; j++ {
			_, _ = fmt.Scan(&w)
			blocks = append(blocks, Block{i, w})
		}
	}

	sort.Slice(blocks, func(i, j int) bool {
		if blocks[i].w > blocks[j].w {
			return true
		} else if blocks[i].w == blocks[j].w && blocks[i].c > blocks[j].c {
			return true
		}
		return false
	})

	// 底が黒の場合と白の場合を確認
	ans := make([]int, 2)
	for i := 0; i < 2; i++ {
		c := i  // 土台の色(底の色はこれと違う色
		w := -1 // 長さ
		for _, b := range blocks {
			if c != b.c && (w == -1 || w > b.w) {
				c, w = b.c, b.w
				ans[i]++
			}
		}
	}
	fmt.Println(int(math.Max(float64(ans[0]), float64(ans[1]))))
}
