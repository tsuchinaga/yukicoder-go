package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	line := ""
	cnt := 0
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&line)
		cnt += len(strings.Replace(line, "R", "", -1))
	}

	fmt.Println(cnt)
}
