package main

import "fmt"

func main() {
	var l, k int
	_, _ = fmt.Scan(&l, &k)

	y := 0
	for {
		if l-k*2 <= 0 {
			break
		}
		y += k
		l -= k * 2
	}

	fmt.Println(y)
}
