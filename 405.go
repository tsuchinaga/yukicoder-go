package main

import (
	"fmt"
)

func main() {
	times := []string{"I", "II", "III", "IIII", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"}
	var s string
	var t int
	_, _ = fmt.Scan(&s, &t)

	n := 0
	for i, time := range times {
		if s == time {
			n = i + 1
		}
	}

	n = (n + t%12 + 12 - 1) % 12
	fmt.Println(times[n])
}
