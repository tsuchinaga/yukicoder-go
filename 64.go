package main

import "fmt"

func main() {
	var a, b, n int
	_, _ = fmt.Scan(&a, &b, &n)

	if n%3 == 0 {
		fmt.Println(a)
	} else if n%3 == 1 {
		fmt.Println(b)
	} else {
		fmt.Println(a ^ b)
	}
}
