package main

import (
	"fmt"
	"math"
)

func main() {
	var d int
	_, _ = fmt.Scan(&d)

	c := make([]int, 14*3)
	for i := 0; i < 2; i++ {
		line := ""
		_, _ = fmt.Scan(&line)
		for j, s := range line {
			if string(s) == "x" {
				c[14+i*7+j] = 0
			} else {
				c[14+i*7+j] = 1
			}
		}
	}

	max := 0
	for i := 0; i < len(c); i++ {
		tmp := make([]int, len(c))
		copy(tmp, c)
		for j := i; j < int(math.Min(float64(i+d), float64(len(c)))); j++ {
			if tmp[j] == 1 {
				break
			}
			tmp[j] = 1
		}
		// fmt.Println(tmp)

		cnt := 0
		for j := 0; j < len(tmp); j++ {
			cnt += tmp[j]
			if tmp[j] == 0 || j == len(tmp)-1 {
				if max < cnt {
					max = cnt
				}
				cnt = 0
			}
		}
	}

	fmt.Println(max)
}
