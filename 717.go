package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	var n, m int
	var s, t string
	_, _ = fmt.Scan(&n, &m, &s, &t)
	sa, sb := len(strings.Replace(s, "B", "", -1)), len(strings.Replace(s, "A", "", -1))
	ta, tb := len(strings.Replace(t, "B", "", -1)), len(strings.Replace(t, "A", "", -1))
	fmt.Println(int(math.Min(float64(sa), float64(ta)) + math.Min(float64(sb), float64(tb))))
}
