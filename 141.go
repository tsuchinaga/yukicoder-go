package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&m, &n)

	cnt := 0
	for n != m {
		if n > m {
			cnt++
			n, m = m, n
		} else {
			if m%n == 0 {
				cnt += m/n - 1
				m = n
			} else {
				cnt += m / n
				m -= m / n * n
			}
		}
		// fmt.Println(m, n, cnt)
	}
	fmt.Println(cnt)
}
