package main

import (
	"fmt"
	"math"
)

func main() {
	var n, k, a int
	_, _ = fmt.Scan(&n, &k)

	min := math.MaxInt64
	max := math.MinInt64
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		if a < min {
			min = a
		}
		if a > max {
			max = a
		}
	}

	fmt.Println(max - min)
}
