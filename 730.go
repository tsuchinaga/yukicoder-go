package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	sm := make(map[string]int, 0)
	for _, r := range []rune(s) {
		sm[string(r)]++
	}

	if len(sm) == len(s) {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
