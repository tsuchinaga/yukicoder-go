package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	t, _ := strconv.Atoi(sc.Text())

	for i := 0; i < t; i++ {
		sc.Scan()
		n, _ := strconv.Atoi(sc.Text())

		ls := make([]int, 0)
		nums := make(map[int]int, 0)
		for j := 0; j < n; j++ {
			sc.Scan()
			l, _ := strconv.Atoi(sc.Text())

			if _, ok := nums[l]; !ok {
				ls = append(ls, l)
			}
			nums[l]++
		}

		// lsを多い順に並び替え
		sort.Slice(ls, func(i, j int) bool {
			return (nums[ls[i]] > nums[ls[j]]) || (nums[ls[i]] == nums[ls[j]] && ls[i] < ls[j])
		})

		// 多いのから順に3つとって、なくなるまで繰り返す
		cnt := 0
		sets := 0
		for i := 0; i < len(ls); i++ {
			if nums[ls[i]] > 0 {
				sets++
				nums[ls[i]]--
				if sets == 3 {
					cnt++
					sets = 0
					i = -1

					// 時間かかるかもやけど、毎回並べなおしてみる
					sort.Slice(ls, func(i, j int) bool {
						return (nums[ls[i]] > nums[ls[j]]) || (nums[ls[i]] == nums[ls[j]] && ls[i] < ls[j])
					})
				}
			}
		}
		fmt.Println(cnt)
	}
}
