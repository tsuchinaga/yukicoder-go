package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	a := ""
	for {
		a = string('A'+n%26) + a
		n /= 26
		if n == 0 {
			break
		}
		n--
	}
	fmt.Println(a)
}
