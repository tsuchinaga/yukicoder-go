package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, k, a, cnt int
	sc := bufio.NewScanner(os.Stdin)

	_, _ = fmt.Scan(&n, &k)
	fmt.Println(1)
	for i := 0; i < n-1; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())

		if k < a {
			cnt++
		}
		fmt.Println(1 + cnt)
	}
}
