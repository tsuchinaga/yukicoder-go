package main

import (
	"fmt"
	"math"
	"math/big"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	sheep := func(i int) int {
		bi1 := big.NewInt(int64(i))
		bi2 := big.NewInt(int64(i))
		bi1.Mul(bi1, bi1)
		bi1.Add(bi1, bi2)
		bi1.Div(bi1, big.NewInt(2))

		return int(bi1.Int64())
	}
	i := 0
	min, max := 1, int(math.Pow10(9))*2
	for min != max {
		i = int(math.Ceil(float64(max-min)/2)) + min
		s := sheep(i)

		if s == n {
			min, max = i, i
		} else if s > n {
			max = i - 1
		} else {
			min = i
		}

		if max == min {
			i = max
		}
		// fmt.Println(min, max, i, sheep(i), n)
	}

	if n == sheep(i) {
		fmt.Println("YES")
		fmt.Println(i)
	} else {
		fmt.Println("NO")
	}
}
