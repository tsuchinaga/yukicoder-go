package main

import "fmt"

func main() {
	var c1, c2, c3 string
	_, _ = fmt.Scan(&c1, &c2, &c3)

	switch c2 {
	case "2":
		fmt.Println("4")
	case "3":
		fmt.Println("1")
	default:
		fmt.Println("14")
	}
}
