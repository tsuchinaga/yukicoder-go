package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c int
	_, _ = fmt.Scan(&a, &b, &c)
	if a == b || a == c || b == c {
		fmt.Println(0)
	} else if (b < a && b < c) || (b > a && b > c) {
		fmt.Println("INF")
	} else {
		max := int(math.Max(float64(a), math.Max(float64(b), float64(c))))
		cnt := 0
		for i := 3; i <= max; i++ {
			d, e, f := a%i, b%i, c%i
			if ((e < d && e < f) || (e > d && e > f)) && d != f {
				cnt++
			}
		}
		fmt.Println(cnt)
	}
}
