package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	paths := make([][]int, n)
	for i := 0; i < m; i++ {
		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		b, _ := strconv.Atoi(sc.Text())

		paths[a] = append(paths[a], b)
		paths[b] = append(paths[b], a)
	}

	// fmt.Println(paths)

	ans := 0
	for start := 0; start < n; start++ {
		// fmt.Println(start)

		for _, n1 := range paths[start] {
			// fmt.Println(start, n1)

			for _, n2 := range paths[n1] {
				// fmt.Println(start, n1, n2)

				if start == n2 {
					continue
				}

				no2 := false
				cnt2 := 0
				for _, n3 := range paths[n2] {
					// fmt.Println(start, n1, n2, n3)

					if n1 == n3 {
						continue
					}

					if start == n3 {
						no2 = true
						break
					}

					no3 := false
					cnt3 := 0
					for _, end := range paths[n3] {
						// fmt.Println(start, n1, n2, n3, end)

						if end == n2 {
							continue
						}

						if end == n1 {
							no3 = true
							break
						}

						if start == end {
							// fmt.Println("add cnt3")
							cnt3++
						}
					}

					if !no3 {
						// fmt.Println("add cnt2")
						cnt2 += cnt3
					}
				}

				if !no2 {
					// fmt.Println("add ans")
					ans += cnt2
				}
			}
		}
	}

	fmt.Println(ans / 8)
}
