package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	g := 0
	nums := make([]int, n)
	for i := range nums {
		_, _ = fmt.Scan(&a)
		nums[i] = a
		if i == 0 {
			g = a
		} else {
			g = Gcd736(g, a)
		}
	}

	s := ""
	for _, a := range nums {
		s += fmt.Sprintf("%d:", a/g)
	}
	fmt.Println(strings.Trim(s, ":"))
}

func Gcd736(a, b int) int {
	if b == 0 {
		return a
	}
	return Gcd736(b, a%b)
}
