package main

import "fmt"

func main() {
	var a int
	cards := make(map[int]int, 13)
	for i := 0; i < 5; i++ {
		_, _ = fmt.Scan(&a)
		cards[a-1]++
	}

	three := 0
	two := 0
	for _, n := range cards {
		if n == 3 {
			three++
		} else if n == 2 {
			two++
		}
	}

	if three == 1 && two == 1 {
		fmt.Println("FULL HOUSE")
	} else if three == 1 && two == 0 {
		fmt.Println("THREE CARD")
	} else if three == 0 && two == 2 {
		fmt.Println("TWO PAIR")
	} else if three == 0 && two == 1 {
		fmt.Println("ONE PAIR")
	} else {
		fmt.Println("NO HAND")
	}
}
