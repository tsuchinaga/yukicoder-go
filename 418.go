package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	fmt.Println(len(s) - len(strings.Replace(s, "n", "", -1)))
}
