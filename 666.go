package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	fmt.Println((a * b) % 1000000007)
}
