package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	t := strings.Replace(s, "(^^*)", "", -1)

	fmt.Printf("%d %d", (len(s)-len(t))/5, len(t)/5)
}
