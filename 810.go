package main

import "fmt"

func main() {
	var l, r, m int
	_, _ = fmt.Scan(&l, &r, &m)

	n := r - l + 1
	if n/m >= 1 {
		fmt.Println(m)
	} else {
		fmt.Println(n % m)
	}
}
