package main

import "fmt"

func main() {
	var n, a, x float64
	_, _ = fmt.Scan(&n, &a)

	sum := 0.0
	for i := 0.0; i < n; i++ {
		_, _ = fmt.Scan(&x)
		sum += x
	}

	if a == sum/n {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
