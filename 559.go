package main

import (
	"fmt"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	var n, cnt int
	for i, r := range []rune(s) {
		if string(r) == "A" {
			n, cnt = n+1, cnt+i-n
		}
	}

	fmt.Println(cnt)
}
