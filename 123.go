package main

import "fmt"

func main() {
	var n, m, a int
	_, _ = fmt.Scan(&n, &m)

	cards := make([]int, 0)
	for i := n; i >= 1; i-- {
		cards = append(cards, i)
	}

	for i := 0; i < m; i++ {
		_, _ = fmt.Scan(&a)
		a = n - a
		cards = append(cards[:a], append(cards[a+1:], cards[a])...)
	}
	fmt.Println(cards[len(cards)-1])
}
