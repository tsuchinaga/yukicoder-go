package main

import "fmt"

func main() {
	var n, k, a int
	_, _ = fmt.Scan(&n, &k)

	lists := make([][]int, k)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		lists[i%k] = append(lists[i%k], a)
	}

	// fmt.Println(lists)

	ans := 0
	for _, list := range lists {
		for {
			ok := true
			for i := 0; i < len(list)-1; i++ {
				if list[i] > list[i+1] {
					ans++
					list[i], list[i+1] = list[i+1], list[i]
					ok = false
				}
			}
			if ok {
				break
			}
		}
	}

	// fmt.Println(ans, lists)

	before := lists[0][0]
	for i := 0; i < n; i++ {
		if before > lists[i%k][i/k] {
			ans = -1
			break
		}
		before = lists[i%k][i/k]
	}

	fmt.Println(ans)
}
