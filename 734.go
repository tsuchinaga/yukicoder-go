package main

import "fmt"

func main() {
	var a, b, c int
	_, _ = fmt.Scan(&a, &b, &c)
	a, c = a*60, c*3600

	n := 0
	if a-b >= 0 {
		n = c / (a - b)
		if c%(a-b) != 0 {
			n++
		}
	} else {
		n = -1
	}
	fmt.Println(n)
}
