package main

import (
	"fmt"
)

func main() {
	var l int
	_, _ = fmt.Scan(&l)

	cnt := 0
	for i := 0; i < l-2; i++ {
		cnt += (l - i - 1) / 2
	}
	fmt.Println(cnt)
}
