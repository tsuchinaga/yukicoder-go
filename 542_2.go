package main

import "fmt"

func main() {
	var a, b, p int
	_, _ = fmt.Scan(&a, &b)

	stop := a
	if stop >= 4 {
		stop = 4
	}
	for i := 0; i < b; i++ {
		for j := 0; j <= stop; j++ {
			p := i*5 + j
			if p > 0 {
				fmt.Println(p)
			}
		}
	}

	for j := 0; j <= a; j++ {
		p = 5*b + j
		if p > 0 {
			fmt.Println(p)
		}
	}
}
