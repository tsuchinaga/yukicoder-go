package main

import "fmt"

func main() {
	var a, b string
	_, _ = fmt.Scan(&a, &b)
	if a+b == "00" || a+b == "22" {
		fmt.Println("E")
	} else if a == "0" || a == "1" || b == "0" || b == "1" {
		fmt.Println("S")
	} else {
		fmt.Println("P")
	}
}
