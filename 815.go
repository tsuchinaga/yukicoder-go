package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	fmt.Println(int(math.Ceil(float64(n) / 2)))
}
