package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	mod := int(math.Pow10(12))

	ans := 1
	maxLen := 1
	for i := 1; i <= n; i++ {
		ans = (ans * (i % mod)) % mod
		l := len(fmt.Sprint(ans))

		if maxLen < l {
			maxLen = l
		}

		// fmt.Println(i, ans, maxLen)
		if ans == 0 {
			break
		}
	}

	fmt.Printf("%0"+strconv.Itoa(maxLen)+"d\n", ans)
}
