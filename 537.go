package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	list := make([]string, 0)
	for i := 1; i <= int(math.Sqrt(float64(n))); i++ {
		if n%i == 0 {
			a := strconv.Itoa(i) + strconv.Itoa(n/i)
			if !Contain537(list, a) {
				list = append(list, a)
			}

			a = strconv.Itoa(n/i) + strconv.Itoa(i)
			if !Contain537(list, a) {
				list = append(list, a)
			}
		}
	}

	fmt.Println(len(list))
}

func Contain537(list []string, a string) bool {
	for _, b := range list {
		if a == b {
			return true
		}
	}
	return false
}
