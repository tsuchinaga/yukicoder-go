package main

import (
	"fmt"
	"strconv"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	total := 0
	for _, c := range s {
		n, err := strconv.Atoi(string(c))
		if err == nil {
			total += n
		}
	}

	fmt.Println(total)
}
