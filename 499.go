package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	s := ""
	for {
		if n/7 == 0 {
			fmt.Printf("%d%s\n", n%7, s)
			break
		}

		s = fmt.Sprintf("%d%s", n%7, s)
		n = n / 7
	}
}
