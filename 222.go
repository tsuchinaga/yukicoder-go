package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	var a, b, aop, bop, op int
	aop, bop = 1, 1
	for i, r := range []rune(s) {
		switch r {
		case '+':
			if i == 0 {
				aop = 1
			} else if op != 0 {
				bop = 1
			} else {
				op = -1
			}
		case '-':
			if i == 0 {
				aop = -1
			} else if op != 0 {
				bop = -1
			} else {
				op = 1
			}
		default:
			if op == 0 {
				a = a*10 + int(r-'0')
			} else {
				b = b*10 + int(r-'0')
			}
		}
	}
	// fmt.Println(aop, a, op, bop, b)
	fmt.Println(a*aop + op*b*bop)
}
