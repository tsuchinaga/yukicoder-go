package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)
	a, b := 0, 1
	for i := 3; i <= n; i++ {
		a, b = b, (a+b)%m
	}

	fmt.Println(b)
}
