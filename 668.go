package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	var a, b, c int
	var s string
	_, _ = fmt.Scan(&s)

	n, _ := strconv.Atoi(string(s[0:3]))
	n = int(math.Round(float64(n) / 10))
	t := strconv.Itoa(n)

	a = int(t[0] - '0')
	b = int(t[1] - '0')
	c = len(s) - 1 + len(t) - 2

	fmt.Printf("%d.%d*10^%d\n", a, b, c)
}
