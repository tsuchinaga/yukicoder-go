package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	l := 2 * n
	m := 0
	for {
		if n == 0 {
			break
		}
		m += n
		n /= 2
	}
	fmt.Println(l - m)
}
