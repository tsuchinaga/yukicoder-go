package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	var s string
	var t, r []string
	for i := 0; i < 2; i++ {
		for j := 0; j < n; j++ {
			_, _ = fmt.Scan(&s)
			if i == 0 {
				t = append(t, s)
			} else {
				r = append(r, s)
			}
		}
	}

	for i := 0; i < n; i++ {
		if t[i] != r[i] {
			fmt.Printf("%d\n%s\n%s\n", i+1, t[i], r[i])
		}
	}
}
