package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	rdr := bufio.NewReaderSize(os.Stdin, 1000000)
	buf := make([]byte, 0, 1000000)
	for {
		l, p, e := rdr.ReadLine()
		if e != nil {
			panic(e)
		}
		buf = append(buf, l...)
		if !p {
			break
		}
	}
	r := []rune(string(buf))
	ans := make([]int, n)

	indexes := make([]int, 0)
	for i, c := range r {
		if c == '(' {
			indexes = append(indexes, i)
		} else {
			ans[indexes[len(indexes)-1]], ans[i] = i, indexes[len(indexes)-1]
			indexes = indexes[:len(indexes)-1]
		}
		// fmt.Println(indexes, ans)
	}

	// fmt.Print(ans)

	for _, a := range ans {
		fmt.Println(a + 1)
	}
}
