package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	for i := 3; i <= int(math.Max(3, math.Sqrt(float64(n)))); i++ {
		if n%i == 0 {
			fmt.Println(i)
			return
		}
	}

	if n%2 == 0 && n > 4 {
		fmt.Println(n / 2)
	} else {
		fmt.Println(n)
	}
}
