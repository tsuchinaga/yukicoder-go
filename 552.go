package main

import "fmt"

func main() {
	var n string
	_, _ = fmt.Scan(&n)
	if n == "0" {
		fmt.Println(0)
	} else {
		fmt.Printf("%s0", n)
	}
}
