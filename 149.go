package main

import (
	"fmt"
	"math"
)

func main() {
	var aw, ab, bw, bb, c, d float64
	_, _ = fmt.Scan(&aw, &ab, &bw, &bb, &c, &d)

	ab = ab - c
	if ab < 0 {
		aw, bw = aw+ab, bw-ab
	}

	bw, aw = bw-d, aw+math.Min(bw, d)

	fmt.Println(aw)
}
