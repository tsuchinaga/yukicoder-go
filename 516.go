package main

import "fmt"

func main() {
	var b string
	bs := make([]int, 2)
	for i := 0; i < 3; i++ {
		_, _ = fmt.Scan(&b)
		if b == "RED" {
			bs[0]++
		} else {
			bs[1]++
		}
	}

	if bs[0] >= 2 {
		fmt.Println("RED")
	} else {
		fmt.Println("BLUE")
	}
}
