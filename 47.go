package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	if n == 1 {
		fmt.Println(0)
	} else {
		fmt.Println(len(fmt.Sprintf("%b", n-1)))
	}
}
