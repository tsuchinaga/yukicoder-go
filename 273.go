package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)
	r := []rune(s)

	ans := 0
	for i := 0; i < len(s); i++ {
		for j := i; j < len(s); j++ {
			if r[i] != r[j] {
				continue
			}

			kaibun := true
			for k := 1; k <= (j-i)/2; k++ {
				if r[i+k] != r[j-k] {
					kaibun = false
					break
				}
			}

			if kaibun {
				l := j - i + 1
				if l != len(r) && ans < l {
					ans = l
					// fmt.Println(i, string(r[i]), j, string(r[j]), ans)
				}
			}
		}
	}
	fmt.Println(ans)
}
