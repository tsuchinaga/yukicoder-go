package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	min := -1
	for i, c := range s {
		if string(c) != "c" {
			continue
		}

		w := 0
		cnt := 0
		for _, c := range s[i:] {
			cnt++
			if string(c) == "w" {
				w++
				if w == 2 {
					if min < 0 || min > cnt {
						min = cnt
					}
					break
				}
			}
		}
	}

	fmt.Println(min)
}
