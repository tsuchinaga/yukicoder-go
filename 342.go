package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var s, t string
	var w int

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanLines)
	sc.Scan()
	s = sc.Text()

	max := 1
	ans := make([]string, 0)
	for _, r := range []rune(s) {
		if string(r) == "ｗ" {
			w++
		} else {
			if w != 0 {
				if max < w && t != "" {
					ans = []string{t}
					max = w
				} else if max == w && t != "" {
					ans = append(ans, t)
				}

				t = ""
				w = 0
			}
			t += string(r)
		}
	}

	if max < w && t != "" {
		ans = []string{t}
		max = w
	} else if max == w && t != "" {
		ans = append(ans, t)
	}

	// fmt.Println(ans)

	for _, str := range ans {
		fmt.Println(str)
	}
}
