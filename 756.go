package main

import (
	"fmt"
	"strconv"
)

func main() {
	var d int
	_, _ = fmt.Scan(&d)

	n := 0
	m := 0
	for {
		n++
		s := strconv.Itoa(n)
		if len(s)+m >= d {
			fmt.Println(string(s[d-m-1]))
			break
		}
		m += len(s)
	}
}
