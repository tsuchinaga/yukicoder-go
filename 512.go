package main

import "fmt"

func main() {
	var x, y, a, n int
	_, _ = fmt.Scan(&x, &y, &n)

	c := make([]int, n)
	for i := range c {
		_, _ = fmt.Scan(&a)
		c[i] = a
	}

	for i := 0; i < n-1; i++ {
		if c[i]*y > c[i+1]*x {
			fmt.Println("NO")
			return
		}
	}
	fmt.Println("YES")
}
