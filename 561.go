package main

import (
	"fmt"
	"math"
)

func main() {
	var n, d, t, k int
	_, _ = fmt.Scan(&n, &d)

	T := 0      // 東京での最高値
	K := d * -1 // 京都での最高値
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&t, &k)
		tmpT, tmpK := T, K

		if tmpT+t > tmpK+t-d {
			T = tmpT + t
		} else {
			T = tmpK + t - d
		}

		if tmpK+k > tmpT+k-d {
			K = tmpK + k
		} else {
			K = tmpT + k - d
		}

		// fmt.Println(T, K)
	}

	fmt.Println(int(math.Max(float64(T), float64(K))))
}
