package main

import (
	"fmt"
)

func main() {
	var x1, y1, x2, y2, x3, y3 float64
	_, _ = fmt.Scan(&x1, &y1, &x2, &y2, &x3, &y3)

	sa := (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)
	sb := (x2-x3)*(x2-x3) + (y2-y3)*(y2-y3)
	sc := (x3-x1)*(x3-x1) + (y3-y1)*(y3-y1)

	// fmt.Println(sa, sb, sc)

	// 3辺の長さがすべて同じか、すべて違う場合は正方形を作れない
	if (sa == sb && sb == sc && sc == sa) || (sa != sb && sb != sc && sc != sa) {
		fmt.Println(-1)
		return
	}

	// 4つ目の点の位置
	var x4, y4 float64
	if sa == sb && sa*2 == sc {
		x4, y4 = x3+x1-x2, y3+y1-y2
	} else if sb == sc && sb*2 == sa {
		x4, y4 = x1+x2-x3, y1+y2-y3
	} else if sc == sa && sc*2 == sb {
		x4, y4 = x2+x3-x1, y2+y3-y1
	} else {
		fmt.Println(-1)
		return
	}
	// fmt.Println(x4, y4)

	// 方眼紙にかけるか = 整数かのチェック
	if x4 == float64(int(x4)) && y4 == float64(int(y4)) {
		fmt.Printf("%.0f %.0f", x4, y4)
	} else {
		fmt.Println(-1)
	}
}
