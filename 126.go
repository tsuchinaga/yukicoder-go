package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, s, ans int
	_, _ = fmt.Scan(&a, &b, &s)

	if s > 1 { // 2階以上にいるとき
		// 近いほうが迎えにくる
		if math.Abs(float64(a-s)) <= math.Abs(float64(b-s)) {
			ans += int(math.Abs(float64(a - s)))
			a = s
		} else {
			ans += int(math.Abs(float64(b - s)))
			b = s
		}

		// bに乗っている場合、Aのいる階にいって地下に行くのと、1階にいってAを呼んで地下に行くのと早い方を選択する
		// bに乗っている場合、Aのいる階に、行けなければ1階に行く
		if s == b {
			if a == 0 {
				ans += b - 1
				b, s = 1, 1
			} else {
				if math.Abs(float64(a-s)) < math.Abs(float64(s-1)) { // Aに行く方が近い
					ans += int(math.Abs(float64(a - s)))
					b, s = a, a
				} else { // 1階に行く方が近い
					ans += s - 1
					b, s = 1, 1
				}
			}
		}
	}

	// Aのエレベータに乗って目的地に行く
	if s != a {
		ans += int(math.Abs(float64(a - s)))
		a = s
	}
	ans += a

	fmt.Println(ans)
}
