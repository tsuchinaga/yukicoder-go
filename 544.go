package main

import "fmt"

func main() {
	var n string
	_, _ = fmt.Scan(&n)

	var a, b int
	for _, c := range n {
		a *= 10
		b *= 10

		if c == '7' {
			a += 3
			b += 4
		} else {
			a += int(c - '0')
		}
	}

	fmt.Printf("%d %d\n", a, b)
}
