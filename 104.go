package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scanln(&s)

	n := 1
	for _, c := range s {
		if string(c) == "L" {
			n += n
		} else {
			n += n + 1
		}
	}

	fmt.Println(n)
}
