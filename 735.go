package main

import (
	"fmt"
	"math"
)

func main() {
	var r, d float64
	_, _ = fmt.Scan(&r, &d)
	fmt.Printf("%.6f", math.Sqrt(d*d-r*r))
}
