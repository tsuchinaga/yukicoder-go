package main

import "fmt"

func main() {
	var n, a, b, c int
	_, _ = fmt.Scan(&n)

	price := make([]int, n-1)
	for i := range price {
		_, _ = fmt.Scan(&a)
		price[i] = a
	}

	// fmt.Println(price)
	people := 0
	ans := 0
	for i := 0; i < n; i++ {
		if i != 0 {
			ans += people * price[i-1]
		}
		_, _ = fmt.Scan(&b, &c)
		people += c - b

		// fmt.Println(ans, people)
	}
	fmt.Println(ans)
}
