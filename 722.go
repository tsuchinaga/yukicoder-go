package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var a, b string
	_, _ = fmt.Scan(&a, &b)

	ah, _ := strconv.Atoi(strings.Trim(a, "0"))
	bh, _ := strconv.Atoi(strings.Trim(b, "0"))
	ac, bc := strings.Count(a, "0"), strings.Count(b, "0")
	if -9 <= ah && ah <= 9 && -9 <= bh && bh <= 9 && ac >= 2 && bc >= 2 {
		fmt.Printf("%d%s\n", ah*bh, strings.Repeat("0", ac+bc-1))
	} else {
		an, _ := strconv.Atoi(a)
		bn, _ := strconv.Atoi(b)
		ans := an * bn
		if ans < -99999999 || 99999999 < ans {
			fmt.Println("E")
		} else {
			fmt.Println(ans)
		}
	}
}
