package main

import (
	"fmt"
	"math"
)

func main() {
	var px, py, qx, qy float64
	_, _ = fmt.Scan(&px, &py, &qx, &qy)
	fmt.Println((math.Abs(qx-px) + math.Abs(qy-py)) / 2)
}
