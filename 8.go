package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	var p, n, k int
	sc.Scan()
	p, _ = strconv.Atoi(sc.Text())
	for i := 0; i < p; i++ {
		sc.Scan()
		n, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		k, _ = strconv.Atoi(sc.Text())

		l := (n - 1) % (k + 1)
		if l != 0 && l <= k {
			fmt.Println("Win")
		} else {
			fmt.Println("Lose")
		}
	}
}
