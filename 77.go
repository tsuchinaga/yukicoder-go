package main

import (
	"fmt"
	"math"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	total := 0
	nums := make([]int, n)
	for i := range nums {
		_, _ = fmt.Scan(&a)
		nums[i] = a
		total += a
	}
	// fmt.Println(total)
	// fmt.Println(nums)

	ans := math.MaxInt64
	for i := int(math.Sqrt(float64(total))); i >= 1; i-- { // 最大の高さを小さくしていく
		w := 2*i - 1
		expect := make([]int, w)
		for j := 0; j < i; j++ {
			expect[j] = j + 1
			expect[w-j-1] = j + 1
		}
		// fmt.Println(i, w)
		// fmt.Println(expect)

		// 理想との差異を確認
		d := 0
		for j := 0; j < int(math.Max(float64(n), float64(w))); j++ {
			if j < n && j < w { // 両方ある
				if nums[j] > expect[j] {
					d += nums[j] - expect[j]
				}
			} else if j < n { // numsだけある
				d += nums[j]
			}
		}

		// fmt.Println(d)
		if ans > d {
			ans = d
		}
	}
	fmt.Println(ans)
}
