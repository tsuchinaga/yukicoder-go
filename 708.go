package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	ans := 0
	mark := 1
	outside := 1
	for _, r := range []rune(s) {
		n := r - '0'
		switch n {
		case -5:
			mark = 1
		case -3:
			mark = -1
		case -8:
			outside = mark
			mark = 1
		case -7:
			outside = 1
		default:
			// fmt.Println(int(n), mark, outside, int(n)*mark*outside)
			ans += int(n) * mark * outside
		}
		// fmt.Println(n)
	}
	fmt.Println(ans)
}
