package main

import "fmt"

func main() {
	var a, b, c, x, y, z int
	_, _ = fmt.Scanf("%d.%d.%d", &a, &b, &c)
	_, _ = fmt.Scanf("%d.%d.%d", &x, &y, &z)

	if a*1000*1000+b*1000+c >= x*1000*1000+y*1000+z {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
