package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c, d int
	_, _ = fmt.Scan(&a, &b, &c, &d)

	if d >= 10 {
		fmt.Println("Impossible")
	} else {
		fmt.Println("Possible")
		s := 0
		l := int(math.Ceil(float64(a+b) / 100))
		for i := 0; i <= l; i++ {
			t := int(math.Pow(2, float64(i))) // 倍率
			if b >= 100 {
				b, s = b-100, s+100*50*t
			} else {
				combo := b
				b, s = 0, s+b*50*t

				if a >= 100-combo {
					a, s = a-(100-combo), s+(100-combo)*100*t
				} else {
					a, s = 0, s+a*100*t
				}
			}
		}
		fmt.Println(s)
	}
}
