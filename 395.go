package main

import "fmt"

func main() {
	var a int
	_, _ = fmt.Scan(&a)

	if a-7 < 8 {
		fmt.Println(-1)
	} else {
		fmt.Println(a - 7)
	}
}
