package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)
	r := []rune(s)

	max := 1
	for i := 0; i < len(r)-1; i++ {
		// fmt.Println("start", i, string(r[i]))

		for j := i + 1; j < len(r); j++ {
			if r[i] == r[j] {
				// fmt.Println(i, string(r[i]), j, string(r[j]))

				cnt := 2
				for k := 1; k <= (j-i)/2; k++ {
					if r[i+k] == r[j-k] {
						if i+k != j-k {
							cnt++
						}
						cnt++
					}
				}

				// fmt.Println(cnt)
				if max < cnt {
					max = cnt
				}
			}
		}

		// この時点で出うる最長なら終了する
		if len(r)-i <= max {
			break
		}
	}

	fmt.Println(max)
}
