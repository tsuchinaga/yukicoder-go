package main

import (
	"fmt"
	"math"
)

func main() {
	s := ""
	_, _ = fmt.Scan(&s)

	var tn, rn, en float64
	for _, r := range []rune(s) {
		switch string(r) {
		case "t":
			tn++
		case "r":
			rn++
		case "e":
			en++
		}
	}

	en = math.Floor(en / 2)
	fmt.Println(math.Min(math.Min(tn, rn), en))
}
