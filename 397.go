package main

import (
	"fmt"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	nums := make([]int, n)
	for i := range nums {
		_, _ = fmt.Scan(&a)
		nums[i] = a
	}

	change := make([]string, 0)
	for i := 0; i < len(nums)-1; i++ {
		min, minJ := -1, -1
		for j := i; j < len(nums); j++ {
			if min == -1 || min > nums[j] {
				min = nums[j]
				minJ = j
			}
		}
		if minJ != -1 && min < nums[i] {
			change = append(change, fmt.Sprintf("%d %d", i, minJ))
			nums[i], nums[minJ] = nums[minJ], nums[i]
		}
	}

	// fmt.Println(nums)
	fmt.Println(len(change))
	for _, c := range change {
		fmt.Println(c)
	}
	_, _ = fmt.Scan(&n)
}
