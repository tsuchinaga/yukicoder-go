package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	bs := bufio.NewScanner(os.Stdin)
	bs.Split(bufio.ScanWords)

	nums := make(map[int]int, 0) // 数字がどこにあるか
	for i := 0; i < n; i++ {
		bs.Scan()
		a, _ := strconv.Atoi(bs.Text())
		nums[a] = i
	}
	// fmt.Println(nums)

	// 順番通りじゃないところがどこかを見つけて、そこから下全部を一度ずつ入れ替える
	cnt := 0
	for i := n; i > 1; i-- {
		if nums[i] < nums[i-1] {
			cnt += i - 1
			break
		}
	}

	fmt.Println(cnt)
}
