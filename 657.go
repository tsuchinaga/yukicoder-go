package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n)
	mod := 17

	sc := bufio.NewScanner(os.Stdin)
	nacci := map[int]int{1: 0, 2: 0, 3: 0, 4: 1}
	last := 4
	for i := 0; i < n; i++ {
		sc.Scan()
		m, _ = strconv.Atoi(sc.Text())

		if num, ok := nacci[m]; ok {
			fmt.Println(num)
		} else {
			for j := last + 1; j <= m; j++ {
				nacci[j] = (nacci[j-1] + nacci[j-2] + nacci[j-3] + nacci[j-4]) % mod
			}
			last = m
			fmt.Println(nacci[m])
		}
	}
}
