package main

import (
	"fmt"
	"strings"
)

func main() {
	s := ""
	_, _ = fmt.Scan(&s)

	c := map[string]string{"I": "1", "l": "1", "O": "0", "o": "0"}
	for k, v := range c {
		s = strings.Replace(s, k, v, -1)
	}

	fmt.Println(s)
}
