package main

import "fmt"

func main() {
	var a, b, c, d int
	_, _ = fmt.Scan(&a, &b, &c, &d)

	ans := 0
	for i := a; i <= b; i++ {
		ans += d - c

		if !(c <= i && i <= d) {
			ans++
		}
	}
	fmt.Println(ans)
}
