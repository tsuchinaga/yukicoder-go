package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, k, x int
	_, _ = fmt.Scan(&n, &k, &x)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	// 初期状態を構築
	be := make([]int, n)
	for i := range be {
		be[i] = i
	}

	// xより前の最後の状態を作りながら、xより後の入れ替えを取得
	type change struct {
		a, b int
	}
	afChanges := make([]change, k-x)
	for i := 0; i < k; i++ {
		if x == i+1 {
			sc.Scan()
			sc.Scan()
			continue
		}

		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		b, _ := strconv.Atoi(sc.Text())

		if i < x {
			be[a-1], be[b-1] = be[b-1], be[a-1]
		} else {
			afChanges[k-i-1] = change{a - 1, b - 1}
		}
	}

	// fmt.Println("be", be)
	// fmt.Println(afChanges)

	// 最終状態を取得
	af := make([]int, n)
	for i := range af {
		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())
		af[i] = a - 1
	}

	// fmt.Println("af", af)

	// xより後の入れ替えを最終状態に実行し、x直後の状態を取得
	for _, c := range afChanges {
		af[c.a], af[c.b] = af[c.b], af[c.a]
	}

	a, b := -1, -1
	for i := range be {
		if be[i] != af[i] {
			if a == -1 {
				a = i + 1
			} else {
				b = i + 1
				break
			}
		}
	}
	fmt.Printf("%d %d\n", a, b)
}
