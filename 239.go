package main

import "fmt"

func main() {
	n := 0
	s, t := "", "nyanpass"
	_, _ = fmt.Scan(&n)

	g := make([][]string, n)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			_, _ = fmt.Scan(&s)

			if i == j {
				continue
			}
			g[j] = append(g[j], s)
		}
	}

	m := -2
	for i, gg := range g {
		r := true
		for _, ggg := range gg {
			if t != ggg {
				r = false
				break
			}
		}

		if r {
			if m < 0 {
				m = i
			} else if m != i {
				fmt.Println(-1)
				return
			}
		}
	}
	fmt.Println(m + 1)
}
