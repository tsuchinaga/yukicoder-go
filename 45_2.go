package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	n, _ := strconv.Atoi(sc.Text())

	var a, b, c, larger int // 取った、取った後やから取れへん、取れるけど取らへん
	for i := 0; i < n; i++ {
		sc.Scan()
		v, _ := strconv.Atoi(sc.Text())

		// fmt.Println(a, b, c, v)
		larger = b
		if b < c {
			larger = c
		}
		a, b, c = larger+v, a, larger
	}

	ans := a
	if a < b {
		ans = b
	}
	fmt.Println(ans)
}
