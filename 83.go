package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	if n%2 == 0 {
		fmt.Println(strings.Repeat("1", n/2))
	} else {
		fmt.Print("7")
		fmt.Println(strings.Repeat("1", (n-3)/2))
	}
}
