package main

import (
	"fmt"
	"math"
)

func main() {
	var g, c, p, ans int
	var s string
	_, _ = fmt.Scan(&g, &c, &p, &s)

	var sg, sc, sp int
	for _, char := range s {
		switch char {
		case 'G':
			if p > 0 {
				ans += 3
				p--
			} else {
				sg++
			}
		case 'C':
			if g > 0 {
				ans += 3
				g--
			} else {
				sc++
			}
		case 'P':
			if c > 0 {
				ans += 3
				c--
			} else {
				sp++
			}
		}
	}

	ans += int(math.Min(float64(g), float64(sg)))
	ans += int(math.Min(float64(c), float64(sc)))
	ans += int(math.Min(float64(p), float64(sp)))
	fmt.Println(ans)
}
