package main

import "fmt"

func main() {
	n := 0
	s := ""
	for {
		fmt.Printf("%03d\n", n)
		_, _ = fmt.Scan(&s)
		if s == "unlocked" {
			break
		}
		n++
	}
}
