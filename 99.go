package main

import (
	"fmt"
	"math"
)

func main() {
	var n, x int
	_, _ = fmt.Scan(&n)

	odd := 0
	even := 0
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&x)
		if x%2 == 0 {
			even++
		} else {
			odd++
		}
	}
	fmt.Println(math.Abs(float64(odd - even)))
}
