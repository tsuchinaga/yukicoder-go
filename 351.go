package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var h, w, n int
	_, _ = fmt.Scan(&h, &w, &n)

	type c struct {
		s string
		k int
	}

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	controls := make([]c, n)
	for i := range controls {
		sc.Scan()
		controls[i].s = sc.Text()

		sc.Scan()
		controls[i].k, _ = strconv.Atoi(sc.Text())
	}

	var x, y int
	for i := n - 1; i >= 0; i-- {
		if controls[i].s == "R" && controls[i].k == y {
			x = (x + w - 1) % w
		} else if controls[i].s == "C" && controls[i].k == x {
			y = (y + h - 1) % h
		}
		// fmt.Println(controls[i], x, y)
	}
	// fmt.Println(x, y)

	ans := (x + y) % 2
	if ans == 1 {
		fmt.Println("black")
	} else {
		fmt.Println("white")
	}
}
