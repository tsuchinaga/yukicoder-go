package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	s := sc.Text()

	if n == 1 || (n == 2 && s[0] != s[1]) || (n == 3 && s[0] == s[2] && s[0] != s[1]) {
		fmt.Println("NO")
	} else {
		fmt.Println("YES")
	}
}
