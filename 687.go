package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	fmt.Printf("%d %d\n", n/2, n-n/2)
}
