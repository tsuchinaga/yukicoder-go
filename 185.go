package main

import "fmt"

func main() {
	var n, x, y, m int
	r := true
	_, _ = fmt.Scan(&n)
	_, _ = fmt.Scan(&x, &y)
	m = y - x
	if y-x <= 0 {
		r = false
	}

	for i := 1; i < n; i++ {
		_, _ = fmt.Scan(&x, &y)

		if r && m != y-x {
			r = false
		}
	}

	if r {
		fmt.Println(m)
	} else {
		fmt.Println(-1)
	}
}
