package main

import (
	"fmt"
)

func main() {
	var xa, ya, xb, yb int
	_, _ = fmt.Scan(&xa, &ya, &xb, &yb)

	// 点Aを第2象限に移す
	xa *= -1
	fmt.Printf("%.7f\n", float64(ya)-float64(yb-ya)*float64(xa)/float64(xb-xa))
}
