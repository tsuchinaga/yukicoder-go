package main

import (
	"fmt"
	"math"
)

func main() {
	var w, h, d int
	_, _ = fmt.Scan(&w, &h, &d)

	// マンハッタン距離分進んだ地点
	var x, y int
	if d <= w {
		x, y = d, 0
	} else {
		x, y = w, d-w
	}

	if x < 0 || w < x || y < 0 || h < y { // 存在しない地点に到達している場合は失敗
		fmt.Println(0)
	} else {
		fmt.Println(int(math.Min(float64(x), float64(h-y))) + 1)
	}
}
