package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	mod := []int{1, 0, -1, 0}

	sc.Scan()
	d, _ := strconv.Atoi(sc.Text())
	nums := make([]int, d+1)
	for i := d; i >= 0; i-- {
		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())
		nums[i] = a
	}

	// fmt.Println(nums)
	// fmt.Println(mod)

	for i := 0; i <= len(nums)-len(mod); i++ {
		b := nums[i]
		nums[i], nums[i+1], nums[i+2], nums[i+3] = nums[i]-mod[0]*b, nums[i+1]-mod[1]*b, nums[i+2]-mod[2]*b, nums[i+3]-mod[3]*b
		// fmt.Println(b, nums)
	}

	// fmt.Println(nums)
	e := 0
	for i, n := range nums {
		if n != 0 {
			e = d - i
			break
		}
	}

	s := strconv.Itoa(nums[d])
	for i := d - 1; i >= d-e; i-- {
		s += " " + strconv.Itoa(nums[i])
	}

	fmt.Println(e)
	fmt.Println(s)
}
