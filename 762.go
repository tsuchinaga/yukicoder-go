package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n, m int
	s := ""
	_, _ = fmt.Scan(&n, &m, &s)
	r := []rune(s)
	pdca := []rune("PDCA")
	mod := int(math.Pow(10, 9)) + 7

	sr := make(map[int][]int, 0) // PDCAの位置
	for i, c := range r {
		if c == pdca[0] {
			sr[0] = append(sr[0], i)
		} else if c == pdca[1] {
			sr[1] = append(sr[1], i)
		} else if c == pdca[2] {
			sr[2] = append(sr[2], i)
		} else {
			sr[3] = append(sr[3], i)
		}
	}

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	// P -> D, D -> C, C -> Aのパスのみを取得する
	path := make(map[int][]int, 0) // from -> to
	for i := 0; i < m; i++ {
		sc.Scan()
		n1, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		n2, _ := strconv.Atoi(sc.Text())

		// fmt.Println(n1, string(r[n1-1]), n2, string(r[n2-1]))

		if (r[n1-1] == pdca[0] && r[n2-1] == pdca[1]) ||
			(r[n1-1] == pdca[1] && r[n2-1] == pdca[2]) ||
			(r[n1-1] == pdca[2] && r[n2-1] == pdca[3]) {
			path[n1-1] = append(path[n1-1], n2-1)
		} else if (r[n2-1] == pdca[0] && r[n1-1] == pdca[1]) ||
			(r[n2-1] == pdca[1] && r[n1-1] == pdca[2]) ||
			(r[n2-1] == pdca[2] && r[n1-1] == pdca[3]) {
			path[n2-1] = append(path[n2-1], n1-1)
		}
	}

	// fmt.Println(path)

	// A, D, C, Pの順に、移動先が何通りになるかの評価
	// Aの移動先はないので、常に1通りになる
	// この時10^9 + 7を超える場合は余りを保持するようにする
	p := make([]int, n)
	ans := 0
	for i := len(pdca) - 1; i >= 0; i-- {
		for _, j := range sr[i] {
			if string(r[j]) == "A" {
				p[j] = 1
			} else {
				sum := 0
				for _, k := range path[j] {
					sum += p[k]
				}
				p[j] = sum % mod

				if i == 0 {
					ans += p[j]
				}
			}
		}
	}

	fmt.Println(ans % mod)
}
