package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)
	l := make([]int, n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	for i := range l {
		sc.Scan()
		l[i], _ = strconv.Atoi(sc.Text())
	}

	// sort.Slice(l, func(i, j int) bool {
	// 	if l[i] < l[j] {
	// 		k--
	// 		return true
	// 	}
	// 	return false
	// })
	for i := 0; i < n; i++ {
		for {
			if i+1 == l[i] {
				break
			}
			l[i], l[l[i]-1] = l[l[i]-1], l[i]
			k--
		}
	}

	// fmt.Println(k, l)

	if k >= 0 && k%2 == 0 {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
