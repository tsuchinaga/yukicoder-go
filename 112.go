package main

import "fmt"

func main() {
	var n, a, b, c int
	_, _ = fmt.Scan(&n)
	as := make(map[int]int, 0)

	max := 0
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		as[a]++
		if max < a {
			max = a
		}
	}

	if len(as) == 2 {
		b, c = as[max], n-as[max]
	} else {
		if max/(n-1) == 2 {
			b = n
		} else {
			c = n
		}
	}
	fmt.Printf("%d %d\n", b, c)
}
