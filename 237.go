package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	var a int
	_, _ = fmt.Scan(&a)

	fp := []int{3, 5, 17, 257, 65537}

	ans := 0
	m := 0
	for len(fp) > 0 {
		n := int(math.Pow(2, float64(m)))

		// 単体でaを超えるようなfpは要らないので消す
		for i := len(fp) - 1; i >= 0; i-- {
			if n*fp[i] > a {
				fp = fp[:len(fp)-1]
			}
		}

		// fmt.Println(m, fp)

		// 2だけ特別扱い
		k := n * 2
		if 3 <= k && k <= a {
			// fmt.Println(k)
			ans++
		}

		// fpの組み合わせ
		lim := int(math.Pow(2, float64(len(fp))))
		for i := 1; i < lim; i++ {
			k = n
			for j, c := range fmt.Sprintf("%0"+strconv.Itoa(len(fp))+"b", i) {
				if c == '1' {
					k *= fp[j]
				}
			}

			if 3 <= k && k <= a {
				// fmt.Println(k)
				ans++
			}
		}

		// fmt.Println(m, ans)
		m++
	}

	fmt.Println(ans)
}
