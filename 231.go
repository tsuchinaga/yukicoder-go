package main

import "fmt"

func main() {
	var n, g, d int
	_, _ = fmt.Scan(&n)

	for i := 1; i <= n; i++ {
		_, _ = fmt.Scan(&g, &d)

		if (g-d*30000)*6 >= 30000*100 {
			fmt.Println("YES")
			for j := 0; j < 6; j++ {
				fmt.Println(i)
			}
			return
		}
	}

	fmt.Println("NO")
}
