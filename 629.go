package main

import "fmt"

func main() {
	var n, m, a, b int
	_, _ = fmt.Scan(&n, &m)

	nodes := make([]int, n)
	for i := range nodes {
		_, _ = fmt.Scan(&a)
		nodes[i] = a
	}

	paths := make(map[int][]int, 0)
	for i := 0; i < m; i++ {
		_, _ = fmt.Scan(&a, &b)
		a--
		b--
		paths[a] = append(paths[a], b)
		paths[b] = append(paths[b], a)
	}

	// fmt.Println(nodes, paths)

	for i, x1 := range nodes {
		for _, j := range paths[i] {
			x2 := nodes[j]
			if x1 == x2 {
				continue
			}

			for _, k := range paths[j] {
				x3 := nodes[k]
				// fmt.Println(x1, x2, x3)
				if i == k || x1 == x3 || x2 == x3 || (x1 < x2 && x2 < x3) || (x1 > x2 && x2 > x3) {
					continue
				}

				fmt.Println("YES")
				return
			}
		}
	}

	fmt.Println("NO")
}
