package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, d int
	_, _ = fmt.Scan(&n, &d)

	ans := []rune(strings.Repeat("C", n))

	// 穴の数が許す限り、先頭からAに変えていく
	for i := 0; i < n; i++ {
		if d <= 0 {
			break
		}

		ans[i] = 'A'
		d--
	}

	// 穴の数が許す限り、後ろからBに変えていく
	for i := n - 1; i >= 0; i-- {
		if d <= 0 {
			break
		}

		ans[i] = 'B'
		d--
	}

	fmt.Println(string(ans))
}
