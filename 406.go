package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	nums := make([]int, n)
	for i := range nums {
		sc.Scan()
		nums[i], _ = strconv.Atoi(sc.Text())
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	diff := nums[1] - nums[0]
	for i := range nums {
		if i < 2 {
			continue
		}

		if diff == 0 || nums[i]-nums[i-1] != diff {
			fmt.Println("NO")
			return
		}
	}

	fmt.Println("YES")
}
