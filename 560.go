package main

import (
	"fmt"
)

func main() {
	var m, n int
	_, _ = fmt.Scan(&m, &n)
	fmt.Println(fmt.Sprintf("%.8f", float64(m)+float64(n/3)+float64(n%3)/3))
}
