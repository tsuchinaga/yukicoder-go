package main

import (
	"fmt"
)

func main() {
	var x int
	_, _ = fmt.Scan(&x)

	d := make(map[int]int, 0)
	i := 2
	for {
		if x%i == 0 {
			d[i]++
			x /= i
		} else {
			i++
		}

		if x == 1 || i*i > x {
			d[x]++
			break
		}
	}

	ans := 1
	for k, v := range d {
		if v%2 == 1 {
			ans *= k
		}
	}
	fmt.Println(ans)
}
