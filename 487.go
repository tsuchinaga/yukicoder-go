package main

import (
	"fmt"
)

func main() {
	var m int
	_, _ = fmt.Scan(&m)

	d := 2017 % m
	e := (d * d) % m
	ans := e
	for i := 0; i < 2017-1; i++ {
		ans = (ans * e) % m
	}

	ans = (ans + d) % m
	fmt.Println(ans)
}
