package main

import "fmt"

func main() {
	var a, b int
	var s string
	_, _ = fmt.Scan(&a, &b, &s)

	fmt.Printf("%d %s\n", a+b, s)
}
