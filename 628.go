package main

import "fmt"

func main() {
	var n, no, m, s int
	_, _ = fmt.Scan(&n)

	var t string
	tags := make(map[string]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&no, &m, &s)
		for j := 0; j < m; j++ {
			_, _ = fmt.Scan(&t)
			tags[t] += s
		}
	}

	for i := 0; i < 10; i++ {
		if len(tags) <= 0 {
			break
		}

		max := 0
		maxKey := ""
		for k, v := range tags {
			if max < v {
				max, maxKey = v, k
			} else if max == v && maxKey > k {
				maxKey = k
			}
		}

		fmt.Printf("%s %d\n", maxKey, max)
		delete(tags, maxKey)
	}
}
