package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	b := "a"
	for i := 0; i < n; i++ {
		s := toString701(i)
		if i+1 == n {
			s += "n"
		}

		fmt.Println(b + s)

		b = string(s[len(s)-1])
	}
}

func toString701(i int) string {
	str := ""
	for {
		j := i % 25
		if j <= 12 {
			str = string(j+'a') + str
		} else {
			str = string(j+'a'+1) + str
		}
		i /= 25
		if i == 0 {
			break
		}
	}
	return strings.Replace(fmt.Sprintf("%18s", str), " ", "a", -1)
}
