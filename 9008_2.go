package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	n, _ := strconv.Atoi(sc.Text())

	ch := make(chan int)

	for i := 0; i < n; i++ {
		go func(sc *bufio.Scanner, ch chan int) {
			sc.Scan()
			a, _ := strconv.Atoi(sc.Text())
			ch <- a
		}(sc, ch)
	}

	sum := 0
	for i := 0; i < n; i++ {
		sum += <-ch
	}
	fmt.Println(sum)
}
