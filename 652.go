package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	sc.Scan()
	a, _ := strconv.Atoi(sc.Text())

	sc.Scan()
	b, _ := strconv.Atoi(sc.Text())

	sc.Scan()
	s := sc.Text()
	tz, _ := strconv.ParseFloat(strings.Trim(s, "UTC"), 64)

	// まず9時間前にする
	a = (a + 24 - 9) % 24
	// fmt.Println(a, b, tz)

	// 別の国のtimezoneを反映
	b = b + int((tz*10-float64(int(tz))*10)*60)/10
	// fmt.Println(tz, float64(int(tz)), int((tz*10-float64(int(tz))*10)*60)/10)
	if b >= 60 {
		a, b = a+1, b-60
	} else if b < 0 {
		a, b = a-1, b+60
	}
	a = (a + int(tz) + 24) % 24

	fmt.Printf("%02d:%02d\n", a, b)
}
