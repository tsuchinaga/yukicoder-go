package main

import "fmt"

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	nums := make(map[int]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		nums[a]++
	}

	cnt := 0
	for _, c := range nums {
		if c == 1 {
			cnt++
		}
	}

	fmt.Println(cnt)
}
