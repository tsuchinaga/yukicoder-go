package main

import "fmt"

func main() {
	var n, k int
	s := ""
	_, _ = fmt.Scan(&n, &k, &s)

	b := make([]int, 0)          // 開き括弧の位置
	sets := make(map[int]int, 0) // 開始位置: 終了位置のマップ
	for i, c := range s {
		if string(c) == "(" {
			b = append(b, i)
		} else {
			if b[len(b)-1] == k-1 {
				fmt.Println(i + 1)
				break
			} else if i == k-1 {
				fmt.Println(b[len(b)-1] + 1)
				break
			}
			sets[b[len(b)-1]] = i
			b = b[:len(b)-1]
		}
	}
}
