package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	w := make([]int, n) // 勝ち数

	type unset struct {
		a, b int
	}
	u := make([]unset, 0) // 未確定

	for i := range w {
		s := ""
		_, _ = fmt.Scan(&s)
		for j, c := range []rune(s) {
			if c == 'o' {
				w[i]++
			} else if c == '-' && i < j {
				u = append(u, unset{i, j})
			}
		}
	}

	// fmt.Println(w)
	// fmt.Println(u)

	// 勝ちの組み合わせを作る
	p := make([][]int, 0)
	for _, v := range u {
		if len(p) == 0 {
			p = append(p, []int{v.a})
			p = append(p, []int{v.b})
			continue
		}
		tmp := make([][]int, 0)
		for _, q := range p {
			tmp = append(tmp, append(q, v.a))
			tmp = append(tmp, append(q, v.b))
		}
		p = tmp
	}

	// fmt.Println(p)
	if len(p) == 0 { // 未確定パターンがない場合、ここで順位を確定させて終わる
		rank := 0
		for i := n - 1; i >= 0; i-- {
			for t, v := range w {
				if i == v {
					rank++
					if t == 0 {
						fmt.Println(rank)
						return
					}
				}
			}
		}
	}

	min := math.MaxInt64
	for _, q := range p {
		ww := make([]int, len(w))
		copy(ww, w)
		for _, i := range q {
			ww[i]++
		}

		rank := 0
		for i := n - 1; i >= 0; i-- {
			end := false
			for t, v := range ww {
				if i == v {
					rank++
					if t == 0 {
						if min > rank {
							min = rank
						}
						end = true
					}
					break
				}
			}
			if end {
				// fmt.Println(ww, rank, min)
				break
			}
		}
	}
	fmt.Println(min)
}
