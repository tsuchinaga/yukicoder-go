package main

import "fmt"

func main() {
	var s, t string
	_, _ = fmt.Scan(&s)

	for i, c := range s {
		slide := int32((i + 1) % 26)
		r := c - slide
		if r < int32("A"[0]) {
			r += 26
		}
		t += string(r)
	}

	fmt.Println(t)
}
