package main

import (
	"fmt"
	"sort"
)

func main() {
	l := make([]int, 3)
	c := make([]int, 3)
	_, _ = fmt.Scan(&l[0], &l[1], &l[2], &c[0], &c[1], &c[2])

	sort.Slice(l, func(i, j int) bool { // 長い順
		return l[i] > l[j]
	})
	sort.Slice(c, func(i, j int) bool { // 多い順
		return c[i] > c[j]
	})

	ans := 0
	for i := 0; i < 3; i++ {
		ans += (l[0]*2 + l[1]*2 + l[2]*2 - l[i]*2) * c[i] // 多いところから長いのをなくすって感じ
	}
	fmt.Println(ans)
}
