package main

import (
	"fmt"
	"math"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	for i := 2; i < int(math.Sqrt(float64(n)))+1; i++ {
		if n%i == 0 {
			fmt.Println(n / i)
			return
		}
	}
	fmt.Println(1)
}
