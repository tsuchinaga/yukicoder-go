package main

import "fmt"

func main() {
	var h, w int
	_, _ = fmt.Scan(&h, &w)
	if h > w {
		fmt.Println("TATE")
	} else {
		fmt.Println("YOKO")
	}
}
