package main

import "fmt"

func main() {
	var a, b, x, y float64
	_, _ = fmt.Scan(&a, &b, &x, &y)

	if x*b <= y*a { // yが十分な量ある => xに合わせる
		fmt.Println(x + x*b/a)
	} else { // yが十分な量ない => yに合わせる
		fmt.Println(y + y*a/b)
	}
}
