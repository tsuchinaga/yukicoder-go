package main

import "fmt"

func main() {
	var n, a, p int
	_, _ = fmt.Scan(&n)
	items := make([]int, 10)
	for i := 0; i < n*3; i++ {
		_, _ = fmt.Scan(&a)

		items[a-1]++
		if items[a-1] >= 2 {
			p++
			items[a-1] -= 2
		}
	}

	t := 0
	for _, num := range items {
		t += num
	}
	p += t / 4

	fmt.Println(p)
}
