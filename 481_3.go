package main

import "fmt"

func main() {
	var b, n int
	n = 55
	for i := 0; i < 9; i++ {
		_, _ = fmt.Scan(&b)
		n -= b
	}
	fmt.Println(n)
}
