package main

import (
	"fmt"
)

func main() {
	var s, o string
	var t, u int
	_, _ = fmt.Scan(&s, &t, &u)

	r := []rune(s)
	for i := len(s) - 1; i >= 0; i-- {
		if i != t && i != u {
			o = string(r[i]) + o
		}
	}

	fmt.Println(o)
}
