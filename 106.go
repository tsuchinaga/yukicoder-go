package main

import (
	"fmt"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	nums := make([]int, n+1)
	nums[0], nums[1] = -1, -1

	for i := 2; i <= n; i++ {
		if nums[i] == 0 {
			for j := i; j <= n; j += i {
				nums[j]++
			}
		}
	}

	ans := 0
	for _, c := range nums {
		if c >= k {
			ans++
		}
	}
	fmt.Println(ans)
}
