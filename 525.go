package main

import "fmt"

func main() {
	var h, m int
	_, _ = fmt.Scanf("%d:%d", &h, &m)

	m += 5
	fmt.Printf("%02d:%02d", (h+m/60)%24, m%60)
}
