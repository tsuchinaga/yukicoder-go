package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, x, y int
	_, _ = fmt.Scan(&n)

	b := bufio.NewScanner(os.Stdin)
	b.Split(bufio.ScanWords)

	p := make(map[string]int)
	cnt := 0
	for i := 0; i < n; i++ {
		b.Scan()
		x, _ = strconv.Atoi(b.Text())
		b.Scan()
		y, _ = strconv.Atoi(b.Text())

		p[fmt.Sprintf("%d,%d", x%2, y%2)]++

		if p[fmt.Sprintf("%d,%d", x%2, y%2)]%2 == 0 {
			cnt++
		}
	}

	if cnt%2 == 1 {
		fmt.Println("Alice")
	} else {
		fmt.Println("Bob")
	}
}
