package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	// 2倍し続けてオーバーするのに必要な手数
	cnt := 0
	a := 1
	nums := []int{1}
	for a < n {
		cnt++
		a *= 2
		nums = append(nums, a)
	}

	// 2倍するだけで一致したなら、そこで出力しておわり
	if a == n {
		fmt.Println(cnt)
		return
	}

	// fmt.Println(nums)

	// 2のべき乗の値で引いて調整する
	for b := a - n; b > 0; {
		i := 0
		for nums[i] <= b {
			i++
		}
		// fmt.Println(b, i)

		b -= nums[i-1]
		cnt++
	}

	fmt.Println(cnt)
}
