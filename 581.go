package main

import "fmt"

func main() {
	var a, c int
	_, _ = fmt.Scan(&a, &c)

	fmt.Println(a ^ c)
}
