package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, a, b int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	// 枝情報の取り出し
	branches := make([]int, n+1)
	for i := 1; i < n; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		b, _ = strconv.Atoi(sc.Text())

		branches[a]++
		branches[b]++
	}
	// fmt.Println(branches)

	// 枝を3本以上持っているものからはぎとりながら、2本の枝を持っているノードの数を数える
	ans := 0
	for _, b := range branches {
		if b >= 3 {
			ans += b - 2
		}
	}

	fmt.Println(ans)
}
