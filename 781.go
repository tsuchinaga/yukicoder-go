package main

import (
	"fmt"
	"math"
)

func main() {
	var min, max int
	_, _ = fmt.Scan(&min, &max)

	ans := 0
	rMap := make(map[int]int)
	minX, maxX := 1, int(math.Sqrt(float64(max)))
	for x := minX; x <= maxX; x++ {
		minY, maxY := 0, int(math.Sqrt(float64(max)-float64(x*x)))
		if min-x*x > 0 {
			minY = int(math.Ceil(math.Sqrt(float64(min) - float64(x*x))))
		}
		// fmt.Println(x, minY, maxY)
		for y := minY; y <= maxY; y++ {
			rMap[x*x+y*y]++
			if ans < rMap[x*x+y*y] {
				ans = rMap[x*x+y*y]
			}
		}
	}
	fmt.Println(ans * 4)
}
