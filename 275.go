package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	nums := make([]int, n)
	for i := 0; i < n; i++ {
		var a int
		_, _ = fmt.Scan(&a)
		nums[i] = a
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] < nums[j]
	})

	c := n / 2
	if n%2 == 0 {
		fmt.Println(float64(nums[c-1]+nums[c]) / 2)
	} else {
		fmt.Println(nums[c])
	}
}
