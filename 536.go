package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	r := []rune(s)
	if string(r[len(r)-2]) == "a" && string(r[len(r)-1]) == "i" {
		fmt.Printf("%sAI\n", string(r[:len(r)-2]))
	} else {
		fmt.Printf("%s-AI\n", s)
	}
}
