package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	if n == 1 {
		fmt.Println(0)
	} else {
		nums := map[int]int{2: 1}
		for i := 2; i <= n; i++ {
			if i+i-1 == n || i+i == n {
				fmt.Println(nums[i] + 1)
				return
			}
			nums[i+i-1] = nums[i] + 1
			nums[i+i] = nums[i] + 1
			delete(nums, i)
		}
	}
}
