package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	ng := make([]int, 0)
	for i := 0; ; i++ {
		pow := int(math.Pow(2, float64(i)))
		ng = append(ng, pow)
		if pow > n/2 {
			break
		}
	}

	a := "-1"
	for i := 3; i <= n/2; i++ {
		if !Contains638(ng, i, n-i) {
			a = fmt.Sprintf("%d %d", i, n-i)
			break
		}
	}
	fmt.Println(a)
}

func Contains638(nums []int, n ...int) bool {
	for _, m := range nums {
		for _, k := range n {
			if m == k {
				return true
			}
		}
	}
	return false
}
