package main

import (
	"fmt"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	if n == 0 {
		fmt.Println(0)
	} else {
		l := []int{1, 4, 2, 8, 5, 7}
		a := "0."
		for i := 0; i < n; i++ {
			a += strconv.Itoa(l[i%len(l)])
		}
		fmt.Println(a)
	}
}
