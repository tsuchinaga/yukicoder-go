package main

import "fmt"

func main() {
	s, t := "", "abcdefghijklm"
	_, _ = fmt.Scan(&s)
	m := make(map[string]int, 0)
	for _, c := range []rune(t) {
		m[string(c)] = 0
	}

	max := 0
	cnt := 0
	for _, c := range []rune(s) {
		if _, ok := m[string(c)]; !ok {
			fmt.Println("Impossible")
			return
		}
		m[string(c)]++
		if max < m[string(c)] {
			max = m[string(c)]
			cnt = 1
		} else if max == m[string(c)] {
			cnt++
		}
	}

	if max >= 3 || (max == 2 && cnt >= 2) {
		fmt.Println("Impossible")
	} else if max == 2 && cnt == 1 {
		for k, v := range m {
			if v == 0 {
				fmt.Println(k)
				break
			}
		}
	} else {
		for _, c := range []rune(t) {
			fmt.Println(string(c))
		}
	}
}
