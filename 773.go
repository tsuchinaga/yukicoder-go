package main

import "fmt"

func main() {
	var a, b, cnt int
	_, _ = fmt.Scan(&a, &b)

	date := []int{23, 24, 25}

	for _, d := range date {
		if !(a <= d && d <= b) {
			cnt++
		}
	}

	fmt.Println(cnt)
}
