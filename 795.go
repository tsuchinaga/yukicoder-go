package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	// まず100円玉を両者に同じ数だけ配る
	n %= 2

	// 100円玉が残っていれば、片方に100円玉を渡して、もう片方に10円玉を10枚渡す
	if n > 0 {
		n, m = 0, m-10
	}

	// 10円玉が0以上の偶数なら可能、出なければ不可
	if m >= 0 && m%2 == 0 {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
