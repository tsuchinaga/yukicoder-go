package main

import "fmt"

func main() {
	var h, w, n, k int
	_, _ = fmt.Scan(&h, &w, &n, &k)

	if h*w%n == k%n {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
