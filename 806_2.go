package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, a, b int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	// 枝情報の取り出し
	branches := make([]int, n+1)
	for i := 1; i < n; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		b, _ = strconv.Atoi(sc.Text())

		branches[a]++
		branches[b]++
	}
	// fmt.Println(branches)

	// 枝の数で集計する
	nodes := make(map[int]int) // 枝の数: ノードの数
	for _, b := range branches {
		nodes[b]++
	}
	// fmt.Println(nodes)

	// 枝を3本以上持つノードからはぎとる
	ans := 0
	for k, v := range nodes {
		if k > 2 {
			ans += (k - 2) * v
		}
	}
	// fmt.Println(ans)

	// 枝の数が2本のノードがn-2以上あるなら、n-2になるようにはぎとる
	if nodes[2] > n-2 {
		ans += nodes[2] - (n - 2)
	}
	fmt.Println(ans)
}
