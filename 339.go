package main

import "fmt"

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	ps := make([]int, n)
	d := 0
	for i := range ps {
		_, _ = fmt.Scan(&a)
		ps[i] = a

		if i == 0 {
			d = a
		} else {
			b, c := a, d
			for {
				// fmt.Println(a, b, c, d)
				if c == 0 {
					d = b
					break
				}
				b, c = c, b%c
			}
		}
	}

	cnt := 0
	for _, a := range ps {
		// fmt.Println(cnt, a, d)
		cnt += a / d
	}
	fmt.Println(cnt)
}
