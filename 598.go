package main

import (
	"fmt"
	"math"
)

func main() {
	var n, x, a, b float64
	_, _ = fmt.Scan(&n, &x, &a, &b) // ビット長, 体力, 攻撃量, 回復量

	max := math.Pow(2, n-1)
	// fmt.Println(int(x), int(max), int(math.Ceil(x/a)), int(math.Ceil((max-x)/b)))
	fmt.Println(int(math.Min(math.Ceil(x/a), math.Ceil((max-x)/b))))
}
