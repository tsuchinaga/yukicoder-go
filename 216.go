package main

import "fmt"

func main() {
	var n, a, b int
	_, _ = fmt.Scan(&n)

	scores := make([]int, n)
	for i := range scores {
		_, _ = fmt.Scan(&a)
		scores[i] = a
	}

	max := 0
	persons := make([]int, 101)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&b)

		persons[b] += scores[i]
		if b != 0 && max < persons[b] {
			max = persons[b]
		}
	}

	if persons[0] >= max {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
