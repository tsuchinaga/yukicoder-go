package main

import "fmt"

func main() {
	var n, a, b, max, cnt int
	_, _ = fmt.Scan(&n)

	line := make([]int, n)
	for i := range line {
		_, _ = fmt.Scan(&a, &b)
		line[i] = a + 4*b
		if max < a+4*b {
			max = a + 4*b
		}
	}

	for _, s := range line {
		if (max-s)%2 != 0 {
			fmt.Println(-1)
			return
		} else {
			cnt += (max - s) / 2
		}
	}
	fmt.Println(cnt)
}
