package main

import "fmt"

func main() {
	var n, a, b, c, d int
	var r string
	_, _ = fmt.Scan(&n)

	nums := make([]int, 10)

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a, &b, &c, &d, &r)

		if r == "YES" {
			for j := range nums {
				if j != a && j != b && j != c && j != d {
					nums[j] = 1
				}
			}
		} else {
			nums[a] = 1
			nums[b] = 1
			nums[c] = 1
			nums[d] = 1
		}
	}

	for k, v := range nums {
		if v == 0 {
			fmt.Println(k)
			break
		}
	}
}
