package main

import "fmt"

func main() {
	var n, a, b, c int
	_, _ = fmt.Scan(&n, &a, &b, &c)

	// n倍の関係なら小さい方で十分なので、大きい方には消えていただく
	if c%a == 0 || c%b == 0 {
		c = 0
	}
	if b%a == 0 {
		b = 0
	}

	// 登場回数をカウント
	ans := 0
	ans += n / a
	if b > 0 {
		ans += n / b
	}
	if c > 0 {
		ans += n / c
	}

	// 各組合せでの重複数を減らす
	if b > 0 {
		// aとb
		ans -= n / ((a * b) / euclid316(a, b))
	}
	if c > 0 {
		// aとc
		ans -= n / ((a * c) / euclid316(a, c))
	}
	if b > 0 && c > 0 {
		// bとc
		ans -= n / ((b * c) / euclid316(b, c))

		// 減らしすぎたのを増やす
		uab := (a * b) / euclid316(a, b)
		ans += n / ((uab * c) / euclid316(uab, c))
	}

	fmt.Println(ans)
}

// 最大公約数
func euclid316(a, b int) int {
	if a%b == 0 {
		return b
	}
	return euclid316(b, a%b)
}
