package main

import "fmt"

func main() {
	var x, y int
	_, _ = fmt.Scan(&x, &y)

	type m struct {
		x, y int
	}
	move := []m{{2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}, {-2, 1}, {-1, 2}, {1, 2}}

	pos := map[m]int{m{0, 0}: 1}
	for i := 0; i < 3; i++ {
		tmp := make(map[m]int, 0)
		for p := range pos {
			for _, a := range move {
				if p.x+a.x == x && p.y+a.y == y {
					fmt.Println("YES")
					return
				}

				tmp[m{p.x + a.x, p.y + a.y}] = 1
			}
		}
		pos = tmp
		// fmt.Println(pos)
	}

	fmt.Println("NO")
}
