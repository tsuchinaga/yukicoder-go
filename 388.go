package main

import "fmt"

func main() {
	var s, f int
	_, _ = fmt.Scan(&s, &f)

	fmt.Println(s/f + 1)
}
