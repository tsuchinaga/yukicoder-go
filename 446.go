package main

import (
	"fmt"
	"strconv"
)

func main() {
	var a, b string
	_, _ = fmt.Scan(&a, &b)

	an, err := strconv.Atoi(a)
	if err != nil || strconv.Itoa(an) != a || an < 0 || 12345 < an {
		fmt.Println("NG")
		return
	}

	bn, err := strconv.Atoi(b)
	if err != nil || strconv.Itoa(bn) != b || bn < 0 || 12345 < bn {
		fmt.Println("NG")
		return
	}

	fmt.Println("OK")
}
