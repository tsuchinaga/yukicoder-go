package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	min := math.MaxInt64
	for i := 0; i < n; i++ {
		sc.Scan()
		s := sc.Text()

		max := 0
		for _, r := range []rune(s) {
			a := int(r)
			if '0' <= a && a <= '9' {
				a = a - '0'
			} else {
				a = a - 'A' + 10
			}
			if max < a {
				max = a
			}
		}

		num := toDecimal164(s, max+1)
		if min > num {
			min = num
		}
	}
	fmt.Println(min)
}

func toDecimal164(s string, i int) int {
	n := 0
	for _, r := range []rune(s) {
		a := int(r)
		if '0' <= a && a <= '9' {
			a = a - '0'
		} else {
			a = a - 'A' + 10
		}
		// fmt.Println(string(r), a)

		n = n*i + a
	}
	return n
}
