package main

import "fmt"

func main() {
	var n, k, m int
	_, _ = fmt.Scan(&n, &k)

	// 勝てる数字の倍数を確認
	l := (n - 1) % (k + 1)
	if l == 0 {
		l = k + 1
	}
	// fmt.Println(l)

	// 勝てる数字を先行で言えるなら言う、言えないなら0にする
	if l <= k {
		fmt.Println(l)
	} else {
		fmt.Println(0)
	}

	for {
		_, _ = fmt.Scan(&m)
		if m >= n {
			return
		}

		fmt.Println((m/l + 1) * l)
	}
}
