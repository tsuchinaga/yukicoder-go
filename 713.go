package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	sum := 0
	p := make([]int, 0)
	for i := 2; i <= n; i++ {
		isP := true
		for _, v := range p {
			if i%v == 0 {
				isP = false
				break
			}
		}
		if isP {
			sum += i
			p = append(p, i)
		}
	}

	fmt.Println(sum)
}
