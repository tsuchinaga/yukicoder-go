package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	list := make([]int, 0)
	for i := 0; i <= n; i++ {
		for j := 0; j <= n; j++ {
			list = append(list, int(math.Pow(2, float64(i)))*int(math.Pow(5, float64(j))))
		}
	}

	sort.Slice(list, func(i, j int) bool {
		return list[i] < list[j]
	})

	for _, l := range list {
		fmt.Println(l)
	}
}
