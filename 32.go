package main

import "fmt"

func main() {
	var l, m, n int // 100円硬貨、25円硬貨、1円硬貨の枚数
	_, _ = fmt.Scan(&l, &m, &n)

	// 1円硬貨を25円硬貨に両替 1円*25枚 = 25円*1枚
	m, n = m+n/25, n%25

	// 25円硬貨を100円効果に両替 25円*4枚 = 100円*1枚
	l, m = l+m/4, m%4

	// 100円効果を1000円紙幣に両替 100円*10枚 = 1000円*1枚
	l = l % 10

	fmt.Println(l + m + n)
}
