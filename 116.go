package main

import (
	"fmt"
	"sort"
)

func main() {
	var n, cnt int
	_, _ = fmt.Scan(&n)

	nums := make([]int, n)
	for i := range nums {
		var a int
		_, _ = fmt.Scan(&a)
		nums[i] = a

		if i >= 2 {
			// 長さが等しいのでスキップ
			if a == nums[i-2] || a == nums[i-1] {
				continue
			}

			target := []int{nums[i-2], nums[i-1], a}
			sort.Slice(target, func(i, j int) bool {
				return target[i] > target[j]
			})

			if nums[i-1] != target[1] { // 2番目が中心じゃない
				cnt++
			}
		}
	}

	fmt.Println(cnt)
}
