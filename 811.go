package main

import (
	"fmt"
	"math"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	// nまでの数字の素因数分解の結果
	pns := make([]map[int]int, n+1)
	for i := 2; i <= n; i++ {
		if pns[i] == nil {
			pns[i] = map[int]int{i: 1}

			for j := i * 2; j <= n; j += i {
				if pns[j] == nil {
					pns[j] = make(map[int]int)
				}

				m := j
				for m%i == 0 {
					m /= i
					pns[j][i]++
				}
			}
		}
		// fmt.Println(i, pns[i])
	}

	// fmt.Println("n未満のkを満たした最大を探す")
	ans := 0
	maxDivisor := 0
	for i := n - 1; i >= 2; i-- {
		var l, d int // 約数の数のうちnと一致する個数, 約数の数

		for p, q := range pns[i] {
			l += int(math.Min(float64(pns[n][p]), float64(q)))
			d += q * (d + 1)
		}

		// fmt.Println(i, d, maxDivisor, pns[i])
		if k <= l && maxDivisor <= d {
			ans, maxDivisor = i, d
		}
	}
	fmt.Println(ans)
}
