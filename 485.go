package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	if b%a == 0 {
		fmt.Println(b / a)
	} else {
		fmt.Println("NO")
	}
}
