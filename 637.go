package main

import (
	"fmt"
	"strconv"
)

func main() {
	var a1, a2, a3, a4, a5 int
	_, _ = fmt.Scan(&a1, &a2, &a3, &a4, &a5)

	out := ""
	for _, a := range []int{a1, a2, a3, a4, a5} {
		if a%3 != 0 && a%5 != 0 {
			out += strconv.Itoa(a)
		}
		if a%3 == 0 {
			out += "Fizz"
		}
		if a%5 == 0 {
			out += "Buzz"
		}
	}

	fmt.Println(len(out))
}
