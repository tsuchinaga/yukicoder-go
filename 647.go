package main

import "fmt"

func main() {
	var n, a, b, m, x, y int
	_, _ = fmt.Scan(&n)
	p := make([]P647, n)
	for i := range p {
		_, _ = fmt.Scan(&a, &b)
		p[i] = P647{a, b}
	}

	_, _ = fmt.Scan(&m)
	max := 0
	list := []int{0}
	for i := 1; i <= m; i++ {
		_, _ = fmt.Scan(&x, &y)
		cnt := 0
		for _, pp := range p {
			if pp.A >= x && pp.B <= y {
				cnt++
			}
		}
		if max < cnt {
			max = cnt
			list = []int{i}
		} else if cnt > 0 && max == cnt {
			list = append(list, i)
		}
	}

	for _, i := range list {
		fmt.Println(i)
	}
}

type P647 struct {
	A, B int
}
