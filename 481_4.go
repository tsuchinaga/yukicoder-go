package main

import (
	"fmt"
	"strings"
)

func main() {
	var b string
	s := "1-2-3-4-5-6-7-8-9-10-"

	for i := 0; i < 9; i++ {
		_, _ = fmt.Scan(&b)
		s = strings.Replace(s, fmt.Sprintf("%s-", b), "", -1)
	}

	fmt.Println(strings.Replace(s, "-", "", -1))
}
