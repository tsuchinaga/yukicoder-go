package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	d := 0
	pn := make([]int, 0)
	mn := make([]int, 0)
	for i := 0; i < m; i++ {
		_, _ = fmt.Scan(&d)
		if d == 0 {
			n--
		} else if d > 0 {
			pn = append(pn, d)
		} else {
			mn = append(mn, d*-1)
		}
	}

	sort.Slice(pn, func(i, j int) bool {
		return pn[i] < pn[j]
	})

	sort.Slice(mn, func(i, j int) bool {
		return mn[i] < mn[j]
	})

	// fmt.Println(pn)
	// fmt.Println(mn)

	min := -1
	for i := 0; i <= n; i++ {
		z := n

		var p, m int
		if i > 0 {
			j := int(math.Min(float64(i), float64(len(pn))))

			if j > 0 {
				p = pn[j-1]
				z -= j
			}
		}

		if z > 0 {
			j := int(math.Min(float64(z), float64(len(mn))))

			if j > 0 {
				m = mn[j-1]
				z -= j
			}
		}

		if z != 0 {
			continue
		}

		d := int(math.Min(float64(p), float64(m))) + p + m
		if min == -1 || min > d {
			min = d
		}
	}

	fmt.Println(min)
}
