package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	m := ""
	d := int(math.Abs(float64(a - b)))
	if a < b {
		m = "+"
	} else if a > b {
		m = "-"
	}
	fmt.Printf("%s%d\n", m, d)
}
