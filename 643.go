package main

import "fmt"

func main() {
	var x, y int
	_, _ = fmt.Scan(&x, &y)

	// (x, y) -> (x+y, x-y) or (y+x, y-x)
	// (x+y, x-y) -> ((x+y)+(x-y), (x+y)-(x-y)) = (2x, 2y) or ((x-y)+(x+y), (x-y)-(x+y)) = (2x, -2y)
	// (y+x, y-x) -> ((y+x)+(y-x), (y+x)-(y-x)) = (2y, 2x) or ((y-x)+(y+x), (y-x)-(y+x)) = (2y, -2x)
	// つまり可能な操作を繰り返しても、2倍されて場合によっては片方が負になるというのが繰り返されるだけ
	// しかし下記の場合にだけ発散しない
	//   x == y: 操作せずにクリア
	//   y == 0: (x, 0) -> (x+0, x-0) -> (x, x) ってことで1回計算で一致！
	//   x == 0: yが0なら一致させられるので、1回入れ替え操作し、1回計算で一致
	//   x == y * -1: 3回操作することで、x * 2, y * -2が実現する
	// 手元の紙で樹形図で書いてやっとわかりました

	ans := -1
	if x == y {
		ans = 0
	} else if x == 0 {
		ans = 2
	} else if y == 0 {
		ans = 1
	} else if x == y*-1 {
		ans = 3
	}
	fmt.Println(ans)
}
