package main

import (
	"fmt"
)

func main() {
	var n, l, cnt int
	_, _ = fmt.Scan(&n, &l)

	p := make([]bool, l/(n-1)+1) // 分かりにくいけどfalseが素数
	// fmt.Println(l/(n-1)+1, p)
	for i, b := range p {

		// 素数チェック
		if b || i == 0 || i == 1 {
			p[i] = true
			continue
		}

		// 新しい素数を踏んだ場合、その倍数はすべて素数じゃないフラグを立てておく
		for j := i + i; j < l/(n-1)+1; j = j + i {
			p[j] = true
		}

		// fmt.Println(cnt, l, n-1, i, l-(n-1)*i+1)
		cnt += l - (n-1)*i + 1
	}
	fmt.Println(cnt)
}
