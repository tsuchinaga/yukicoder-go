package main

import "fmt"

func main() {
	var s string
	var i, j int
	_, _ = fmt.Scan(&s, &i, &j)

	r := []rune(s)
	r[i], r[j] = r[j], r[i]
	fmt.Println(string(r))
}
