package main

import "fmt"

func main() {
	var w1, w2 string
	_, _ = fmt.Scan(&w1, &w2)
	cs := make([]int, 1)

	for _, c := range w1 + w2 {
		if string(c) == "x" {
			cs = append(cs, 0)
		} else {
			cs[len(cs)-1]++
		}
	}

	max := 0
	for _, n := range cs {
		if max < n {
			max = n
		}
	}

	fmt.Println(max)
}
