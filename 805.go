package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	s := sc.Text()
	r := []rune(s)

	ans := 0
	for i := 1; i <= n/2; i++ {
		for j := 0; j < len(r)-i*2; j++ {
			// fmt.Println(i, j, j+i, j+i*2)
			if r[j] == 'U' && r[j+i] == 'M' && r[j+i+i] == 'G' {
				ans++
			}
		}
	}
	fmt.Println(ans)
}
