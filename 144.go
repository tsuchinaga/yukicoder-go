package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	var p float64
	_, _ = fmt.Scan(&n, &p)

	ps := make([]float64, n+1)
	m := 0.0
	l := 0.0
	for i := 2; i <= n; i++ {
		if ps[i] != 0 {
			l += math.Pow(1-p, ps[i])
		} else {
			m++
		}

		for j := i + i; j <= n; j += i {
			ps[j]++
		}
	}

	fmt.Printf("%.7f\n", m+l)
}
