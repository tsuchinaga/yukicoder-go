package main

import (
	"fmt"
	"math"
)

func main() {
	var n, a, f, m int
	_, _ = fmt.Scan(&n)

	min := math.MaxInt64
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		if i == 0 {
			f, m = a, a
		} else {
			if min > a-m {
				min = a - m
			}
			m = a
		}
	}
	fmt.Printf("%d\n%d\n", min, m-f)
}
