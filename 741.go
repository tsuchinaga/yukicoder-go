package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	mod := int(math.Pow10(9)) + 7
	nums := map[int]int{1: 1, 2: 1, 3: 1, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1, 9: 1}
	ans := 10
	for i := 2; i <= n; i++ {
		total := 0
		tmp := make(map[int]int)
		for j := 9; j >= 1; j-- {
			total = (total + nums[j]) % mod
			tmp[j] = total
			ans = (ans + total) % mod
		}
		nums = tmp

		// fmt.Println(i, total, ans, nums)
	}
	fmt.Println(ans)
}
