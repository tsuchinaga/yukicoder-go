package main

import "fmt"

func main() {
	var b int
	sum := 0
	for i := 0; i < 9; i++ {
		_, _ = fmt.Scan(&b)
		sum += b
	}
	fmt.Println(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 - sum)
}
