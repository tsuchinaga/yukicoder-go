package main

import (
	"fmt"
	"math"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	max := math.MinInt64
	r := []rune(s)
	for i := 0; i <= len(s); i++ {
		r = append(r[1:], r[0])
		if string(r[0]) != "+" && string(r[0]) != "-" && string(r[len(r)-1]) != "+" && string(r[len(r)-1]) != "-" {
			v := 0
			tmp := 0
			mark := "+"
			for _, c := range string(r) {
				switch c {
				case '+', '-':
					if mark == "+" {
						v += tmp
					} else {
						v -= tmp
					}
					mark = string(c)
					tmp = 0
				default:
					tmp = tmp*10 + int(c-'0')
				}
			}
			if mark == "+" {
				v += tmp
			} else {
				v -= tmp
			}
			if max < v {
				max = v
			}
		}
	}

	fmt.Println(max)
}
