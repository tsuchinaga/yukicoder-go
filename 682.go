package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	y := ((3 - (a+b)%3 + 3) - a%3) % 3                // 3-(a+b)%3が欲しい余り、a%3が先頭の数字の余り、(欲しい余り+3-先頭の余り)%3が先頭からいくつマッチしない数値かを表す
	fmt.Println(int(math.Ceil(float64(b-a+1-y) / 3))) // b-a+1が対象の数字の数で、そこから使えない数を飛ばして、3で割って切り上げたら欲しい数字！
}
