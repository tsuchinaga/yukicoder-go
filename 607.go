package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	cars := make([]int, n)
	r := false
	for i := 0; i < m; i++ {
		sum := 0
		for j := range cars {
			sc.Scan()
			a, _ := strconv.Atoi(sc.Text())

			cars[j] += a
			sum += cars[j]
		}

		if !r && sum >= 777 { // 成功していなくて、777人以上乗っていたらチェック
			for k := 0; k < n; k++ {
				sum := 0
				for l := k; l < n; l++ {
					sum += cars[l]
					if sum == 777 {
						r = true
						break
					}
				}

				if r {
					break
				}
			}
		}
		// fmt.Println(cars)
	}

	if r {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
