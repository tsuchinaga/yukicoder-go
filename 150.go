package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	good := "good"
	problem := "problem"

	for i := 0; i < n; i++ {
		s := ""
		_, _ = fmt.Scan(&s)

		// goodへの書き換えに必要な手数の一覧
		goodCost := make([]int, 0)
		for i := 0; i < len(s)-len(problem)-len(good)+1; i++ {
			d := 0
			for j := range good {
				if good[j] != s[i+j] {
					d++
				}
			}
			goodCost = append(goodCost, d)
			// fmt.Println(good, string(s[i:i+len(good)]), d)
		}

		// problemへの書き換えに必要な手数の一覧
		problemCost := make([]int, 0)
		for i := len(good); i < len(s)-len(problem)+1; i++ {
			d := 0
			for j := range problem {
				if problem[j] != s[i+j] {
					d++
				}
			}
			problemCost = append(problemCost, d)
			// fmt.Println(problem, string(s[i:i+len(problem)]), d)
		}

		// 可能な組み合わせのうち最小となるものを出力
		min := -1
		for j := 0; j < len(goodCost); j++ {
			for k := j; k < len(problemCost); k++ {
				if min > goodCost[j]+problemCost[k] || min == -1 {
					min = goodCost[j] + problemCost[k]
				}
			}
		}
		fmt.Println(min)
	}
}
