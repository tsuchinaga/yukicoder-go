package main

import "fmt"

func main() {
	var k, n, f, a int
	_, _ = fmt.Scan(&k, &n, &f)
	age := 0
	for i := 0; i < f; i++ {
		_, _ = fmt.Scan(&a)
		age += a
	}

	if k*n-age >= 0 {
		fmt.Println(k*n - age)
	} else {
		fmt.Println(-1)
	}
}
