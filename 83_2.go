package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	if n%2 == 1 {
		fmt.Print(7)
		n -= 3
	}
	fmt.Println(strings.Repeat("1", n/2))
}
