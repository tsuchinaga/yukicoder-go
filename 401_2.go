package main

import (
	"fmt"
	"strings"
)

func main() {
	var N int
	_, _ = fmt.Scan(&N)

	// N x Nの二次元配列
	data := make([][]string, N)
	for i := range data {
		data[i] = make([]string, N)
	}

	x, y := 0, 0 // 現在位置 左上が0, 0、右下がN-1, N-1
	d := 0       // 向き 0: 右向き, 1: 下向き, 2: 左向き, 3: 上向き
	for i := 1; i <= N*N; i++ {
		data[y][x] = fmt.Sprintf("%03d", i)

		// 向いている方向に進めるか
		isMovable := true
		switch d {
		case 0:
			isMovable = x < N-1 && data[y][x+1] == ""
		case 1:
			isMovable = y < N-1 && data[y+1][x] == ""
		case 2:
			isMovable = 0 < x && data[y][x-1] == ""
		case 3:
			isMovable = 0 < y && data[y-1][x] == ""
		}

		// 移動できないなら向きを変える
		if !isMovable {
			d = (d + 1) % 4
		}

		// 進行方向に進む
		switch d {
		case 0:
			x++
		case 1:
			y++
		case 2:
			x--
		case 3:
			y--
		}

		// fmt.Println(data)
	}

	// 出力
	for _, line := range data {
		fmt.Println(strings.Join(line, " "))
	}
}
