package main

import "fmt"

func main() {
	var n, a, b, c, d int
	var r string
	_, _ = fmt.Scan(&n)

	nums := make(map[int]int)
	for i := 0; i < 10; i++ {
		nums[i] = 1
	}

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a, &b, &c, &d, &r)

		if r == "YES" {
			for j := range nums {
				if j != a && j != b && j != c && j != d {
					delete(nums, j)
				}
			}
		} else {
			delete(nums, a)
			delete(nums, b)
			delete(nums, c)
			delete(nums, d)
		}
	}

	for k := range nums {
		fmt.Println(k)
		return
	}
}
