package main

import "fmt"

func main() {
	var n, total, miss int
	_, _ = fmt.Scan(&n)

	ms := float64(12) / float64(1000) // 1msで入力できる数

	for i := 0; i < n; i++ {
		var t float64
		var s string
		_, _ = fmt.Scan(&t, &s)

		total += len(s)
		if len(s)-int(t*ms) > 0 {
			miss += len(s) - int(t*ms)
		}
	}

	fmt.Printf("%d %d\n", total-miss, miss)
}
