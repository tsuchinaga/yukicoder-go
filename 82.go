package main

import "fmt"

func main() {
	var w, h int
	var c string
	_, _ = fmt.Scan(&w, &h, &c)

	var s []string
	if c == "B" {
		s = []string{"B", "W"}
	} else {
		s = []string{"W", "B"}
	}

	for i := 0; i < h; i++ {
		for j := 0; j < w; j++ {
			fmt.Print(s[(i+j)%2])
		}
		fmt.Printf("\n")
	}
}
