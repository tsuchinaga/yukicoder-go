package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	c := 1
	i := 0
	for c < n {
		c *= 2
		i++
	}
	fmt.Println(i)
}
