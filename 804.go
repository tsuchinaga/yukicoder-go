package main

import "fmt"

func main() {
	var a, b, c, d int
	_, _ = fmt.Scan(&a, &b, &c, &d)

	ans := 0
	i := 0
	for {
		i++
		if b < c || i+c*i > d || a == 0 {
			fmt.Println(ans)
			return
		}
		a--
		b -= c
		ans++
	}
}
