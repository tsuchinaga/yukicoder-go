package main

import (
	"fmt"
	"math"
)

func main() {
	var a, cnt int
	min, max := 1, int(math.Pow10(9))

	for min != max {
		cnt++
		y := min + int(float64(max-min)/2+0.5)
		fmt.Printf("? %d\n", y)
		_, _ = fmt.Scan(&a)
		if a == 0 {
			max = y - 1
		} else {
			min = y
		}
		// fmt.Println(min, max, cnt)
	}

	fmt.Printf("! %d\n", min)
}
