package main

import (
	"fmt"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)
	max, min := 2000000000, 1

	if a < b {
		max = b
	} else {
		min = b
	}

	fmt.Println(max - min - 1)
}
