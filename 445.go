package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)
	fmt.Println(50*a + (500 * a / (8 + 2*b)))
}
