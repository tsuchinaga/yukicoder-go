package main

import (
	"fmt"
	"strings"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	c := strings.Count(s, "c")
	if c-1 > len(s)-c { // wを削るほうが早い
		fmt.Println(len(s) - c)
	} else { // wでもcでも同じか、cを削るほうが早い
		fmt.Println(c - 1)
	}
}
