package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	max := 0
	cnt := 0
	for _, r := range []rune(s) {
		if string(r) != "…" {
			if max < cnt {
				max = cnt
			}
			cnt = 0
		} else {
			cnt++
		}
	}

	if max < cnt {
		fmt.Println(cnt)
	} else {
		fmt.Println(max)
	}
}
