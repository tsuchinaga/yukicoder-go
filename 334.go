package main

import (
	"fmt"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)
	nums := make([]int, n)
	for i := range nums {
		_, _ = fmt.Scan(&a)
		nums[i] = a
	}

	if !kadomatsu334(nums, 1) {
		fmt.Println(-1)
	}
}

func kadomatsu334(nums []int, depth int) bool {
	for i := 0; i < len(nums)-2; i++ {
		if nums[i] == -1 {
			continue
		}
		for j := i + 1; j < len(nums)-1; j++ {
			if nums[j] == -1 {
				continue
			}
			for k := j + 1; k < len(nums); k++ {
				if nums[k] == -1 {
					continue
				}
				if nums[i] != nums[k] && ((nums[i] < nums[j] && nums[k] < nums[j]) || (nums[i] > nums[j] && nums[k] > nums[j])) {
					newNums := make([]int, len(nums))
					copy(newNums, nums)
					newNums[i] = -1
					newNums[j] = -1
					newNums[k] = -1
					w := kadomatsu334(newNums, depth+1)

					// fmt.Println(strings.Repeat("-", depth), i, j, k, "-", nums[i], nums[j], nums[k], newNums, w)

					if depth%2 == 0 && !w { // 相手から渡された数で勝てないなら負け
						return false
					} else if depth%2 == 1 && w { // 相手に渡した数で勝てるなら勝ち
						if depth == 1 {
							fmt.Println(i, j, k)
						}
						return true
					}
				}
			}
		}
	}
	return depth%2 == 0
}
