package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	for i := n; i > 0; i-- {
		for j := 0; j < i; j++ {
			fmt.Print(n)
		}
		fmt.Printf("\n")
	}
}
