package main

import (
	"fmt"
	"math"
)

func main() {
	var n string
	_, _ = fmt.Scan(&n)

	a, b := 0, 1 // 数字の数と、背景の数
	for _, c := range n {
		a++
		switch string(c) {
		case "0", "4", "6", "9":
			b += 1
		case "8":
			b += 2
		}
	}

	// 数字を違う色 -> 背景を変える -> 数字を変える
	// or
	// 背景を違う色 -> 数字を変える -> 背景を変える
	ans := int(math.Min(float64(a*2+b), float64(b*2+a)))
	fmt.Println(ans)
}
