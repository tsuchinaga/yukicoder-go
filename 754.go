package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	mod := int(math.Pow10(9)) + 7

	sc := bufio.NewScanner(os.Stdin)
	an := make([]int, n+1)
	for i := range an {
		sc.Scan()
		an[i], _ = strconv.Atoi(sc.Text())
	}

	ans := 0
	bSum := 0
	for i := 0; i <= n; i++ {
		sc.Scan()
		b, _ := strconv.Atoi(sc.Text())
		bSum = (bSum + b) % mod

		ans = (ans + bSum*an[n-i]) % mod
	}
	fmt.Println(ans)
}
