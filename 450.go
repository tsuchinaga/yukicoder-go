package main

import (
	"fmt"
)

func main() {
	var v1, v2, d, w float64
	_, _ = fmt.Scan(&v1, &v2, &d, &w)

	fmt.Printf("%.12f\n", d/(v1+v2)*w)
}
