package main

import "fmt"

func main() {
	var n, a, b, c, d, cnt int
	var r string
	_, _ = fmt.Scan(&n)

	nums := make([]int, 10)

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a, &b, &c, &d, &r)

		if r == "YES" {
			nums[a]++
			nums[b]++
			nums[c]++
			nums[d]++
			cnt++
		} else {
			nums[a]--
			nums[b]--
			nums[c]--
			nums[d]--
		}
	}

	for k, v := range nums {
		if v == cnt {
			fmt.Println(k)
			return
		}
	}
}
