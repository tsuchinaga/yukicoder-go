package main

import "fmt"

func main() {
	var n, o string
	_, _ = fmt.Scan(&n)

	cnt := 0
	for i := 0; i < len(n); i++ {
		if cnt == 3 {
			cnt = 0
			o = "," + o
		}
		c := string(n[len(n)-1-i])
		cnt++
		o = c + o
	}

	fmt.Println(o)
}
