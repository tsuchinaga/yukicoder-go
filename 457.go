package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	buf := 10000

	sc := bufio.NewScanner(os.Stdin)
	sc.Buffer(make([]byte, buf+1), buf+1)

	sc.Scan()
	s := sc.Text()
	r := []rune(s)

	// fmt.Println(len(s))

	var left, right int
	for i := 0; i < len(r)-4; i++ {
		if r[i] != '(' {
			continue
		}

		c, b, a := 0, -1, -1 // ^の出た回数, 最初に*が出た時の^の数, 最後に*が出た時の^の数
		for j := i + 1; j < len(r); j++ {
			switch r[j] {
			case '^':
				c++
			case '*':
				if b == -1 {
					b = c
				}
				a = c
			case ')':
				// fmt.Println(c, b, a)

				// 左向きが作れるか
				if a >= 2 {
					left++
				}

				// 右向きが作れるか
				if b >= 0 && c-b >= 2 {
					right++
				}
			}
		}
	}

	fmt.Println(left, right)
}
