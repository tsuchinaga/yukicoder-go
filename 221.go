package main

import "fmt"

func main() {
	var n float64
	_, _ = fmt.Scan(&n)

	// a := (10000 - n) * 0.01 // 誤認逮捕される一般人
	// b := n * 0.99           // 逮捕されるマフィア
	// c := a / (a + b) * 100  // 逮捕されたのが一般人の確率
	fmt.Println((10000 - n) * 100 / (10000 + 98*n))
}
