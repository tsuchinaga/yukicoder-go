package main

import "fmt"

func main() {
	var b, n uint
	for i := 0; i < 9; i++ {
		_, _ = fmt.Scan(&b)
		n |= 1 << b
	}

	i := 1
	for n > 0 {
		if n&1 == 0 {
			fmt.Println(i)
			return
		}
		n >>= 1
		i++
	}
	fmt.Println(10)
}
