package main

import (
	"fmt"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	// 配列にする
	nums := make([]int, 0)
	for _, c := range s {
		nums = append(nums, int(c-'0'))
	}

	for i := 0; i < len(nums); i++ {
		top := nums[i]
		max := 0
		maxJ := -1

		for j := len(nums) - 1; j > i; j-- {
			if max < nums[j] {
				max = nums[j]
				maxJ = j
			}
		}

		if max > top {
			nums[i], nums[maxJ] = nums[maxJ], nums[i]
			break
		}
	}

	for _, v := range nums {
		fmt.Print(v)
	}
	fmt.Println()
}
