package main

import "fmt"

func main() {
	n, p := 0, 0
	_, _ = fmt.Scan(&n, &p)
	if n*p == p {
		fmt.Println("=")
	} else {
		fmt.Println("!=")
	}
}
