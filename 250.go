package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	var q, d, x, t int
	_, _ = fmt.Scan(&q)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	for i := 0; i < q; i++ {
		sc.Scan()
		d, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		x, _ = strconv.Atoi(sc.Text())
		sc.Scan()
		t, _ = strconv.Atoi(sc.Text())

		cnt := 1
		r := true
		for j := 1; j < d; j++ {
			cnt = cnt * (x + j) / j
			if cnt > t {
				r = false
				break
			}
		}
		if r && cnt <= t {
			fmt.Println("AC")
		} else {
			fmt.Println("ZETUBOU")
		}
	}
}
