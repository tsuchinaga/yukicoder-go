package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	for i := a; i <= b; i++ {
		if IsThree(i) {
			fmt.Println(i)
		}
	}
}

func IsThree(n int) bool {
	if n%3 == 0 {
		return true
	}

	for {
		if n%10 == 3 {
			return true
		}
		n /= 10
		if n == 0 {
			break
		}
	}

	return false
}
