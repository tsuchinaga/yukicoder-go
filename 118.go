package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)
	mod := int(math.Pow10(9)) + 7

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	ls := make([]int, 0)
	bs := make(map[int]int, 0)
	for i := 0; i < n; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())
		if _, ok := bs[a]; !ok {
			ls = append(ls, a)
		}
		bs[a]++
	}

	// fmt.Println(ls, bs)

	cnt := 0
	for i := 0; i < len(ls)-2; i++ {
		for j := i + 1; j < len(ls)-1; j++ {
			for k := j + 1; k < len(ls); k++ {
				cnt = (cnt + bs[ls[i]]*bs[ls[j]]*bs[ls[k]]) % mod
			}
		}
	}
	fmt.Println(cnt)
}
