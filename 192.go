package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	if n%2 == 0 {
		fmt.Println(n)
	} else {
		fmt.Println(n + 1)
	}
}
