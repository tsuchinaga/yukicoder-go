package main

import (
	"fmt"
	"math"
)

func main() {
	var r, c int
	_, _ = fmt.Scan(&r, &c)

	ans := 0
	if r == c {
		ans = (r / 2) * (c / 2)
		if r%2 != 0 {
			ans += r/2 + 1
		}
	} else {
		ans = r / 2 * c
		if r%2 == 1 {
			ans += int(math.Ceil(float64(c) / 2))
		}
	}
	fmt.Println(ans - 1)
}
