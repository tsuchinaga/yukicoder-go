package main

import (
	"fmt"
	"math"
)

func main() {
	var s string
	var x, y float64
	_, _ = fmt.Scan(&s)

	for _, r := range []rune(s) {
		switch string(r) {
		case "N":
			y++
		case "E":
			x++
		case "W":
			x--
		case "S":
			y--
		}
	}

	fmt.Print(math.Sqrt(x*x + y*y))
}
