package main

import (
	"fmt"
	"math"
)

func main() {
	var n, x int
	_, _ = fmt.Scan(&n, &x)

	f := 0.0
	nums := make([]float64, n)
	for i := range nums {
		_, _ = fmt.Scan(&f)
		nums[i] = f
	}

	// 微分
	a := 0.0
	for _, n := range nums {
		a += n * math.Pow(float64(x), n-1)
	}
	fmt.Printf("%.5f\n", a)

	// 積分
	b := 0.0
	for _, n := range nums {
		if n == -1 {
			b += math.Log(math.Abs(float64(x)))
		} else {
			b += math.Pow(float64(x), n+1) / (n + 1)
		}
	}
	fmt.Printf("%.5f\n", b)
}
