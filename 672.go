package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
)

func main() {
	maxSize := 2 * int(math.Pow10(5))
	rdr := bufio.NewReaderSize(os.Stdin, maxSize)
	buf := make([]byte, 0, maxSize)
	for {
		l, p, _ := rdr.ReadLine()
		buf = append(buf, l...)
		if !p {
			break
		}
	}
	s := string(buf)

	n := 0
	m := map[int]int{0: -1}
	max := 0
	for i := 0; i < len(s); i++ {
		if string(s[i]) == "A" {
			n++
		} else {
			n--
		}
		if v, ok := m[n]; !ok {
			m[n] = i
		} else if max < i-v {
			max = i - v
		}
		// fmt.Println(n, i, m, m[n])
	}
	fmt.Println(max)
}
