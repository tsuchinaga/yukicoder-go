package main

import (
	"fmt"
	"math"
)

func main() {
	s, t, max, min := 0.0, 0.0, float64(math.MinInt64), float64(math.MaxInt64)
	for i := 0; i < 6; i++ {
		_, _ = fmt.Scan(&s)
		t += s
		if max < s {
			max = s
		}

		if min > s {
			min = s
		}
	}

	fmt.Printf("%0.2f\n", (t-max-min)/4)
}
