package main

import (
	"fmt"
	"reflect"
)

func main() {
	var a, b string
	_, _ = fmt.Scan(&a, &b)

	am, bm := make(map[string]int, 0), make(map[string]int, 0)

	for _, c := range []rune(a) {
		if _, ok := am[string(c)]; ok {
			am[string(c)]++
		} else {
			am[string(c)] = 1
		}
	}

	for _, c := range []rune(b) {
		if _, ok := bm[string(c)]; ok {
			bm[string(c)]++
		} else {
			bm[string(c)] = 1
		}
	}

	if reflect.DeepEqual(am, bm) {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
