package main

import "fmt"

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	win := "NA"
	for i := range s {
		if i < 2 {
			continue
		}

		if s[i] == s[i-1] && s[i] == s[i-2] {
			if string(s[i]) == "O" {
				win = "East"
			} else {
				win = "West"
			}
			break
		}
	}
	fmt.Println(win)
}
