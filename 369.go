package main

import "fmt"

func main() {
	var n, v, a int
	_, _ = fmt.Scan(&n)

	sum := 0
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		sum += a
	}

	_, _ = fmt.Scan(&v)

	fmt.Println(sum - v)
}
