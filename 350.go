package main

import "fmt"

func main() {
	var v, t float64
	_, _ = fmt.Scanf("0.%f %f\n", &v, &t)
	fmt.Println(int(v * t / 10000))
}
