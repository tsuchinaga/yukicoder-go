package main

import "fmt"

func main() {
	var t, x, bx int
	_, _ = fmt.Scan(&t)

	for i := 0; i < t; i++ {
		_, _ = fmt.Scan(&x)
		if bx+1 != x && bx-1 != x {
			fmt.Println("F")
			return
		}
		bx = x
	}

	fmt.Println("T")
}
