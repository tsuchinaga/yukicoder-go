package main

import (
	"fmt"
	"strings"
)

func main() {
	var n float64
	_, _ = fmt.Scan(&n)
	fmt.Println(strings.Trim(strings.Trim(fmt.Sprintf("%.1f", 3.5*n), "0"), "."))
}
