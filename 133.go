package main

import "fmt"

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	aCards := make([]int, n)
	for i := range aCards {
		_, _ = fmt.Scan(&a)
		aCards[i] = a
	}

	bCards := make([]int, n)
	for i := range bCards {
		_, _ = fmt.Scan(&a)
		bCards[i] = a
	}

	pattern := [][]int{{}}
	for i := 0; i < n; i++ {
		tmp := make([][]int, 0)
		for j := 0; j < n; j++ {
			for _, p := range pattern {
				contains := false
				for _, q := range p {
					if q == j {
						contains = true
						break
					}
				}
				if !contains {
					q := make([]int, len(p))
					copy(q, p)
					tmp = append(tmp, append(q, j))
				}
			}
		}
		pattern = tmp
	}

	ans := 0
	for _, aP := range pattern {
		for _, bP := range pattern {
			win := 0
			for j := 0; j < n; j++ {
				if aCards[aP[j]] > bCards[bP[j]] {
					win++
				}
			}
			if win > n/2 {
				// fmt.Println(win, aP, bP)
				ans++
			}
		}
	}

	fmt.Printf("%.4f\n", float64(ans)/float64(len(pattern)*len(pattern)))
}
