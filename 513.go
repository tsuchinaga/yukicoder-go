package main

import "fmt"

func main() {
	type ans struct {
		x, y, d int
	}

	qs := []ans{{x: 0, y: 0}, {x: 100000, y: 0}}
	for i, q := range qs {
		fmt.Printf("%d %d\n", q.x, q.y)

		a := 0
		_, _ = fmt.Scan(&a)

		if a == 0 { // 当たりを引いた場合
			return
		}

		qs[i].d = a
	}

	y := (qs[0].d + qs[1].d - 100000) / 2
	x := qs[0].d - y

	fmt.Println(x, y)
}
