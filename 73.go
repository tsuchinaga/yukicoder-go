package main

import (
	"fmt"
	"math"
)

func main() {
	var c int

	alpha := make(map[string]int, 0)
	for i := 0; i < 26; i++ {
		_, _ = fmt.Scan(&c)

		alpha[string('a'+i)] = c
	}
	// fmt.Println(alpha)

	ans := 1
	for _, c := range "helowrd" {
		switch string(c) {
		case "h", "e", "w", "r", "d":
			if alpha[string(c)] < 1 {
				ans *= 0
			} else {
				ans *= alpha[string(c)]
			}
		case "o":
			if alpha[string(c)] < 2 {
				ans *= 0
			} else {
				p1 := int(math.Ceil(float64(alpha[string(c)]) / 2))
				p2 := alpha[string(c)] / 2
				ans *= p1 * p2
			}
		case "l":
			if alpha[string(c)] < 3 {
				ans *= 0
			} else {
				// lの確率が最大になる組み合わせを探す
				max := 1
				for i := alpha[string(c)] - 3; i >= 0; i-- {
					p1 := 0
					for j := 1; j < i+2; j++ {
						p1 += j
					}
					p2 := alpha[string(c)] - 2 - i

					// fmt.Println(i, p1, p2)

					if p1*p2 > max {
						max = p1 * p2
					} else {
						break
					}
				}

				ans *= max
			}
		}
		// fmt.Println(string(c), alpha[string(c)], ans)
	}

	fmt.Println(ans)
}
