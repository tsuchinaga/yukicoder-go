package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	buf := 100000
	sc := bufio.NewScanner(os.Stdin)
	sc.Buffer(make([]byte, buf+1), buf+1)

	sc.Scan()
	r := []rune(sc.Text())

	// fmt.Println(len(r))
	// fmt.Println(r)

	// 後ろからcとwを探しながら、答えを算出する
	ans := 0
	w := 0
	score := 0
	for i := len(r) - 1; i >= 0; i-- {
		switch r[i] {
		case 'c':
			ans += score
		case 'w':
			w++
			score += w - 1
		}
	}

	fmt.Println(ans)
}
