package main

import (
	"fmt"
	"strconv"
)

func main() {
	var t int
	_, _ = fmt.Scan(&t)

	s := ""
	for i := 0; i < t; i++ {
		_, _ = fmt.Scan(&s)
		for {
			if len(s) == 1 {
				fmt.Println(s)
				break
			}

			t := ""
			for j := 0; j < len(s)-1; j++ {
				a, _ := strconv.Atoi(string(s[j]))
				b, _ := strconv.Atoi(string(s[j+1]))
				a += b
				for {
					if a < 10 {
						break
					}
					a = a/10 + a%10
				}
				t += strconv.Itoa(a)
			}
			s = t
		}
	}
}
