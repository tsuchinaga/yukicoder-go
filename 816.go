package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	check := func(c int) bool {
		return a != c && b != c && (a+b)%c == 0 && (b+c)%a == 0 && (c+a)%b == 0
	}

	if check(1) {
		fmt.Println(1)
		return
	}

	// 素因数分解
	n := a + b
	pn := make([]int, 0)
	for i := 2; i < n+1; i++ {
		if n%i == 0 {
			is := true
			for _, p := range pn {
				if i%p == 0 {
					is = false
					break
				}
			}
			if is {
				for n%i == 0 {
					n /= i
					// fmt.Println(i, n)
					pn = append(pn, i)
				}
			}
		}
	}
	// fmt.Println(pn)

	// 素因数の組み合わせで約数をチェック
	pattern := map[int]int{1: 1}
	for _, p := range pn {
		for q := range pattern {
			if check(p * q) {
				fmt.Println(p * q)
				return
			}
			pattern[p*q] = 1
		}
	}

	fmt.Println(-1)
}
