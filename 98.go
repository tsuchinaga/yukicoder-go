package main

import (
	"fmt"
	"math"
)

func main() {
	var x, y float64
	_, _ = fmt.Scan(&x, &y)

	r2 := math.Pow(math.Abs(x), 2) + math.Pow(math.Abs(y), 2)
	r := math.Sqrt(r2)
	rr := math.Ceil(r * 2)

	if r < rr/2 {
		fmt.Println(int(rr))
	} else {
		fmt.Println(int(rr + 1))
	}
}
