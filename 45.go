package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)

	var b, c, d float64 // 取った場合, 取れないから取らなかった場合, 取れるのに取らなかった場合
	for i := 0; i < n; i++ {
		sc.Scan()
		a, _ = strconv.Atoi(sc.Text())

		larger := math.Max(c, d)
		b, c, d = larger+float64(a), b, larger
		// fmt.Println(b, c, d)
	}

	fmt.Println(math.Max(b, math.Max(c, d)))
}
