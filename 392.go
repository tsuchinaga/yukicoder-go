package main

import "fmt"

func main() {
	var n, a int
	_, _ = fmt.Scan(&n)

	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)

		ans := ""
		for {
			if a == 0 {
				fmt.Println(ans)
				break
			}

			if a%2 == 0 {
				ans = "R" + ans
				a -= 2
			} else {
				ans = "L" + ans
				a -= 1
			}
			a /= 2
		}
	}
}
