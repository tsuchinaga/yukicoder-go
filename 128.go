package main

import "fmt"

func main() {
	var n, m int
	_, _ = fmt.Scan(&n, &m)

	fmt.Println(n / 1000 / m * 1000)
}
