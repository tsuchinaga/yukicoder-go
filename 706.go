package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, max, maxI int
	_, _ = fmt.Scan(&n)

	s := ""
	is := make(map[int]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&s)
		c := strings.Count(s, "^")
		is[c]++
		if max < is[c] {
			max = is[c]
			maxI = c
		} else if max == is[c] && maxI < c {
			maxI = c
		}
	}

	fmt.Println(maxI)
}
