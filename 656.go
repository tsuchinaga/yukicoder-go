package main

import (
	"fmt"
	"strconv"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)

	total := 0
	for _, c := range s {
		n, _ := strconv.Atoi(string(c))

		total += n
		if n == 0 {
			total += 10
		}
	}

	fmt.Println(total)
}
