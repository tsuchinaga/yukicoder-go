package main

import "fmt"

func main() {
	s := ""
	_, _ = fmt.Scan(&s)

	sets := make(map[string]int, 0)
	for _, c := range s {
		if _, ok := sets[string(c)]; ok {
			sets[string(c)]++
			if sets[string(c)] > 2 {
				fmt.Println("Impossible")
				return
			}
		} else {
			sets[string(c)] = 1
		}
	}

	if len(sets) != 7 {
		fmt.Println("Impossible")
		return
	}

	c := ""
	for k, v := range sets {
		if v == 1 {
			c = k
		}
	}

	fmt.Println(c)
}
