package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	m := n

	ans := 0
	p := make(map[int]int, 0)
	nums := make([]int, int(math.Ceil(math.Sqrt(float64(n))))+1)
	nums[0], nums[1] = 1, 1

	for i, v := range nums {
		if i == 0 || i == 1 || v != 0 {
			continue
		}

		for {
			if n%i != 0 {
				break
			}
			p[i]++
			ans++
			n /= i
		}

		if n == 1 {
			break
		}

		for j := i + i; j < len(nums)-1; j += i {
			nums[j] = 1
		}
	}

	// fmt.Println(p)
	if ans >= 3 {
		fmt.Println("YES")
	} else if ans == 2 {
		a := 1
		for k, v := range p {
			a *= int(math.Pow(float64(k), float64(v)))
		}
		if a != m {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	} else {
		fmt.Println("NO")
	}
}
