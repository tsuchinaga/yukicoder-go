package main

import (
	"fmt"
	"time"
)

func main() {
	r := []rune("Hello World!")
	ch := make(chan int)

	for i, c := range r {
		go func(ch chan int, i int, c rune) {
			time.Sleep(time.Duration(i) * 100 * time.Millisecond) // i * 100 ms
			fmt.Print(string(c))
			ch <- i
		}(ch, i, c)
	}

	for i := 0; i < len(r); i++ {
		<-ch
	}
	fmt.Printf("\n")
}
