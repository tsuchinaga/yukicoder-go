package main

import (
	"fmt"
	"math"
)

func main() {
	var c, ri, ro float64
	_, _ = fmt.Scan(&c, &ri, &ro)
	fmt.Println(math.Pi * math.Pi / 4 * (ri + ro) * (ro - ri) * (ro - ri) * c)
}
