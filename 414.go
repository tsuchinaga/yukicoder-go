package main

import (
	"fmt"
	"math"
)

func main() {
	var m int
	_, _ = fmt.Scan(&m)

	nums := make([]int, int(math.Ceil(math.Sqrt(float64(m))))+1) // 0が素数という扱い
	// fmt.Println(len(nums))
	nums[0] = 1
	nums[1] = 1

	for i, v := range nums {
		if v == 1 {
			continue
		}

		if m%i == 0 {
			fmt.Printf("%d %d\n", i, m/i)
			return
		}

		for j := i + i; j < len(nums); j += i {
			nums[j] = 1
		}
	}

	fmt.Printf("%d %d\n", 1, m)
}
