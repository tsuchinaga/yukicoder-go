package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b, c, d float64
	_, _ = fmt.Scan(&a, &b, &c, &d)
	fmt.Println(int(math.Min(a, math.Min(b/c, d/(c+1)))))
}
