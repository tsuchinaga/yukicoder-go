package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)
	sc.Split(bufio.ScanWords)
	sc.Scan()
	n, _ := strconv.Atoi(sc.Text())
	sc.Scan()
	m, _ := strconv.Atoi(sc.Text())
	sc.Scan()
	k, _ := strconv.Atoi(sc.Text())

	// 犯人が潜伏しうる町
	cities := make([]int, n)
	for i := range cities {
		cities[i] = i + 1
	}
	// fmt.Println(cities)

	// どこからどこにいくのにいくらかかるか
	type route struct {
		to, cost int
	}
	routes := make(map[int][]route)
	for i := 0; i < m; i++ {
		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		b, _ := strconv.Atoi(sc.Text())
		sc.Scan()
		c, _ := strconv.Atoi(sc.Text())

		if _, ok := routes[a]; !ok {
			routes[a] = make([]route, 0)
		}
		routes[a] = append(routes[a], route{b, c})

		if _, ok := routes[b]; !ok {
			routes[b] = make([]route, 0)
		}
		routes[b] = append(routes[b], route{a, c})
	}
	// fmt.Println(routes)

	for i := 0; i < k; i++ {
		sc.Scan()
		c, _ := strconv.Atoi(sc.Text())

		exists := make(map[int]int) // キーが町
		for _, city := range cities {
			// cityから行けるところで、cの通行料金がかかるところがあるか
			if len(routes[city]) > 0 {
				for _, r := range routes[city] {
					if r.cost == c {
						exists[r.to]++
					}
				}
			}
		}
		// fmt.Println(i, "exists", exists)

		cities = make([]int, len(exists))
		i := 0
		for e := range exists {
			cities[i] = e
			i++
		}
		// fmt.Println(i, "cities", cities)
	}

	sort.Slice(cities, func(i, j int) bool {
		return cities[i] < cities[j]
	})

	s := ""
	for _, c := range cities {
		if s != "" {
			s += " "
		}
		s += strconv.Itoa(c)
	}
	fmt.Println(len(cities))
	fmt.Println(s)
}
