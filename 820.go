package main

import (
	"fmt"
	"math"
)

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	if n < k {
		fmt.Println(0)
	} else {
		fmt.Println(int(math.Pow(2, float64(n-k))))
	}
}
