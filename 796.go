package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	// sum := 0
	for i := 0; i < (n-2)/3; i++ {
		fmt.Print("1 1 1 ")
		// sum += 3
	}

	fmt.Print("1 3")
	// sum += 4

	for i := 0; i < (n-2)%3; i++ {
		fmt.Print(" 3")
		// sum += 3
	}
	fmt.Println()
	// fmt.Println(sum%3, sum)
}
