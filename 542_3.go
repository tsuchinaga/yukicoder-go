package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	contain := make(map[int]int)
	for i := 0; i <= b; i++ {
		for j := 0; j <= a; j++ {
			n := 5*i + j
			if n != 0 && contain[n] == 0 {
				contain[n] = 1
				fmt.Println(n)
			}
		}
	}
}
