package main

import "fmt"

func main() {
	var n, c int
	_, _ = fmt.Scan(&n)

	b := n
	box1 := 0 // 1個の箱の数
	box2 := 0 // 2個の箱の数
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&c)
		switch c {
		case 0: // アメちゃんが入っていない箱は箱として認めない！
			b--
		case 1:
			box1++
		case 2:
			box2++
		}
	}

	// すべての箱に1つずつ入っていて、箱の数が奇数
	// 1つの箱に2つ、それ以外の箱に1つずつ入っていて、1つの箱の数が奇数
	if (box1%2 == 1) && (box1 == b || (box1 == b-1 && box2 == 1)) {
		fmt.Println("A")
	} else {
		fmt.Println("B")
	}
}
