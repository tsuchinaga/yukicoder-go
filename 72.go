package main

import (
	"fmt"
	"math"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	mod := int(math.Pow(10, 6)) + 7
	ans := (((n/2+1)%mod)*((n-n/2+1)%mod) - 1) % mod
	if ans < 0 {
		ans += mod
	}
	fmt.Println(ans)
}
