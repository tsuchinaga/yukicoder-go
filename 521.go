package main

import "fmt"

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)

	if k < 1 || n < k {
		fmt.Println(0)
	} else if k == n-k+1 {
		fmt.Println(n - 1)
	} else {
		fmt.Println(n - 2)
	}
}
