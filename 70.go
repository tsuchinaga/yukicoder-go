package main

import "fmt"

func main() {
	var n, h1, m1, h2, m2 int
	_, _ = fmt.Scan(&n)

	total := 0
	for i := 0; i < n; i++ {
		_, _ = fmt.Scanf("%d:%d", &h1, &m1)
		_, _ = fmt.Scanf("%d:%d", &h2, &m2)
		total += ((h2-h1)*60 + (m2 - m1) + 24*60) % (24 * 60)
	}

	fmt.Println(total)
}
