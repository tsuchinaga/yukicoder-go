package main

import (
	"fmt"
	"sort"
)

type Fraction struct {
	M, D float64
}

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	nums := make([]Fraction, n)
	for i := range nums {
		var a, b float64
		_, _ = fmt.Scan(&a, &b)
		nums[i] = Fraction{a, b}
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i].M/nums[i].D > nums[j].M/nums[j].D
	})

	for _, f := range nums {
		fmt.Printf("%.0f %.0f\n", f.M, f.D)
	}
}
