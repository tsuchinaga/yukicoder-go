package main

import (
	"fmt"
)

func main() {
	var r, g, b, ans int
	_, _ = fmt.Scan(&r, &g, &b)

	for {
		// fmt.Println(r, g, b, ans)
		if r > 0 && g > 0 && b > 0 {
			r, g, b, ans = r-1, g-1, b-1, ans+1
			continue
		} else if r == 0 && g > 0 && b > 0 {
			if g >= b && g >= 3 {
				g, b, ans = g-3, b-1, ans+1
				continue
			} else if b > g && b >= 3 {
				g, b, ans = g-1, b-3, ans+1
				continue
			}
		} else if r > 0 && g == 0 && b > 0 {
			if r >= b && r >= 3 {
				r, b, ans = r-3, b-1, ans+1
				continue
			} else if b > r && b >= 3 {
				r, b, ans = r-1, b-3, ans+1
				continue
			}
		} else if r > 0 && g > 0 && b == 0 {
			if r >= g && r >= 3 {
				r, g, ans = r-3, g-1, ans+1
				continue
			} else if g > r && g >= 3 {
				r, g, ans = r-1, g-3, ans+1
				continue
			}
		} else if r == 0 && g == 0 && b >= 5 {
			b, ans = b-5, ans+1
			continue
		} else if r == 0 && g >= 5 && b == 0 {
			g, ans = g-5, ans+1
			continue
		} else if r >= 5 && g == 0 && b == 0 {
			r, ans = r-5, ans+1
			continue
		}

		break
	}

	fmt.Println(ans)
}
