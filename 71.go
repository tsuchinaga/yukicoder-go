package main

import "fmt"

func main() {
	var n int
	_, _ = fmt.Scan(&n)
	fmt.Println((n/2+1)*(n-n/2+1) - 1)
}
