package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
)

func main() {
	var n, k, a int
	_, _ = fmt.Scan(&n, &k)

	nums := make([]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&a)
		if a <= k {
			nums = append(nums, a)
		}
	}

	sort.Slice(nums, func(i, j int) bool {
		return nums[i] > nums[j]
	})

	max := 0
	for i := 1; i < int(math.Pow(float64(2), float64(len(nums)))); i++ {
		b := fmt.Sprintf("%0"+strconv.Itoa(len(nums))+"b\n", i)
		sum := 0
		for i, v := range []rune(b) {
			if string(v) == "1" {
				sum += nums[i]
				if sum > k {
					break
				}
			}
		}
		if sum <= k && max < sum {
			max = sum
		}
	}

	fmt.Println(max)
}
