package main

import "fmt"

func main() {
	var d1, d2, d3, s int
	_, _ = fmt.Scan(&d1, &d2, &d3, &s)

	if d1+d2+d3 < 2 || s == 1 {
		fmt.Println("SURVIVED")
	} else {
		fmt.Println("DEAD")
	}
}
