package main

import "fmt"

func main() {
	t := "yukicoder"
	var s string
	_, _ = fmt.Scan(&s)

	for i, r := range []rune(s) {
		if string(r) == "?" {
			fmt.Println(string([]rune(t)[i]))
			break
		}
	}
}
