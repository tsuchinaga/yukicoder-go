package main

import (
	"fmt"
	"math"
)

func main() {
	var n, d, t, k, inT, inK float64 // 日数、交通費、東京報酬、京都報酬、最大東京収益、最大京都収益
	_, _ = fmt.Scan(&n, &d)

	inK = -d // 最初は東京にいるので、京都に居るという場合は東京から移動した後ということになる

	for i := 0.0; i < n; i++ {
		_, _ = fmt.Scan(&t, &k)
		inT, inK = math.Max(inT+t, inK+t-d), math.Max(inT+k-d, inK+k) // 東京最高、京都最高
		// fmt.Println(inT, inK)
	}
	fmt.Println(int(math.Max(inT, inK)))
}
