package main

import (
	"fmt"
	"math"
	"sort"
)

func main() {
	var n, xlb, xrb int // 敵の数, ビームの左端, ビームの右端
	_, _ = fmt.Scan(&n, &xlb, &xrb)

	type unit struct {
		i, xl, yu, xr, yd int
	}
	units := make([]unit, n)
	for i := range units {
		units[i].i = i
		_, _ = fmt.Scan(&units[i].xl, &units[i].yu, &units[i].xr, &units[i].yd)
	}

	// fmt.Println(units)

	sort.Slice(units, func(i, j int) bool {
		return units[i].xl < units[j].xl || (units[i].xl == units[j].xl && units[i].yu < units[j].yu)
	})

	// fmt.Println(units)

	hits := make(map[int]int, n)
	for i := xlb; i <= xrb; i++ {
		hit := -1
		hitY := math.MinInt64
		for _, u := range units {
			if u.xl <= i && i <= u.xr {
				if hitY < u.yu {
					hitY = u.yu
					hit = u.i
				}
			}
		}
		if hit >= 0 {
			hits[hit] = hits[hit] | 1
		}
		// fmt.Println(i, hits)
	}

	for i := 0; i < n; i++ {
		fmt.Println(hits[i])
	}
}
