package main

import (
	"fmt"
	"math"
)

func main() {
	var n, max int
	s := ""
	_, _ = fmt.Scan(&n)
	e := make(map[string]int, 0)
	for i := 0; i < n; i++ {
		_, _ = fmt.Scan(&s)
		if _, ok := e[s]; ok {
			e[s]++
		} else {
			e[s] = 1
		}

		if max < e[s] {
			max = e[s]
		}
	}

	if int(math.Ceil(float64(n)/2)) >= max {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
