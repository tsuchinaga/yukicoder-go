package main

import "fmt"

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	c := a + b
	d := divisor442(c, a)

	c /= d
	e := divisor442(c, b)

	fmt.Println(d * e)
}

func divisor442(a, b int) int {
	if a%b == 0 {
		return b
	}
	return divisor442(b, a%b)
}
