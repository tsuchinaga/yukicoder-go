package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	var k int
	_, _ = fmt.Scan(&k)
	max := int(math.Pow(2, float64(k))) - 1
	root := int(math.Pow(2, float64(k-1))) + 1

	a := fmt.Sprintf("%d ", root)
	for i := 0; i < k; i++ {
		p := int(math.Pow(2, float64(k-i-1)))
		for j := 0; j < int(math.Pow(2, float64(i))); j++ {
			q := p + p*2*j
			if 1 <= q && q <= max && q != root {
				a += fmt.Sprintf("%d ", q)
			}
		}
	}
	fmt.Println(strings.Trim(a, " "))
}
