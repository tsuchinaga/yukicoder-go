package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var a, b int
	_, _ = fmt.Scan(&a, &b)

	pattern := map[string]map[string]string{
		"0": {"0": "0", "1": "1", "2": "2"},
		"1": {"0": "1", "1": "2", "2": "3"},
		"2": {"0": "2", "1": "3"},
	}

	bits := make([]string, 32)
	for i := range bits {
		bits[i] = "0"
	}

	ab := []rune(fmt.Sprintf("%032b", a))
	bb := []rune(fmt.Sprintf("%032b", b))

	for i := range bits {
		r := pattern[string(ab[31-i])][string(bb[31-i])]
		r = pattern[r][bits[31-i]]

		switch r {
		case "1":
			bits[31-i] = "1"
		case "2":
			bits[31-i] = "0"
			bits[31-i-1] = "1"
		case "3":
			bits[31-i] = "1"
			bits[31-i-1] = "1"
		}
	}

	ans, _ := strconv.ParseInt(strings.Join(bits, ""), 2, 0)
	fmt.Println(ans)
}
