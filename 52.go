package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	var s string
	_, _ = fmt.Scan(&s)
	r := []rune(s)
	l := len(r)

	if l == 1 {
		fmt.Println(1)
		return
	}

	m := make(map[string]int)
	for i := 0; i < int(math.Pow(2, float64(l-1))); i++ {
		front, back := 0, l-1
		t := ""
		for _, c := range fmt.Sprintf("%0"+strconv.Itoa(l-1)+"b", i) {
			if c == '0' {
				t += string(r[front])
				front++
			} else {
				t += string(r[back])
				back--
			}
		}
		m[t+string(r[front])]++
	}

	fmt.Println(len(m))
}
