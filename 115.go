package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, d, k int
	_, _ = fmt.Scan(&n, &d, &k)

	// k個を満たす最低と最大の金額を出す
	total := 0
	nums := make([]int, k)
	for i := 1; i <= k; i++ {
		total += i
		nums[i-1] = i
	}
	// fmt.Println(total)
	// fmt.Println(nums)

	// 後ろからなるべく合計に近づくように調整していく
	t := k - 1
	for i := n; i > 0; i-- {
		if nums[t] == i { // 変えたいところと変えられるものが一致した時点で詰み
			break
		}

		u := total - nums[t] + i
		if u == d {
			total = u
			nums[t] = i
			t--
		} else if u < d {
			total = u
			nums[t] = i
			t--
		}

		if t < 0 { // tが小さくなりすぎてもNG
			break
		}
	}
	// fmt.Println(total, d)
	// fmt.Println(nums)

	if total == d {
		s := ""
		for _, n := range nums {
			s = fmt.Sprintf("%s %d", s, n)
		}
		fmt.Println(strings.Trim(s, " "))
	} else {
		fmt.Println(-1)
	}
}
