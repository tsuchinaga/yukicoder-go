package main

import (
	"fmt"
	"math"
)

func main() {
	var n, q, r int
	_, _ = fmt.Scan(&n)
	ans := make([]string, 0)
	for i := 0; i < int(math.Pow(2, float64(n))); i++ {
		qs := make([]int, n+1)
		for j := range qs {
			_, _ = fmt.Scan(&q)
			qs[j] = q
		}

		r += qs[len(qs)-1]
		if qs[len(qs)-1] == 1 {
			a := ""
			for k := 0; k < len(qs)-1; k++ {
				if k != 0 {
					a += "∧"
				}
				if qs[k] == 0 {
					a += "¬"
				}
				a += fmt.Sprintf("P_%d", k+1)
			}
			ans = append(ans, "("+a+")")
		}
	}

	if r == 0 {
		ans = []string{"⊥"}
	} else if r == int(math.Pow(2, float64(n))) {
		ans = []string{"⊤"}
	}

	ansStr := ""
	for i, a := range ans {
		if i != 0 {
			ansStr += "∨"
		}
		ansStr += a
	}
	fmt.Printf("A=%s\n", ansStr)
}
