package main

import (
	"fmt"
)

func main() {
	var n int
	_, _ = fmt.Scan(&n)

	a, b := 0, 1
	for i := 0; i < n; i++ {
		a, b = b, a+b
	}
	fmt.Println(b)
}
