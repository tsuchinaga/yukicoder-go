package main

import (
	"fmt"
	"math"
)

func main() {
	var a, b float64
	_, _ = fmt.Scan(&a, &b)

	if a > b {
		a, b = b, a
	}
	c1 := math.Sqrt(a*a + b*b)
	c2 := math.Sqrt(b*b - a*a)

	// fmt.Println(c1, c2)
	if c2 == 0 || c1 <= c2 {
		fmt.Printf("%.7f", c1)
	} else {
		fmt.Printf("%.7f", c2)
	}
}
