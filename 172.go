package main

import (
	"fmt"
	"math"
)

func main() {
	var x, y, r float64
	_, _ = fmt.Scan(&x, &y, &r)

	fmt.Println(math.Abs(x) + math.Abs(y) + math.Ceil(math.Sqrt(r*r+r*r)))
}
