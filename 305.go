package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var x int
	var s string
	// 全部0～全部9を試して、各数字の個数を探す
	nums := make([]int, 10)
	for i := 0; i <= 9; i++ {
		fmt.Println(strings.Repeat(strconv.Itoa(i), 10))
		_, _ = fmt.Scan(&x, &s)

		if s == "unlocked" {
			return
		}
		nums[i] = x
	}

	// fmt.Println(nums)

	// 1番当たりに近いのを取り出しておく
	max := nums[0]
	ans := make([]int, 10)
	for i := 1; i <= 9; i++ {
		if max < nums[i] {
			max = nums[i]
			for j := range ans {
				ans[j] = i
			}
		}
	}
	// fmt.Println(ans)

	// 先頭から変えながらmaxを増えるよう調整する
	for i := 0; i <= 9; i++ {
		for j, a := range nums {
			if a == 0 {
				continue
			}

			tmp := make([]int, 10)
			copy(tmp, ans)
			tmp[i] = j
			print305(tmp)

			_, _ = fmt.Scan(&x, &s)
			if s == "unlocked" {
				return
			}

			if max > x { // 減ったなら、元のが正解
				break
			} else if max < x { // 増えたなら、選んだのが正解
				max++
				ans[i] = j
				break
			}
		}

		nums[ans[i]]--
		// fmt.Println(i, max, nums, ans)
	}
}

func print305(nums []int) {
	for _, n := range nums {
		fmt.Print(n)
	}
	fmt.Println()
}
