package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
)

func main() {
	sc := bufio.NewScanner(os.Stdin)

	sn := func() int {
		sc.Scan()
		a, _ := strconv.Atoi(sc.Text())
		return a
	}

	b := sn()
	n := sn()
	total := b
	boxes := make([]int, n)
	for i := range boxes {
		c := sn()
		boxes[i] = c
		total += c
	}

	sort.Slice(boxes, func(i, j int) bool {
		return boxes[i] < boxes[j]
	})

	// fmt.Println("total:", total)
	// fmt.Println("boxes:", boxes)

	// 中央値
	m := 0
	if n%2 == 1 {
		m = boxes[n/2]
	} else {
		m = (boxes[n/2-1] + boxes[n/2]) / 2
	}
	// fmt.Println("m:", m)

	if m*n > total {
		m = total / n
	}
	// fmt.Println("m:", m)

	d := 0
	for _, c := range boxes {
		d += int(math.Abs(float64(m - c)))
	}
	fmt.Println(d)
}
