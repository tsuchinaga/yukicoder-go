package main

import "fmt"

func main() {
	var n, k int
	_, _ = fmt.Scan(&n, &k)
	fmt.Println(n/(k+1) + 1)
}
